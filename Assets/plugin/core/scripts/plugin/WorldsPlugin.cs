﻿using UnityEngine;
using System.Collections;

public class WorldsPlugin : Plugin {

	public GameObject worldsMenu;

	public override void InitializePlugin ()
	{
		base.InitializePlugin();
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		Plugin persistencePlugin = pluginRegistry.GetPlugin("Persistence");
		persistencePlugin.AddElement(new PersistenceCategory("Worlds", "worlds"));
		Plugin mainMenuPlugin = pluginRegistry.GetPlugin("MainMenu");
		mainMenuPlugin.AddElement(new MenuOption("Worlds", "World management", worldsMenu));
	}
}
