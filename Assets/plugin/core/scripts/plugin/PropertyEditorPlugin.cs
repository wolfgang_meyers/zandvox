﻿using UnityEngine;
using System.Collections.Generic;

public class PropertyEditorPlugin : Plugin {

	public List<PropertyEditor> editors = new List<PropertyEditor>();

	protected override System.Type ElementType ()
	{
		return typeof(PropertyEditor);
	}

	public override void InitializePlugin ()
	{
		base.InitializePlugin ();
		AddElements<PropertyEditor>(editors);
	}

}
