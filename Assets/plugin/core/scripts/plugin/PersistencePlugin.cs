using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class PersistencePlugin : Plugin {

	public string persistenceBaseDirectory;

	public List<PersistenceCategory> persistenceCategories = new List<PersistenceCategory>();

	protected override Type ElementType ()
	{
		return typeof(PersistenceCategory);
	}

	public override void InitializePlugin() {
		base.InitializePlugin();
		AddElements<PersistenceCategory>(persistenceCategories);
		foreach (PersistenceCategory category in persistenceCategories) {
			string dir = persistenceBaseDirectory + Path.DirectorySeparatorChar + category.path;
			if (!Directory.Exists(dir)) {
				Directory.CreateDirectory(dir);
			}
		}
	}

	public List<PersistedObject> SerializeElements<T>(ICollection<T> objects) where T : PluginElement {
		List<PersistedObject> result = new List<PersistedObject>(objects.Count);
		foreach (T item in objects) {
			PersistedObject obj = new PersistedObject();
			obj.objectType = item.displayName;
			Component[] components = item.GetComponents(typeof(IPersistentBehavior));
			foreach (Component component in components) {
				IPersistentBehavior persistentBehavior = (IPersistentBehavior)component;
				persistentBehavior.Save(obj);
			}
			result.Add(obj);
		}
		return result;
	}

	public List<T> DeserializeElements<T>(Plugin plugin, ICollection<PersistedObject> objects)
			where T : PluginElement {
		List<string> missingTypes = new List<string>();
		List<T> result = new List<T>();
		foreach (PersistedObject obj in objects) {
			T item = (T)plugin.GetElement(obj.objectType);
			if (item == null && !missingTypes.Contains(obj.objectType)) {
				missingTypes.Add(obj.objectType);
			}
			if (item != null) {
				T newItem = (T)Instantiate(item);
				Component[] components = newItem.GetComponents(typeof(IPersistentBehavior));
				foreach (Component component in components) {
					IPersistentBehavior persistentBehavior = (IPersistentBehavior)component;
					persistentBehavior.Restore(obj);
				}
				result.Add(newItem);
			}
		}
		foreach (string missingType in missingTypes) {
			Debug.LogError("Could not deserialize object type: " + missingType);
		}
		return result;
	}

	public List<string> ListPersistedItems(PersistenceCategory category, string filter) {
		List<string> result = new List<string>();
		string parentDirectory = GetBaseDir(category);
		foreach (string s in Directory.GetDirectories(parentDirectory)) {
			string name = s.Replace(parentDirectory + Path.DirectorySeparatorChar, "");
			if (name.Contains(filter)) {
				result.Add(name);
			}
		}
		return result;
	}

	private string GetBaseDir(PersistenceCategory category) {
		string basedir = persistenceBaseDirectory
			+ Path.DirectorySeparatorChar
				+ category.path;
		return basedir;
	}

	public List<string> ListPersistedItemComponents(PersistenceCategory category, string persistedItem) {
		string basedir = GetBaseDir(category);
		string dir = basedir + Path.DirectorySeparatorChar + persistedItem;
		if (!Directory.Exists(dir)) {
			throw new InvalidOperationException("Directory not found: " + dir);
		}
		List<string> result = new List<string>();
		foreach (string item in Directory.GetFiles(dir)) {
			String component = item.Replace(basedir + Path.DirectorySeparatorChar + persistedItem + Path.DirectorySeparatorChar, "");
			int ext = component.IndexOf(".");
			if (ext != -1) {
				component = component.Substring(0, ext);
			}
			result.Add(component);
		}
		return result;
	}

	public void CreatePersistedItem(PersistenceCategory category, string persistedItem) {
		string basedir = GetBaseDir(category);
		string dir = basedir + Path.DirectorySeparatorChar + persistedItem;
		if (!Directory.Exists(dir)) {
			Directory.CreateDirectory(dir);
		}
	}

	public void DeletePersistedItem(PersistenceCategory category, string persistedItem) {
		string basedir = GetBaseDir(category);
		string dir = basedir + Path.DirectorySeparatorChar + persistedItem;
		if (Directory.Exists(dir)) {
			Directory.Delete(dir, true);
		}
	}

	public bool PersistedItemExists(PersistenceCategory category, string persistedItem) {
		string basedir = GetBaseDir(category);
		string dir = basedir + Path.DirectorySeparatorChar + persistedItem;
		return Directory.Exists(dir);
	}

	public bool PersistedItemComponentExists(PersistenceCategory category, string persistedItem, string name) {
		string basedir = GetBaseDir(category);
		string file = basedir + Path.DirectorySeparatorChar + persistedItem + Path.DirectorySeparatorChar + name;
		if (!file.EndsWith(".json")) {
			file = file + ".json";
		}
		return File.Exists(file);
	}

	public void SavePersistedComponent<T>(PersistenceCategory category, string persistedItem, T component, string name) {
		string basedir = GetBaseDir(category);
		string dir = basedir + Path.DirectorySeparatorChar + persistedItem;
		string file = dir + Path.DirectorySeparatorChar + name;
		if (!file.EndsWith(".json")) {
			file = file + ".json";
		}
		string serializedData = JsonConvert.SerializeObject(component);
		File.WriteAllText(file, serializedData);
	}

	public T LoadPersistedComponent<T>(PersistenceCategory category, string persistedItem, string name) {
		string basedir = GetBaseDir(category);
		string dir = basedir + Path.DirectorySeparatorChar + persistedItem;
		string file = dir + Path.DirectorySeparatorChar + name;
		if (!file.EndsWith(".json")) {
			file = file + ".json";
		}
		string serializedData = File.ReadAllText(file);
		return JsonConvert.DeserializeObject<T>(serializedData);
	}

	public void DeletePersistedComponent(PersistenceCategory category, string persistedItem, string name) {
		string basedir = GetBaseDir(category);
		string dir = basedir + Path.DirectorySeparatorChar + persistedItem;
		string file = dir + Path.DirectorySeparatorChar + name;
		if (!file.EndsWith(".json")) {
			file = file + ".json";
		}
		File.Delete(file);
	}
}
