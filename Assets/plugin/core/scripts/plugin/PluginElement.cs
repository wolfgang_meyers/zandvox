using UnityEngine;
using System.Collections.Generic;

public class PluginElement : MonoBehaviour, ISelectableElement {
	public string displayName;
	public List<string> aliases = new List<string>();
	public Texture2D thumbnail;
	public string description;
	public bool selectable = true;

	#region IPluginElement implementation
	public string DisplayName {
		get {
			return displayName;
		}
	}
	public ICollection<string> Aliases {
		get {
			return aliases;
		}
	}
	public Texture2D Thumbnail {
		get {
			return thumbnail;
		}
	}
	public string Description {
		get {
			return description;
		}
	}
	
	public bool Selectable {
		get {
			return selectable;
		}
	}
	#endregion

	public override string ToString ()
	{
		return DisplayName;
	}
}
