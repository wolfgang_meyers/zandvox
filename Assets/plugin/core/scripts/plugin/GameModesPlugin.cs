﻿using UnityEngine;
using System.Collections.Generic;

public class GameModesPlugin : Plugin {

	public List<GameMode> modes = new List<GameMode>();
	public Tool gameModeTool;

	protected override System.Type ElementType ()
	{
		return typeof(GameMode);
	}

	public override void InitializePlugin ()
	{
		base.InitializePlugin();
		AddElements<GameMode>(modes);

		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		Plugin toolPlugin = pluginRegistry.GetPlugin("Tools");
		toolPlugin.AddElement(gameModeTool);
	}
}
