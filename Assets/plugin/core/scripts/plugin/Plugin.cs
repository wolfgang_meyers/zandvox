using UnityEngine;
using System;
using System.Collections.Generic;

public abstract class Plugin : PluginElement, ISelectableProvider {

	public List<string> requiredPlugins = new List<string>();
	public int loadingPriority = 0;

	protected virtual Type ElementType() {
		return typeof(ISelectableElement);
	}

	private SelectableElementProvider elementProvider;

	public SelectableElementProvider ElementProvider {
		get {
			if (elementProvider == null) {
				elementProvider = new SelectableElementProvider(ElementType());
			}
			return elementProvider;
		}
	}

	/// <summary>
	/// Adds the element.
	/// </summary>
	/// <returns><c>true</c>, if element was added, <c>false</c> otherwise.</returns>
	/// <param name="element">Element.</param>
	public bool AddElement<T>(T element) where T : ISelectableElement{
		return ElementProvider.AddElement<T>(element);
	}

	/// <summary>
	/// Adds the elements.
	/// </summary>
	/// <returns>The number of elements that were successfully added.</returns>
	/// <param name="elements">Elements.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public int AddElements<T>(params T[] elements) where T : ISelectableElement {
		return ElementProvider.AddElements<T>(elements);
	}

	public int AddElements<T>(IEnumerable<T> elements) where T : ISelectableElement {
		return ElementProvider.AddElements<T>(elements);
	}

	//ridiculous work-around to make specific type constraints work
	//in derived classes, while still using the PluginElement base class
	//here. Because IEnumerable<T : PluginElement> cannot be downcast to
	//IEnumerable<PluginElement> due to contravariance
//	private IEnumerable<ISelectableElement> cachedElements;
//
//	private IEnumerable<ISelectableElement> CachedElements {
//		get {
//			if (cachedElements == null) {
//				cachedElements = GetElements();
//			}
//			return cachedElements;
//		}
//	}

	public virtual void InitializePlugin() {

	}

	public virtual ISelectableElement GetDefaultElement() {
		return ElementProvider.GetDefaultElement();
	}

	public ISelectableElement GetElement(string name) {
		return ElementProvider.GetElement(name);
	}

	#region IEnumerable implementation

	public IEnumerator<ISelectableElement> GetEnumerator ()
	{
		return ElementProvider.GetEnumerator();
	}

	#endregion

	#region IEnumerable implementation

	System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator ()
	{
		return ElementProvider.GetEnumerator();
	}

	#endregion
}
