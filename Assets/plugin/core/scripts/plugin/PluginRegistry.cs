﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PluginRegistry : MonoBehaviour {
//	public List<Block> blockRegistry = new List<Block>();
//	public List<Tool> toolRegistry = new List<Tool>();
	public int maxRegistrationIterations = 10;
	private List<Plugin> registeredPlugins = new List<Plugin>();

	private bool initialized = false;

	public void SavePluginPrefences () {
		if (!Application.isPlaying) {
			return;
		}
		PersistencePlugin persistencePlugin = (PersistencePlugin)GetPlugin ("Persistence");
		PersistenceCategory pluginSettings = (PersistenceCategory)persistencePlugin.GetElement ("Plugin Settings");
		if (!persistencePlugin.PersistedItemExists(pluginSettings, "preferences")) {
			persistencePlugin.CreatePersistedItem(pluginSettings, "preferences");
		}
		foreach (Plugin plugin in registeredPlugins) {
			bool saved = false;
			PersistedObject obj = new PersistedObject();
			foreach (Component c in plugin.GetComponents<Component> ()) {
				if (c is IPersistentBehavior) {
					((IPersistentBehavior)c).Save (obj);
					saved = true;
				}
			}
			if (saved) {
				persistencePlugin.SavePersistedComponent(pluginSettings, "preferences", obj, plugin.DisplayName);
			}
		}
	}

	public void LoadPluginPreferences ()
	{
		if (!Application.isPlaying) {
			return;
		}
		//load plugin preferences
		PersistencePlugin persistencePlugin = (PersistencePlugin)GetPlugin ("Persistence");
		PersistenceCategory pluginSettings = (PersistenceCategory)persistencePlugin.GetElement ("Plugin Settings");
		if (!persistencePlugin.PersistedItemExists(pluginSettings, "preferences")) {
			persistencePlugin.CreatePersistedItem(pluginSettings, "preferences");
		}
		foreach (string component in persistencePlugin.ListPersistedItemComponents(pluginSettings, "preferences")) {
			Plugin plugin = GetPlugin(component);
			if (plugin == null) {
				Debug.LogError("Cannot load preferences for plugin " + component + " - plugin not found");
			} else {
				PersistedObject obj = persistencePlugin.LoadPersistedComponent<PersistedObject> (pluginSettings, "preferences", plugin.DisplayName);
				foreach (Component c in plugin.GetComponents<Component> ()) {
					if (c is IPersistentBehavior) {
						((IPersistentBehavior)c).Restore (obj);
					}
				}
			}
		}
	}

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			List<Plugin> plugins = new List<Plugin>(GameObject.FindObjectsOfType<Plugin>());
			plugins = plugins.OrderBy(p => p.loadingPriority).ToList();
			int tries = maxRegistrationIterations;
			while (tries > 0 && plugins.Count > 0) {
				plugins = RegisterPlugins(plugins);
				tries -= 1;
			}
			foreach (Plugin plugin in registeredPlugins) {
				if (Application.isPlaying) {
					plugin.SendMessage("InitializePlugin", SendMessageOptions.DontRequireReceiver);
				}
			}
			foreach (Plugin plugin in plugins) {
				Debug.LogError("Could not register plugin due to unmet dependencies: " + plugin.displayName);
			}
			LoadPluginPreferences();
		}
	}

	void Start() {
		Initialize();
	}

	/// <summary>
	/// Registers all plugins that have satisfied dependencies. A list
	/// of plugins without satisfied dependencies will be returned - the
	/// intent is that the list of plugins found in the scene will be passed
	/// in repeatedly until either a maximum number of iterations has been reached,
	/// or the returned list is empty (indicating that all plugins were successfully registered).
	/// If any plugins cannot be registered due to missing dependencies, an error
	/// will be logged.
	/// </summary>
	/// <returns>A list of plugins that couldn't yet be registered.</returns>
	/// <param name="plugins">Plugins.</param>
	private List<Plugin> RegisterPlugins(List<Plugin> plugins) {
		List<Plugin> unregistered = new List<Plugin>();
		foreach (Plugin plugin in plugins) {
			bool dependenciesMet = true;
			foreach (string dependency in plugin.requiredPlugins) {
				dependenciesMet = dependenciesMet && GetPlugin(dependency) != null;
			}
			if (dependenciesMet) {
				registeredPlugins.Add(plugin);
			} else {
				unregistered.Add(plugin);
			}
		}
		return unregistered;
	}

	public Plugin GetPlugin(string name) {
		Initialize();
		//TODO: index by name
		foreach (Plugin plugin in registeredPlugins) {
			if (plugin.displayName == name || plugin.aliases.Contains(name)) {
				return plugin;
			}
		}
		Debug.Log("PluginRegistry.GetPlugin(" + name + ") not registered");
		return null;
	}
}
