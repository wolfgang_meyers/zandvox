﻿using UnityEngine;
using System.Collections.Generic;

public class ClipboardActionPlugin : Plugin {

	public List<MenuOption> defaultMenuOptions = new List<MenuOption>();

	protected override System.Type ElementType ()
	{
		return typeof(MenuOption);
	}

	public override void InitializePlugin ()
	{
		base.InitializePlugin ();
		AddElements<MenuOption>(defaultMenuOptions);
	}

}
