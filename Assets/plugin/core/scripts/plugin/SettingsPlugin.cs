﻿using UnityEngine;
using System.Collections.Generic;

public class SettingsPlugin : Plugin {

	public MenuOption settingsOption;
	public List<MenuOption> settings = new List<MenuOption>();

	protected override System.Type ElementType ()
	{
		return typeof(MenuOption);
	}

	public override void InitializePlugin ()
	{
		base.InitializePlugin ();
		AddElements<MenuOption>(settings);

		//add "Settings" to main menu
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		Plugin mainMenuPlugin = pluginRegistry.GetPlugin("MainMenu");
		mainMenuPlugin.AddElement(settingsOption);
	}

}
