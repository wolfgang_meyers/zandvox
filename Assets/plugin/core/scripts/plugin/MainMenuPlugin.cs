﻿using UnityEngine;
using System.Collections.Generic;

public class MainMenuPlugin : Plugin {

	protected override System.Type ElementType ()
	{
		return typeof(MenuOption);
	}
}
