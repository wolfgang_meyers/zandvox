using UnityEngine;
using System.Collections.Generic;

public class PaintPlugin : Plugin {

	public string ColorComponentToHex(float component) {
		int v = (int)Mathf.Floor(component * 255);
		string result = string.Format ("{0:X}", v);
		if (result.Length < 2) {
			result = "0" + result;
		}
		return result;
	}

	public Block paintableBlock;
	public bool addPaintBlocks = true;

	public List<ColorSpec> colors = new List<ColorSpec>();

	protected override System.Type ElementType ()
	{
		return typeof(ColorSpec);
	}

	override public void InitializePlugin() {
		PluginRegistry pluginRegistry = (PluginRegistry)GameObject.FindObjectOfType(typeof(PluginRegistry));
		Plugin blockPlugin = pluginRegistry.GetPlugin("Blocks");
		foreach (ColorSpec colorSpec in colors) {
			AddElement<ColorSpec>(colorSpec);
			Texture2D texture = new Texture2D(100, 100);
			for (int x = 0; x < 100; x++) {
				for (int y = 0; y < 100; y++) {
					texture.SetPixel(x, y, colorSpec.ToColor());
				}
			}
			texture.Apply();
			colorSpec.thumbnail = texture;
			if (addPaintBlocks && Application.isPlaying) {
				Block prototype = (Block)Instantiate(paintableBlock);
				prototype.displayName = colorSpec.DisplayName;
				prototype.transform.position = new Vector3(float.MaxValue, 0, 0);
				prototype.thumbnail = texture;
				prototype.SendMessage("PaintObject", colorSpec.DisplayName);
				blockPlugin.AddElement(prototype);
			}
		}
	}

}
