﻿using UnityEngine;
using System.Collections.Generic;

public class InputPlugin : Plugin, IPersistentBehavior {

	private bool initialized = false;
	private string captureInputFor = null;

	public List<InputBinding> inputBindings = new List<InputBinding>();

	public MenuOption inputBindingsOption;

	public void CaptureInput(string inputFor) {
		this.captureInputFor = inputFor;
	}

	public void ReleaseInput() {
		captureInputFor = null;
	}

	protected override System.Type ElementType ()
	{
		return typeof(InputBinding);
	}

	public override void InitializePlugin ()
	{
		base.InitializePlugin ();
		Initialize();
	}

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			foreach (InputBinding binding in inputBindings) {
				binding.key = binding.defaultKey;
				binding.modifierKey = binding.defaultModifierKey;
			}
			AddElements<InputBinding>(inputBindings);
			PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
			Plugin settingsPlugin = pluginRegistry.GetPlugin("Settings");
			settingsPlugin.AddElement(inputBindingsOption);
		}
	}

	private bool IsBindingEnabled(InputBinding binding) {
		//user may disable inputs...
		return binding != null &&
			(binding.ignoreGroups || captureInputFor == null || captureInputFor == (binding.inputGroup));
	}

	public bool GetInput(string input) {
		Initialize();
		InputBinding binding = (InputBinding)GetElement(input);
		if (IsBindingEnabled(binding)) {
			//TODO: check for other input types than key
			return binding.GetKey();
		}
		return false;
	}

	public bool GetInputDown(string input) {
		Initialize();
		InputBinding binding = (InputBinding)GetElement(input);
		if (IsBindingEnabled(binding)) {
			//TODO: check for other input types than key
			return binding.GetKeyDown();
		}
		return false;
	}

	public bool GetInputUp(string input) {
		Initialize();
		InputBinding binding = (InputBinding)GetElement(input);
		if (IsBindingEnabled(binding)) {
			//TODO: check for other input types than key
			return binding.GetKeyUp();
		}
		return false;
	}
	
	public void Save (PersistedObject obj)
	{
		Initialize();

		foreach (ISelectableElement element in this) {
			InputBinding binding = (InputBinding)element;
			PersistedBehaviorData data = new PersistedBehaviorData();
			data.SetStringValue("key", binding.key.ToString());
			data.SetStringValue("modifierKey", binding.modifierKey.ToString());
			obj.SetData<PersistedBehaviorData>(binding.DisplayName, data);
		}
	}

	public void Restore (PersistedObject obj)
	{
		Initialize();
		foreach (ISelectableElement element in this) {
			InputBinding binding = (InputBinding)element;
			PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>(binding.DisplayName);
			if (data != null) {
				binding.key = (KeyCode)System.Enum.Parse(typeof(KeyCode), data.GetStringValue("key"));
				binding.modifierKey = (KeyCode)System.Enum.Parse(typeof(KeyCode), data.GetStringValue("modifierKey"));
			}
		}
	}
}
