﻿using UnityEngine;
using System.Collections.Generic;

public class Block : PluginElement, IPersistentBehavior {

	public bool orientable = true;
	public bool supportsInteraction = false;

	private Coordinates position = new Coordinates();
	private IBlockSystem environment;
	
	public Coordinates Position {
		get { return position; }
	}
	
	public IBlockSystem Environment {
		get { return environment; }
	}
	
	//TODO: implement component...
	
	//block properties
//	public string category;
//	public string displayName;
//	public List<string> aliases = new List<string>();
//	public Texture2D thumbnail;
	
	public void MoveTo(Coordinates position) {
		Coordinates lastPosition = Position;
		this.position = position;
		transform.localPosition = position.ToVector();
		if (environment != null) {
			environment.BlockMoved(this, lastPosition);
		}
		SendMessage("OnBlockMoved", position, SendMessageOptions.DontRequireReceiver);
	}

	public void UpdateOrientation(Vector3 orientation) {
		if (orientable) {
			gameObject.transform.forward = orientation.normalized;
		}
	}
	
	public void InitEnvironment(IBlockSystem env) {
		environment = env;
	}

	public void DetachFromEnvironment() {
		environment = null;
	}
	
	// Use this for initialization
	void Start () {
		//environment = (Environment)GameObject.FindObjectOfType(typeof(Environment));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Save (PersistedObject obj)
	{
		PersistedBehaviorData data = new PersistedBehaviorData();
		data.SetCoordinatesValue("position", Position);
		data.SetVector3Value("forward", gameObject.transform.forward);
		obj.SetData("Block", data);
	}

	public void Restore (PersistedObject obj)
	{
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("Block");
		if (data != null) {
			MoveTo(data.GetCoordinatesValue("position"));
			if (data.IsSet("forward") && orientable) {
				gameObject.transform.forward = data.GetVector3Value("forward");
			}
		}
		//backwards compat with older versions that contained orientation
		//in a separately saved component
		data = obj.GetData<PersistedBehaviorData>("OrientOnInteraction");
		if (data != null) {
			UpdateOrientation(data.GetVector3Value("forward"));
		}
	}

	public string Name {
		get {
			return "Block";
		}
	}
}
