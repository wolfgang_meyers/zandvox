﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class Chunk : MonoBehaviour, IEnumerable<Block> {

	private bool regenerating = false;
	private Dictionary<Coordinates, Block> grid = new Dictionary<Coordinates, Block>();

	private List<GameObject> meshContainers = new List<GameObject>();
	
	public void RemoveBlock(Block block) {
		Coordinates c = block.Position;
		if (grid.ContainsKey(c)) {
			grid.Remove(c);
		}
		ScheduleRegeneration();
	}

	public void AddBlock(Block block) {
		RemoveBlock(block);
		Coordinates c = block.Position;
		grid[c] = block;
		ScheduleRegeneration();
	}

	// Use this for initialization
	void Start () {

	}
	
	private void ScheduleRegeneration() {
		if (!regenerating) {
			regenerating = true;
			StartCoroutine("Regenerate");
		}
	}

	private System.Collections.IEnumerable Regenerate() {
		yield return new WaitForSeconds(0.1f);
		foreach (GameObject gobj in meshContainers) {
			Destroy (gobj);
		}
		meshContainers.Clear();

		Dictionary<int, List<Block>> blocksByMaterial = new Dictionary<int, List<Block>>();
		Dictionary<int, Material> materialIndex = new Dictionary<int, Material>();
		foreach (Block block in grid.Values) {
			//TODO: customized behavior in the block?


			MeshRenderer mr = block.GetComponent<MeshRenderer>();
			if (mr != null) {
				bool hide = true;
				foreach (Coordinates c in block.Position.GetNeighbors()) {
					Block neighbor = block.Environment.GetBlockAt(c);
					
					hide = hide && !(neighbor == null ||
					                 neighbor.GetComponent(typeof(MeshOptimizer)) == null);
				}
				if (!hide) {
					Material mat = mr.material;
					int matId = mat.GetInstanceID();
					if (!blocksByMaterial.ContainsKey(matId)) {
						blocksByMaterial[matId] = new List<Block>();
					}
					blocksByMaterial[matId].Add(block);
					materialIndex[matId] = mat;
				}
				mr.enabled = false;
			}
		}

		foreach (int matId in blocksByMaterial.Keys) {
			List<Block> blocks = blocksByMaterial[matId];
			Material mat = materialIndex[matId];
			GameObject meshContainer = new GameObject();
			meshContainers.Add(meshContainer);
			MeshFilter mf = meshContainer.AddComponent<MeshFilter>();
			MeshRenderer mr = meshContainer.AddComponent<MeshRenderer>();

			CombineInstance[] cmb = new CombineInstance[blocks.Count];
			for (int i = 0; i < blocks.Count; i++) {
				Block block = blocks[i];
				block.transform.position = block.Position.ToVector();
				MeshFilter childMf = block.GetComponent<MeshFilter>();
				MeshRenderer childMr = block.GetComponent<MeshRenderer>();
				if (childMf != null && childMr != null) {
					cmb[i].mesh = childMf.sharedMesh;
					cmb[i].transform = childMf.transform.localToWorldMatrix;
				}
				block.transform.position = new Vector3(0, 0, float.MaxValue);
			}
			mf.mesh = new Mesh();
			mf.mesh.CombineMeshes(cmb);
			mr.material = mat;
			mr.castShadows = true;
			mr.receiveShadows = true;
		}
		regenerating = false;
	}

	public IEnumerator<Block> GetEnumerator ()
	{
		return grid.Values.GetEnumerator();
	}

	System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator ()
	{
		return grid.Values.GetEnumerator();
	}
}
