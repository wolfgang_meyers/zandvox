using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable()]
public struct Coordinates
{
	int x;
	int y;
	int z;
	
	public int X { get { return x; } }
	public int Y { get { return y; } }
	public int Z { get { return z; } }
	
	public Coordinates(Vector3 v) {
		this.x = Mathf.RoundToInt(v.x);
		this.y = Mathf.RoundToInt(v.y);
		this.z = Mathf.RoundToInt(v.z);
	}
	
	public Coordinates (int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3 ToVector() {
		return new Vector3(x, y, z);
	}
	
	public override int GetHashCode ()
	{
		return ToString().GetHashCode();
	}
	
	public override bool Equals (object obj)
	{
		if (!(obj is Coordinates)) {
			return false;
		}
		Coordinates other = (Coordinates)obj;
		return other.x == x && other.y == y && other.z == z;
	}

	public static bool operator ==(Coordinates a, Coordinates b) {
		return (a.Equals(b));
	}

	public static bool operator !=(Coordinates a, Coordinates b) {
		return !(a == b);
	}
	
	public override string ToString ()
	{
		return x + "," + y + "," + z;
	}

	public List<Coordinates> GetNeighbors() {
		return GetNeighbors(1);
	}
	
	public List<Coordinates> GetNeighbors(int dist) {
		List<Coordinates> result = new List<Coordinates>(6);
		result.Add(new Coordinates(x + dist, y, z));
		result.Add(new Coordinates(x - dist, y, z));
		result.Add(new Coordinates(x, y + dist, z));
		result.Add(new Coordinates(x, y - dist, z));
		result.Add(new Coordinates(x, y, z + dist));
		result.Add(new Coordinates(x, y, z - dist));
		return result;
	}
	
	public static Coordinates operator +(Coordinates c, Coordinates other) {
		return new Coordinates(c.X + other.X, c.Y + other.Y, c.Z + other.Z);
	}
	
	public static Coordinates operator -(Coordinates c, Coordinates other) {
		return new Coordinates(c.X - other.X, c.Y - other.Y, c.Z - other.Z);
	}

	public static implicit operator Vector3(Coordinates c) {
		return c.ToVector();
	}

	public static implicit operator Coordinates(Vector3 v) {
		return new Coordinates(v);
	}

	public static Coordinates Minimize(Coordinates a, Coordinates b) {
		return new Coordinates(
			Mathf.Min(a.X, b.X),
			Mathf.Min(a.Y, b.Y),
			Mathf.Min(a.Z, b.Z)
		);
	}

	public static Coordinates Maximize(Coordinates a, Coordinates b) {
		return new Coordinates(
			Mathf.Max(a.X, b.X),
			Mathf.Max(a.Y, b.Y),
			Mathf.Max(a.Z, b.Z)
		);
	}

	public static Coordinates Lerp(Coordinates a, Coordinates b, float t) {
		return new Coordinates(Vector3.Lerp(a.ToVector(), b.ToVector(), t));
	}

	public static Coordinates Parse(string value) {
		string[] parts = value.Split(',');
		return new Coordinates(int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2]));
	}
}


