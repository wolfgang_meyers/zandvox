﻿using UnityEngine;
using System.Collections.Generic;

public class GarbageCollector : MonoBehaviour {

	public float gcDelay = 0.1f;
	private Stack<GameObject> garbageHeap = new Stack<GameObject>();

	public void Collect(IEnumerable<GameObject> garbage) {
		foreach (GameObject obj in garbage) {
			Collect(obj);
		}
	}

	public void Collect(GameObject garbage) {
		garbage.transform.position = new Vector3(0, 0, float.MaxValue);
		garbageHeap.Push(garbage);
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(GC ());
	}
	
	System.Collections.IEnumerator GC() {
		yield return new WaitForSeconds(gcDelay);
		if (garbageHeap.Count > 0) {
			GameObject garbage = garbageHeap.Pop();
			//we don't want a failure to destroy to end
			//our GC process. Log an error when it happens
			try {
				Destroy (garbage);
			} catch (System.Exception e) { 
				Debug.LogError("Error collecting garbage: " + e.Message);
			}
		}
		StartCoroutine(GC ());
	}
}
