﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System.Linq;

public class Environment : MonoBehaviour, IBlockSystem {

	public int chunkSize = 8;
	public int regionSize = 8 * 8;
	public float autosaveDelay = 10f;
	public float autoloadDelay = 2f;
	public float chunkloadDelay = 0.1f;

	public int regionLoadDistance = 8 * 8 * 2;
	public int regionUnloadDistance = 8 * 8 * 3;

	private IDictionary<Coordinates, Block> grid = new Dictionary<Coordinates, Block>();
	private Plugin blocksPlugin;

	private Dictionary<Coordinates, Chunk> loadedChunks = new Dictionary<Coordinates, Chunk>();
	private Dictionary<Coordinates, List<PersistedObject>> unloadedChunks = new Dictionary<Coordinates, List<PersistedObject>>();
	private Dictionary<Coordinates, HashSet<Coordinates>> regionChunkIndex = new Dictionary<Coordinates, HashSet<Coordinates>>();

	private HashSet<Coordinates> modifiedRegions = new HashSet<Coordinates>();
//	private HashSet<Coordinates> unloadingChunks = new HashSet<Coordinates>();

	private string worldName;
	private PersistedObject persistedWorldData = new PersistedObject();
	private GarbageCollector gc;
	private bool initialized = false;
	private bool isPlaying = false;

	public PersistedObject PersistedWorldData {
		get { return persistedWorldData; }
	}
	
	public string WorldName {
		get { return worldName; }
	}
	
	private PluginRegistry pluginRegistry;
	private PersistencePlugin persistencePlugin;
	private GameModesPlugin gameModesPlugin;
	private Vector3 lastPerspectivePosition = Vector3.zero;
	private IPerspectiveController perspectiveController;

	/// <summary>
	/// Gets or sets the perspective controller.
	/// Setting this value will trigger a change of active
	/// IPerspectiveController instance.
	/// </summary>
	/// <value>The perspective controller.</value>
	public IPerspectiveController PerspectiveController {
		get {
			return perspectiveController;
		}
		set {
			if (perspectiveController != null) {
				perspectiveController.DisableController();
			}
			perspectiveController = value;
			if (perspectiveController != null) {
				perspectiveController.EnableController();
				//perspectiveController.Position = lastPerspectivePosition;
			}
		}
	}

	private GameMode gameMode;

	public string GameMode {
		get {
			string result = persistedWorldData.GetData<string>("gameMode");
			//backwards compatibility with worlds that have no game mode set
			if (result == null) {
				GameMode = gameModesPlugin.GetDefaultElement().DisplayName;
			}
			return persistedWorldData.GetData<string>("gameMode");
		} set {
			Initialize();
			if (gameModesPlugin.GetElement(value) == null) {
				Debug.LogError("Invalid game mode: " + value);
			} else {
				persistedWorldData.SetData<string>("gameMode", value);
			}
		}
	}

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			pluginRegistry = (PluginRegistry)GameObject.FindObjectOfType(typeof(PluginRegistry));
			persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
			gameModesPlugin = (GameModesPlugin)pluginRegistry.GetPlugin("GameModes");
			GameMode = gameModesPlugin.GetDefaultElement().DisplayName;
			blocksPlugin = pluginRegistry.GetPlugin("Blocks");
			gc = GameObject.FindObjectOfType<GarbageCollector>();
		}
	}

	private void OnDisable() {
		isPlaying = false;
	}
	
	// Use this for initialization
	void Start () {
		Initialize();
	}
	
	public void BlockMoved(Block block, Coordinates lastPosition) {
		Coordinates c = block.Position;
		if (grid.ContainsKey(c)) {
			DestroyBlock(grid[c]);
		}
		grid[c] = block;
		WakeUpNeighbors(lastPosition);
		WakeUpNeighbors(c);
	}
	
	private void WakeUpNeighbors(Coordinates c) {
		foreach(Coordinates neighborCoordinates in c.GetNeighbors()) {
			if (grid.ContainsKey(neighborCoordinates)) {
				grid[neighborCoordinates].SendMessage("WakeUp", SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	private void RemoveBlock(Block block, bool scheduleSave) {
		Coordinates c = block.Position;
		
		if (grid.ContainsKey(c)) {
			grid.Remove(c);

			WakeUpNeighbors(c);

			//in reality, the chunk/region should already be loaded
			//and if they're not, it's a bug. Tolerating it anyway.
			Coordinates chunkCoordinates = ZUtil.RoundCoordinates(c, chunkSize);
			if (loadedChunks.ContainsKey(chunkCoordinates)) {
				loadedChunks[chunkCoordinates].RemoveBlock(block);
			}


			Coordinates regionCoordinates = ZUtil.RoundCoordinates(chunkCoordinates, regionSize);

			if (scheduleSave) {
				modifiedRegions.Add(regionCoordinates);
			}
		}
	}

	public void RemoveBlock(Block block) {
		RemoveBlock(block, true);
	}
	
	public void DestroyBlock(Block block) {
		RemoveBlock(block);
		gc.Collect(block.gameObject);
	}

	private void AddBlock(Block block, bool scheduleSave) {
		Coordinates c = block.Position;
		if (grid.ContainsKey(c)) {
			DestroyBlock(grid[c]);
		}
		grid[c] = block;
		block.InitEnvironment(this);
		WakeUpNeighbors(c);

		Coordinates chunkCoordinates = ZUtil.RoundCoordinates(c, chunkSize);
		if (!loadedChunks.ContainsKey(chunkCoordinates)) {
			loadedChunks[chunkCoordinates] = (new GameObject()).AddComponent<Chunk>();
		}
		loadedChunks[chunkCoordinates].AddBlock(block);

		Coordinates regionCoordinates = ZUtil.RoundCoordinates(chunkCoordinates, regionSize);
		if (!regionChunkIndex.ContainsKey(regionCoordinates)) {
			regionChunkIndex[regionCoordinates] = new HashSet<Coordinates>();
		}
		regionChunkIndex[regionCoordinates].Add(chunkCoordinates);

		if (scheduleSave) {
			modifiedRegions.Add(regionCoordinates);
		}
	}

	public void AddBlock(Block block) {
		AddBlock(block, true);
	}
	
	public Block GetBlockAt(Coordinates c) {
		if (grid.ContainsKey(c)) {
			return grid[c];
		}
		return null;
	}

	public void Save(string name) {
		Initialize();
		worldName = name;
		PersistenceCategory worlds = (PersistenceCategory)persistencePlugin.GetElement("Worlds");
		if (modifiedRegions.Count > 0) {
			foreach (Coordinates regionC in modifiedRegions) {
				
				if (regionChunkIndex.ContainsKey(regionC)) {
					HashSet<Coordinates> chunkCoordinateList = regionChunkIndex[regionC];
					string regionFileName = RegionComponentName(regionC);
					PersistedRegion persistedRegion = new PersistedRegion();
					persistedRegion.blocks = new List<PersistedObject>();
					foreach (Coordinates chunkC in chunkCoordinateList) {
						if (loadedChunks.ContainsKey(chunkC)) {
							Chunk chunk = loadedChunks[chunkC];
							List<Block> chunkBlocks = chunk.ToList();
							persistedRegion.blocks.AddRange(persistencePlugin.SerializeElements(chunkBlocks));
						} else if (unloadedChunks.ContainsKey(chunkC)) {
							persistedRegion.blocks.AddRange(unloadedChunks[chunkC]);
						} else {
							Debug.LogError(
								string.Format (
								"Found chunk in regionChunkIndex, but chunk does not exist. Region={0}, Chunk={1}",
								regionC, chunkC
								)
							);
						}
					}
					persistencePlugin.SavePersistedComponent(worlds, WorldName, persistedRegion, regionFileName);
				}
				
			}
			modifiedRegions.Clear();
		}
		if (perspectiveController != null && lastPerspectivePosition != perspectiveController.Position) {
			lastPerspectivePosition = perspectiveController.Position;
			PersistedBehaviorData data = new PersistedBehaviorData();
			data.SetVector3Value("lastPerspectivePosition", lastPerspectivePosition);
			persistedWorldData.SetData<PersistedBehaviorData>("PlayerPosition", data);
		}
		if (!persistencePlugin.PersistedItemExists(worlds, name)) {
			persistencePlugin.CreatePersistedItem(worlds, name);
		}
		persistencePlugin.SavePersistedComponent(worlds, name, persistedWorldData, "world");
	}
	
	public bool Exists(string name) {
		Initialize();
		PersistenceCategory worlds = (PersistenceCategory)persistencePlugin.GetElement("Worlds");
		return persistencePlugin.PersistedItemExists(worlds, name);
	}
	
	public List<string> ListGames(string filter) {
		Initialize();
		PersistenceCategory worlds = (PersistenceCategory)persistencePlugin.GetElement("Worlds");
		return persistencePlugin.ListPersistedItems(worlds, filter);
	}
	
	public void Load(string name) {
		Initialize();
		PersistenceCategory worlds = (PersistenceCategory)persistencePlugin.GetElement("Worlds");
		persistedWorldData = persistencePlugin.LoadPersistedComponent<PersistedObject>(worlds, name, "world");
		//TODO: hack?
		CameraGuide cameraGuide = (CameraGuide)GameObject.FindObjectOfType(typeof(CameraGuide));
		cameraGuide.Restore(persistedWorldData);

		worldName = name;

		PersistedBehaviorData data = persistedWorldData.GetData<PersistedBehaviorData>("PlayerPosition");
		if (data != null) {
			lastPerspectivePosition = data.GetVector3Value("lastPerspectivePosition");
			if (perspectiveController != null) {
				perspectiveController.Position = lastPerspectivePosition;
			}
		}
		isPlaying = true;
		StartCoroutine(Autoload());
		StartCoroutine(Autosave());
		StartCoroutine(ChunkLoad());
	}

	private string RegionComponentName(Coordinates regionCoordinates) {
		return string.Format("region.{0}.{1}.{2}", regionCoordinates.X, regionCoordinates.Y, regionCoordinates.Z);
	}

	private void LoadRegion(Coordinates regionCoordinates) {
		//Debug.Log("Load region: " + regionCoordinates);
		PersistedRegion region = null;
		PersistenceCategory worlds = (PersistenceCategory)persistencePlugin.GetElement("Worlds");
		if (persistencePlugin.PersistedItemComponentExists(worlds, WorldName, RegionComponentName(regionCoordinates))) {
			region = persistencePlugin.LoadPersistedComponent<PersistedRegion>(
				worlds, WorldName, RegionComponentName(regionCoordinates)
			);
		} else {
			modifiedRegions.Add(regionCoordinates);
			region = new PersistedRegion();
			region.blocks = new List<PersistedObject>();
		}
		regionChunkIndex[regionCoordinates] = new HashSet<Coordinates>();
		foreach (PersistedObject obj in region.blocks) {
			PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("Block");
			Coordinates pos = data.GetCoordinatesValue("position");
			Coordinates chunkCoordinates = ZUtil.RoundCoordinates(pos, chunkSize);
			if (!unloadedChunks.ContainsKey(chunkCoordinates)) {
				unloadedChunks[chunkCoordinates] = new List<PersistedObject>();
			}
			unloadedChunks[chunkCoordinates].Add(obj);
			regionChunkIndex[regionCoordinates].Add(chunkCoordinates);
		}
	}

	private void UnloadRegion(Coordinates regionCoordinates) {
		//Debug.Log("Unload region: " + regionCoordinates);
		HashSet<Coordinates> chunkCoordinates = new HashSet<Coordinates>(regionChunkIndex[regionCoordinates]);
		foreach (Coordinates c in chunkCoordinates) {
			UnloadChunk(c);
		}
		regionChunkIndex.Remove(regionCoordinates);
	}

	private void LoadChunk(Coordinates chunkCoordinates) {
		if (unloadedChunks.ContainsKey(chunkCoordinates)) {
			List<PersistedObject> objs = unloadedChunks[chunkCoordinates];
			List<Block> loadedBlocks = persistencePlugin.DeserializeElements<Block>(blocksPlugin, objs);
			foreach (Block block in loadedBlocks) {
				AddBlock(block);
			}
			unloadedChunks.Remove(chunkCoordinates);
		}
	}

	private void UnloadChunk(Coordinates chunkCoordinates) {
		if (loadedChunks.ContainsKey(chunkCoordinates)) {
			Chunk chunk = loadedChunks[chunkCoordinates];
			List<Block> blocksToDestroy = new List<Block>();
			foreach (Block block in chunk) {
				blocksToDestroy.Add(block);
			}
			foreach (Block block in blocksToDestroy) {
				DestroyBlock(block);
			}
			gc.Collect(chunk.gameObject);
			loadedChunks.Remove(chunkCoordinates);
		}
	}

	private void SortByCloseness(Coordinates reference, List<Coordinates> positions) {
		positions.Sort (
			(a, b) => (a - reference).ToVector().magnitude
			.CompareTo((b - reference).ToVector().magnitude)
		);
	}

	private System.Collections.IEnumerator ChunkLoad() {
		if (!isPlaying) {
			yield break;
		}
		Coordinates perspectivePosition = new Coordinates(
			perspectiveController == null
				? lastPerspectivePosition : perspectiveController.Position
		);
		//try to load at least one chunk, if there are any unloaded, scheduled to load
		if (unloadedChunks.Count > 0) {
			List<Coordinates> unloadedChunkCoordinates = new List<Coordinates>(unloadedChunks.Keys);
			SortByCloseness(perspectivePosition, unloadedChunkCoordinates);
			LoadChunk(unloadedChunkCoordinates[0]);
		}
		
		//try to unload at least one chunk, if there are any loaded, scheduled to unload
//		if (unloadingChunks.Count > 0) {
//			Coordinates chunkCoordinates = unloadingChunks.First();
//			UnloadChunk(chunkCoordinates);
//			unloadingChunks.Remove(chunkCoordinates);
//		}
		yield return new WaitForSeconds(chunkloadDelay);
		StartCoroutine(ChunkLoad ());
	}

	private System.Collections.IEnumerator Autoload() {
		if (!isPlaying) {
			yield break;
		}
		//load any regions that are close enough
		Coordinates perspectivePosition =  new Coordinates(
			perspectiveController == null ? lastPerspectivePosition : perspectiveController.Position);
		Coordinates currentRegion = ZUtil.RoundCoordinates(perspectivePosition, regionSize);
		if (!regionChunkIndex.ContainsKey(currentRegion)) {
			LoadRegion(currentRegion);
		}
		for (int x = currentRegion.X - regionLoadDistance; x < currentRegion.X + regionLoadDistance; x += regionSize) {
			for (int y = currentRegion.Y - regionLoadDistance; y < currentRegion.Y + regionLoadDistance; y += regionSize) {
				for (int z = currentRegion.Z - regionLoadDistance; z < currentRegion.Z + regionLoadDistance; z += regionSize) {
					//round, just to be safe
					Coordinates regionCoordinates = ZUtil.RoundCoordinates(new Coordinates(x, y, z), regionSize);
					if (!regionChunkIndex.ContainsKey(regionCoordinates)) {
						float distance = (regionCoordinates - currentRegion).ToVector().magnitude;
						if (distance <= regionLoadDistance) {
							LoadRegion(regionCoordinates);
						}
					}
				}
			}
		}
		//unload any regions that are too far away
		List<Coordinates> regionsToRemove = new List<Coordinates>();
		foreach (Coordinates regionCoordinates in regionChunkIndex.Keys) {
			float distance = (regionCoordinates - currentRegion).ToVector().magnitude;
			if (distance >= regionUnloadDistance) {
				regionsToRemove.Add(regionCoordinates);
			}
		}
		foreach (Coordinates regionCoordinates in regionsToRemove) {
			UnloadRegion(regionCoordinates);
		}
		yield return new WaitForSeconds(autoloadDelay);
		StartCoroutine(Autoload());
	}

	private System.Collections.IEnumerator Autosave() {
		yield return new WaitForSeconds(autosaveDelay);
		if (!isPlaying) {
			yield break;
		} else {
			Save(worldName);
			StartCoroutine(Autosave ());
		}
	}

	public void Quit() {
		isPlaying = false;
		Save(WorldName);
		List<Coordinates> regions = new List<Coordinates>(regionChunkIndex.Keys);
		foreach (Coordinates c in regions) {
			UnloadRegion(c);
		}
	}

	public bool IsChunkLoading(Coordinates c) {
		c = ZUtil.RoundCoordinates(c, chunkSize);
		return unloadedChunks.ContainsKey(c);
	}

	public bool IsChunkLoaded(Coordinates c) {
		c = ZUtil.RoundCoordinates(c, chunkSize);
		return loadedChunks.ContainsKey(c);
	}
}
