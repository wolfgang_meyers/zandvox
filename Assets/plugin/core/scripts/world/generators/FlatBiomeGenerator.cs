﻿using UnityEngine;
using System.Collections;

//TODO: overhaul this class when chunk loading/unloading
//becomes an issue
public class FlatBiomeGenerator : MonoBehaviour {
	
	public int height;
	public Block block;
	public float tickInterval;
	public int radius;

	private Environment env;

	// Use this for initialization
	void Start () {
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		StartCoroutine("OnTick");
	}

	IEnumerator OnTick() {
		Object[] avatars = GameObject.FindObjectsOfType(typeof(AvatarCharacter));
		for (int i = 0; i < avatars.Length; i++) {
			AvatarCharacter avatarCharacter = (AvatarCharacter)avatars[i];
			Generate (avatarCharacter);
		}
		yield return new WaitForSeconds(tickInterval);
		if (enabled) {
			StartCoroutine("OnTick");
		}
	}

	void Generate(Coordinates center) {
		for (int x = -radius; x <= radius; x++) {
			for (int z = -radius; z <= radius; z++) {
				Coordinates targetPosition = center + new Coordinates(x, 0, z);
				if (env.GetBlockAt(targetPosition) == null) {
					Block b = (Block)Instantiate(block);
					b.MoveTo(targetPosition);
					env.AddBlock(b);
				}
			}
		}
	}

	void Generate(AvatarCharacter avatar) {
		Vector3 pos = avatar.transform.position;
		pos.y = height;
		Coordinates c = new Coordinates(pos);
		Generate (c);
	}
}
