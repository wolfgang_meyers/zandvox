﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class IsolationSystem : MonoBehaviour, IBlockSystem, IPersistentBehavior, IEnumerable<Block> {

	//by default, contained blocks will be invisible
	public Vector3 scale = Vector3.zero;
	public float chunkLoadDelay = 0.1f;
	public int workloadChunkSize = 4 * 4 * 4;

	private IDictionary<Coordinates, Block> grid = new Dictionary<Coordinates, Block>();
	private PersistencePlugin persistencePlugin;
	private Plugin blocksPlugin;
	private GarbageCollector gc;

	private bool initialized = false;
	private List<PersistedObject> loading = null;

	public bool IsLoading {
		get {
			return loading != null;
		}
	}

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
			persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
			blocksPlugin = pluginRegistry.GetPlugin("Blocks");
			gc = GameObject.FindObjectOfType<GarbageCollector>();
		}
	}

	// Use this for initialization
	void Start () {
		Initialize();
	}

	void OnDisable() {
		foreach (Block block in grid.Values) {
			gc.Collect(block.gameObject);
		}
		grid = null;
	}

	public void BlockMoved(Block block, Coordinates lastPosition) {
		Initialize();
		Coordinates c = block.Position;
		if (grid.ContainsKey(c)) {
			DestroyBlock(grid[c]);
		}
		grid[c] = block;
		WakeUpNeighbors(lastPosition);
		WakeUpNeighbors(c);
	}
	
	private void WakeUpNeighbors(Coordinates c) {
		foreach(Coordinates neighborCoordinates in c.GetNeighbors()) {
			if (grid.ContainsKey(neighborCoordinates)) {
				grid[neighborCoordinates].SendMessage("WakeUp", SendMessageOptions.DontRequireReceiver);
			}
		}
	}
	
	public void RemoveBlock(Block block) {
		Initialize();
		Coordinates c = block.Position;
		if (grid.ContainsKey(c)) {
			grid.Remove(c);
		}
		WakeUpNeighbors(c);
	}
	
	public void DestroyBlock(Block block) {
		Initialize();
		RemoveBlock(block);
		//TODO: remove block from component
		gc.Collect(block.gameObject);
	}
	
	public void ClearBlocks() {
		Initialize();
		List<Coordinates> coordinates = new List<Coordinates>(grid.Keys);
		foreach (Coordinates c in coordinates) {
			Block block = grid[c];
			DestroyBlock(block);
		}
	}
	
	public void AddBlock(Block block) {
		Initialize();
		Coordinates c = block.Position;
		if (grid.ContainsKey(c)) {
			DestroyBlock(grid[c]);
		}
		grid[c] = block;
		block.InitEnvironment(this);
		block.transform.position = new Vector3(0, 0, float.MaxValue);
		MeshRenderer renderer = block.GetComponent<MeshRenderer>();
		if (renderer != null) {
			renderer.enabled = false;
		}
		WakeUpNeighbors(c);
	}
	
	public Block GetBlockAt(Coordinates c) {
		Initialize();
		if (grid.ContainsKey(c)) {
			return grid[c];
		}
		return null;
	}

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		Initialize();
		//prevents accidental data corruption from saving before initial
		//load was completed
		List<PersistedObject> serializedBlocks = loading == null 
			? persistencePlugin.SerializeElements<Block>(grid.Values) : loading;
		obj.SetData("IsolationSystem", serializedBlocks);
	}

	public void Restore (PersistedObject obj)
	{
		Initialize();
		ClearBlocks();
		loading = obj.GetData<List<PersistedObject>>("IsolationSystem");
		if (loading != null) {
			StartCoroutine(SlowRestore(loading));
		}
	}

	System.Collections.IEnumerator SlowRestore(List<PersistedObject> serializedBlocks) {
		List<List<PersistedObject>> workload = ZUtil.DivideWorkload(serializedBlocks, 4 * 4 * 4);
		foreach (List<PersistedObject> chunk in workload) {
			yield return new WaitForSeconds(chunkLoadDelay);
			//in case the user quits before the system has finished loading...
			if (grid == null) {
				yield break;
			}
			foreach (Block b in persistencePlugin.DeserializeElements<Block>(blocksPlugin, chunk)) {
				AddBlock(b);
				//b.transform.parent = transform;
				b.transform.localScale = Vector3.zero;
			}
		}
		loading = null;
	}

	#endregion

	#region IEnumerable implementation

	public IEnumerator<Block> GetEnumerator ()
	{
		return grid.Values.GetEnumerator();
	}

	System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator ()
	{
		return grid.Values.GetEnumerator();
	}

	#endregion
}
