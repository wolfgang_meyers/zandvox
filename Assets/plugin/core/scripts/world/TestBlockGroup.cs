﻿using UnityEngine;
using System.Collections.Generic;

public class TestBlockGroup : MonoBehaviour {

	private static System.Random rnd = new System.Random();
	private bool initialized = false;

	private string material;

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			List<ColorSpec> colors = new List<ColorSpec>();
			PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
			Plugin paintPlugin = pluginRegistry.GetPlugin("Paint");
			foreach(ISelectableElement element in paintPlugin) {
				colors.Add((ColorSpec)element);
			}
			material = colors[rnd.Next(colors.Count)].DisplayName;
		}
	}

	// Use this for initialization
	void Start () {
		Initialize();
	}
	
	void BlockAdded(Block block) {
		Initialize();
		//paint
		block.SendMessage("PaintObject", material);
	}
}
