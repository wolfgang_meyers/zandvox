﻿using UnityEngine;
using System.Collections;

public class IdentityInfo : MonoBehaviour, IPersistentBehavior {

	private string username = "_player";
	private Environment env;

	void Start() {
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		//TODO: load from global props
		Restore (env.PersistedWorldData);
	}

	public string UserName {
		get {
			return username;
		}
		set {
			username = value;
			//TODO: save to global props
			Save(env.PersistedWorldData);
		}
	}

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		PersistedBehaviorData data = new PersistedBehaviorData();
		data.SetStringValue("name", username);
		obj.SetData("IdentityInfo", data);
	}

	public void Restore (PersistedObject obj)
	{
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("IdentityInfo");
		if (data != null) {
			username = data.GetStringValue("name");
		}
	}

	#endregion
}
