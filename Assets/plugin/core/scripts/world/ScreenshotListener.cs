﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ScreenshotListener : MonoBehaviour {

	private InputPlugin inputPlugin;
	private Messages messages;

	// Use this for initialization
	void Start () {
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
		messages = GameObject.FindObjectOfType<Messages>();
		if (!Directory.Exists("screenshots")) {
			Directory.CreateDirectory("screenshots");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (inputPlugin.GetInputDown("Capture Screenshot")) {
			System.DateTime now = System.DateTime.Now;
			string filename = "screenshots" + Path.DirectorySeparatorChar
				+ now.ToString().Replace(":", ".").Replace("/", "-")
					+ ".png";
			Application.CaptureScreenshot(filename);
			messages.QueueMessage("Screenshot captured: " + filename);
		}
	}
}
