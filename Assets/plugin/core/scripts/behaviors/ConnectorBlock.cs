﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class ConnectorBlock : MonoBehaviour, IPersistentBehavior {
	
	public List<string> types = new List<string>();
	public List<string> excludedTypes = new List<string>();
	
	public List<Vector3> locations = new List<Vector3>();
	public List<GameObject> connectors = new List<GameObject>();

	//used to show saved connections in cases where no environment
	//is available - for instance, in a clipboard preview
	private List<int> connectorState = new List<int>();

	public GameObject defaultState;

	public Material connectorMaterial;
	public bool invert;
	
	//private List<GameObject> connectorInstances = new List<GameObject>();
	// Use this for initialization
	protected void Start () {
		WakeUp();
	}
	
//	void resetConnectors() {
//		foreach (GameObject connectorInstance in connectorInstances) {
//			Destroy (connectorInstance);
//		}
//		connectorInstances.Clear();
//	}
	
	protected virtual bool shouldConnect(Block block) {
		bool result = block != null &&
			(types.Count == 0 || types.Contains(block.DisplayName))
				&& !excludedTypes.Contains(block.DisplayName);
		if (invert) {
			result = !result;
		}
		return result;
	}

//	private void buildConnector(GameObject connector) {
//		GameObject connectorInstance = (GameObject)Instantiate(connector);
//		connectorInstance.transform.position += transform.position;
//		connectorInstance.transform.parent = transform;
//		if (connectorMaterial != null) {
//			MeshRenderer renderer = (MeshRenderer)connectorInstance.GetComponent(typeof(MeshRenderer));
//			if (renderer != null) {
//				renderer.material = connectorMaterial;
//			}
//		}
//		connectorInstances.Add(connectorInstance);
//	}

	private void refreshConnectorState() {
		Block block = (Block)GetComponent(typeof(Block));
		IBlockSystem env = block.Environment;
		connectorState.Clear();
		for (int i = 0; i < locations.Count && i < connectors.Count; i++) {
			Coordinates loc = new Coordinates(locations[i]);
			Block neighbor = env.GetBlockAt(block.Position + loc);
			if (shouldConnect(neighbor)) {
				connectorState.Add(i);
			}
		}
	}

	protected void WakeUp() {
		Block block = (Block)GetComponent(typeof(Block));
		IBlockSystem env = block.Environment;

		if (env != null) {
			refreshConnectorState();
		}
		MeshFilter mf = GetComponent<MeshFilter>();
		MeshRenderer mr = GetComponent<MeshRenderer>();
		List<GameObject> onConnectors = new List<GameObject>();
		foreach (int i in connectorState) {
			GameObject connector = connectors[i];
			onConnectors.Add(connector);
		}
		if (defaultState != null) {
			onConnectors.Add(defaultState);
		}

		CombineInstance[] cmb = new CombineInstance[onConnectors.Count];
		for (int i = 0; i < cmb.Length; i++) {
			MeshFilter connectorMf = onConnectors[i].GetComponent<MeshFilter>();
			cmb[i].mesh = connectorMf.sharedMesh;
			cmb[i].transform = connectorMf.transform.localToWorldMatrix;
		}
		mf.mesh = new Mesh();
		mf.mesh.CombineMeshes(cmb);
		mr.material = connectorMaterial;
		mr.castShadows = true;
		mr.receiveShadows = true;
	}

	#region IPersistentBehavior implementation
	public void Save (PersistedObject obj)
	{
		obj.SetData("ConnectorBlock", connectorState);
	}
	public void Restore (PersistedObject obj)
	{
		List<int> connectorState = obj.GetData<List<int>>("ConnectorBlock");
		if (connectorState != null) {
			this.connectorState.Clear();
			this.connectorState.AddRange(connectorState);
		}
	}
	#endregion
}
