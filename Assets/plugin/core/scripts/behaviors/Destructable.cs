﻿using UnityEngine;
using System.Collections.Generic;

public class Destructable : MonoBehaviour {

	public List<GameObject> damageIndicators = new List<GameObject>();

	public float maxHealth;
	private float health;
	private GameObject damageIndicator;

	// Use this for initialization
	void Start () {
		health = maxHealth;
	}

	void OnDestruction() {
		Block block = (Block)GetComponent(typeof(Block));
		block.Environment.DestroyBlock(block);
	}

	void HarvestResource(GameObject recipient) {
		Block block = (Block)GetComponent(typeof(Block));
		recipient.SendMessage("OnResourceHarvested", block.DisplayName, SendMessageOptions.DontRequireReceiver);
	}

	private void showDamage() {
		if (damageIndicator != null) {
			Destroy (damageIndicator);
			damageIndicator = null;
		}
		if (health < maxHealth) {
			float ratio = health / maxHealth;
			int i = (int)Mathf.Floor(ratio * damageIndicators.Count);
			damageIndicator = (GameObject)Instantiate(damageIndicators[i]);
			damageIndicator.transform.position = transform.position;
			damageIndicator.transform.parent = transform;
		}
	}

	void OnDamage(GameMessage message) {
		float amount = (float)message.message;
		health -= amount;
		if (health <= 0) {
			HarvestResource(message.sender);
			OnDestruction();
		} else {
			showDamage();
		}
	}
}
