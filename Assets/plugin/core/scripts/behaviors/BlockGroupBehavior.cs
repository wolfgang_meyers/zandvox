﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class BlockGroupBehavior : MonoBehaviour {

	public BlockGroup prototypeBlockGroup;

	private BlockGroup group;
	private Block block;

	public BlockGroup Group {
		get { return group; }
		set {
			group = value;
		}
	}

	public List<string> includedTypes = new List<string>();
	public List<string> excludedTypes = new List<string>();

	protected virtual bool ShouldInclude(Block block) {
		if (block != null) {
			BlockGroupBehavior groupBehavior = block.GetComponent<BlockGroupBehavior>();
			return groupBehavior != null &&
				(includedTypes.Count == 0 || includedTypes.Contains(block.DisplayName)) &&
					!excludedTypes.Contains(block.DisplayName);
		}
		return false;
	}

	// Use this for initialization
	void Start () {
		block = GetComponent<Block>();
			if (block.Environment != null && Group == null) {
			//groupMembers are blocks that will be in the same group
			//as this block
			List<BlockGroupBehavior> groupMembers = block.Position.GetNeighbors()
				.ConvertAll(c => block.Environment.GetBlockAt(c))
					.FindAll(b => ShouldInclude(b))
					.ConvertAll(b => b.GetComponent<BlockGroupBehavior>())
					.FindAll(b => b.Group != null).ToList();
			if (groupMembers.Count == 0) {
				Group = (BlockGroup)Instantiate(prototypeBlockGroup);
				Group.AddBlock(block);
			} else {
				BlockGroupBehavior groupMember = groupMembers[0];
				groupMember.Group.AddBlock(block);
				for (int i = 1; i < groupMembers.Count; i++) {
					groupMember = groupMembers[i];
					if (groupMember.Group != Group) {
						Group.Absorb(groupMember.Group);
					}
				}
			}
		}
	}

	void OnDisable() {
		if (Group != null) {
			Group.RemoveBlock(block);
		}
	}
}
