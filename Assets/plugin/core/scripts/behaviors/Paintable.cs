﻿using UnityEngine;
using System.Collections;

//TODO: allow separation of color and texture
public class Paintable : MonoBehaviour, IPersistentBehavior {

	private bool initialized = false;
	private PaintPlugin paintPlugin;
	//this is made public so that calls to Instantiate()
	//will carry over the material name, and everything
	//doesn't get painted the default color
	public string materialName;

	void Start() {
		Initialize();
	}

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
			paintPlugin = (PaintPlugin)pluginRegistry.GetPlugin("Paint");
			if (materialName == null || materialName == "") {
				materialName = paintPlugin.GetDefaultElement().DisplayName;
			}
			PaintObject(materialName);
		}
	}

	void PaintObject(string materialName) {
		Initialize();
		this.materialName = materialName;
		ColorSpec colorSpec = (ColorSpec)paintPlugin.GetElement(materialName);
		if (colorSpec != null) {
			gameObject.renderer.material = colorSpec.ToMaterial();
		}
	}

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		Initialize();
		PersistedBehaviorData data = new PersistedBehaviorData();
		data.SetStringValue("material", materialName);
		obj.SetData("Paintable", data);
	}

	public void Restore (PersistedObject obj)
	{
		Initialize();
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("Paintable");
		if (data != null) {
			string materialName = data.GetStringValue("material");
			PaintObject(materialName);
		}
	}

	#endregion
}
