﻿using UnityEngine;
using System.Collections.Generic;

public class MeshOptimizer : MonoBehaviour {

	private Block block;
	private new MeshRenderer renderer;
	private new Collider collider;

	// Use this for initialization
	void Start () {
		block = (Block)GetComponent(typeof(Block));
		renderer = GetComponent<MeshRenderer>();
		collider = GetComponent<Collider>();
		WakeUp();
//		block = (Block)GetComponent(typeof(Block));
//		if (connectorTemplate == null) {
//			Debug.Log("meshoptimizer cannot function without connector template");
//		} else {
//			renderer = (MeshRenderer)GetComponent(typeof(MeshRenderer));
//			renderer.enabled = false;
//			Material mat = renderer.material;
//			connectors = new List<GameObject>(connectorTemplate.connectors);
//			locations = new List<Vector3>(connectorTemplate.locations);
//			connectorMaterial = mat;
//		}
//		base.Start();
	}

//	protected override bool shouldConnect (Block block)
//	{
//		return block == null || block.GetComponent(typeof(MeshOptimizer)) == null;
//	}

	void WakeUp() {
		if ((renderer != null || collider != null) && block.Environment != null) {
			bool hide = true;
			foreach (Coordinates c in block.Position.GetNeighbors()) {
				Block neighbor = block.Environment.GetBlockAt(c);
			
				hide = hide && !(neighbor == null ||
				                 neighbor.GetComponent(typeof(MeshOptimizer)) == null);
			}
			//TODO: refactor to use chunks and move collision optimization elsewhere
			if (renderer != null) {
				renderer.enabled = !hide;
			}
			if (collider != null) {
				collider.enabled = !hide;
			}
		}
	}
}
