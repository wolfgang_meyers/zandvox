﻿using UnityEngine;
using System.Collections;

public class OrientOnInteraction : MonoBehaviour {

	void OnInteract(RaycastHit hit) {
		SendMessage("UpdateOrientation", hit.normal);
	}

}
