﻿using UnityEngine;
using System.Collections;

public class LabelBehavior : MonoBehaviour, IPersistentBehavior {

	public string label;
	public Color color = Color.white;

	// Use this for initialization
	void Start () {
		UpdateLabel(label);
	}

	void UpdateLabel(string value) {
		if (value == null) {
			value = string.Empty;
		}
		this.label = value;
		foreach (TextMesh tm in GetComponentsInChildren<TextMesh>()) {
			tm.text = value;
			tm.color = color;
		}
	}

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		obj.SetData("Label", label);
	}

	public void Restore (PersistedObject obj)
	{
		string value = obj.GetData<string>("Label");
		if (value != null) {
			UpdateLabel(value);
		}
	}

	#endregion
}
