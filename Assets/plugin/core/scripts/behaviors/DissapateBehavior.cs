﻿using UnityEngine;
using System.Collections;

public class DissapateBehavior : MonoBehaviour {
	
	public float delay = 0;
	private Block block;
	
	// Use this for initialization
	void Start () {
		block = (Block)GetComponent(typeof(Block));
		StartCoroutine("Dissapate");
	}
	
	IEnumerator Dissapate() {
		if (block.Environment != null) {
			yield return new WaitForSeconds(delay);
			block.Environment.DestroyBlock(block);
		}
	}
}
