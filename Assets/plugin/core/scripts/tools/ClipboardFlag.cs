﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The presence of this behavior in a GameObject
/// indicates that the game object is currently
/// serving as a clipboard preview, and should avoid executing
/// behaviors that would be inconsistent with a clipboard preview.
/// </summary>
public class ClipboardFlag : MonoBehaviour {

}
