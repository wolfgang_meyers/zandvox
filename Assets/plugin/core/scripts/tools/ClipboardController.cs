using UnityEngine;
using System.Collections.Generic;

public class ClipboardController : MonoBehaviour, IPersistentBehavior {

	private enum ClipboardMode {
		PreCapture,
		Capturing,
		Pasting
	}

	private ClipboardMode mode = ClipboardMode.PreCapture;
	private Coordinates captureStart;
	private Coordinates captureEnd;
	private FrameWizard frameWizard;
	private PluginRegistry pluginRegistry;
	private InputPlugin inputPlugin;

	public Material selectionCubeMaterial;
	public Material selectionGridMaterial;
	public float selectionCubeLineWidth = 0.05f;
	public float selectionGridLineWidth = 0.025f;

	public ClipboardUI clipboardUI;

	public float selectionVolumeLimit = 1000000;

	public EditorGrid editorGrid;
	public bool connectCorners = false;

	//this is the capture area
	private GameObject selectionDisplay;
	private Environment env;
	private GUIRegions guiRegions;
	private Messages messages;

	private List<PersistedClipboardData> clipboardData = new List<PersistedClipboardData>();
	//when pasting data, this is the parent object that will be
	//used to move around the previewed blocks
	private GameObject clipboardPreview;
	private PersistedClipboardData selectedClipboardData;
	private PersistencePlugin persistencePlugin;


	public void AddClip(PersistedClipboardData data) {
		clipboardData.Add(data);
		SaveClipData();
	}

	public PersistedClipboardData GetClip(int index) {
		return clipboardData[index];
	}

	public void RemoveClip(PersistedClipboardData data) {
		clipboardData.Remove(data);
		SaveClipData();
	}

	public void SaveClipData() {
		Save (env.PersistedWorldData);
	}

	public int ClipCount {
		get { return clipboardData.Count; }
	}

	public void PasteMode(PersistedClipboardData data) {
		Plugin blocks = pluginRegistry.GetPlugin("Blocks");
		DestroyPreview();
		selectedClipboardData = data;
		mode = ClipboardMode.Pasting;
		clipboardPreview = (GameObject)Instantiate(new GameObject());
		clipboardPreview.transform.position = Coordinates.Lerp(Coordinates.Parse(data.minCorner), Coordinates.Parse(data.maxCorner), 0.5f);
		//adjust
		//clipboardPreview.transform.position -= new Vector3(0.5f, 0.5f, 0.5f);
		List<Block> pastedBlocks = persistencePlugin.DeserializeElements<Block>(blocks, data.blocks);
		foreach (Block block in pastedBlocks) {
			block.transform.parent = clipboardPreview.transform;
			MeshRenderer mr = block.GetComponent<MeshRenderer>();
			if (mr != null) {
				mr.enabled = true;
			}
		}
		if (selectedClipboardData.rotation != null) {
			clipboardPreview.transform.Rotate(Coordinates.Parse(selectedClipboardData.rotation).ToVector());
		}
	}

	private void RotateSelectedClip(Vector3 eulerAngle) {
		clipboardPreview.transform.Rotate(eulerAngle);
		selectedClipboardData.rotation = (new Coordinates(clipboardPreview.transform.eulerAngles)).ToString();
	}

	private void ApplyRotation(PersistedClipboardData clip) {
		Vector3 eulerAngle = clip.rotation == null ? Vector3.zero : Coordinates.Parse(clip.rotation).ToVector();
		Coordinates center = Coordinates.Lerp(Coordinates.Parse(clip.minCorner), Coordinates.Parse(clip.maxCorner), 0.5f);
		foreach (PersistedObject obj in clip.blocks) {
			PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("Block");
			Coordinates position = data.GetCoordinatesValue("position");
			Coordinates newPosition = ZUtil.RotateAroundPoint(position, center, eulerAngle);
			Debug.Log("Clipboard rotate: position=" + position + ", newPosition=" + newPosition);
			data.ClearProperty("position");
			data.SetCoordinatesValue("position", newPosition);
			if (data.IsSet("forward")) {
				Vector3 forward = data.GetVector3Value("forward");
				data.ClearProperty("forward");
				data.SetVector3Value("forward", ZUtil.Rotate(forward, eulerAngle));
			} else {
				//backwards compatible with previous blocks that didn't store orientation
				PersistedBehaviorData orient = obj.GetData<PersistedBehaviorData>("OrientOnInteraction");
				if (orient != null && orient.IsSet("forward")) {
					Vector3 forward = orient.GetVector3Value("forward");
					data.ClearProperty("forward");
					data.SetVector3Value("forward", ZUtil.Rotate(forward, eulerAngle));
				}
			}
			obj.SetData<PersistedBehaviorData>("Block", data);
		}
	}

	private Coordinates FlipX(Coordinates c, Coordinates minCorner, Coordinates maxCorner) {
		return new Coordinates((maxCorner.X - c.X) + minCorner.X, c.Y, c.Z);
	}

	private Coordinates FlipY(Coordinates c, Coordinates minCorner, Coordinates maxCorner) {
		return new Coordinates(c.X, (maxCorner.Y - c.Y) + minCorner.Y, c.Z);
	}

	private Coordinates FlipZ(Coordinates c, Coordinates minCorner, Coordinates maxCorner) {
		return new Coordinates(c.X, c.Y, (maxCorner.Z - c.Z) + minCorner.Z);
	}

	private void FlipSelectedClip(System.Func<Coordinates, Coordinates, Coordinates, Coordinates> f) {
		Coordinates minCorner = Coordinates.Parse(selectedClipboardData.minCorner);
		Coordinates maxCorner = Coordinates.Parse(selectedClipboardData.maxCorner);
		foreach (PersistedObject obj in selectedClipboardData.blocks) {
			PersistedBehaviorData blockData = obj.GetData<PersistedBehaviorData>("Block");
			Coordinates position = blockData.GetCoordinatesValue("position");
			blockData.ClearProperty("position");
			blockData.SetCoordinatesValue("position", f(position, minCorner, maxCorner));
			obj.SetData("Block", blockData);
		}
	}


	private void Paste(Coordinates c) {
		PersistedClipboardData selectedClipboardData = ZUtil.DeepCopy(this.selectedClipboardData);
		ApplyRotation(selectedClipboardData);
		Plugin blocks = pluginRegistry.GetPlugin("Blocks");
		//TODO: refactor duplicated code
		List<Block> pastedBlocks = persistencePlugin.DeserializeElements<Block>(blocks, selectedClipboardData.blocks);
		foreach (Block block in pastedBlocks) {
			Coordinates oldCenter = Coordinates.Lerp(Coordinates.Parse(selectedClipboardData.minCorner), Coordinates.Parse(selectedClipboardData.maxCorner), 0.5f);
			block.MoveTo(block.Position - oldCenter + c);
			env.AddBlock(block);
		}
	}

	private void CancelCapture() {
		mode = ClipboardMode.PreCapture;
		BuildSelectionDisplay();
	}

	private void BeginCapture() {
		mode = ClipboardMode.Capturing;
		captureEnd = captureStart;
	}

	private void EndCapture() {
		List<Block> capturedBlocks = new List<Block>();
		for (int x = Mathf.Min(captureStart.X, captureEnd.X); x <= Mathf.Max(captureStart.X, captureEnd.X); x++) {
			for (int y = Mathf.Min (captureStart.Y, captureEnd.Y); y <= Mathf.Max(captureStart.Y, captureEnd.Y); y++) {
				for (int z = Mathf.Min (captureStart.Z, captureEnd.Z); z <= Mathf.Max(captureStart.Z, captureEnd.Z); z++) {
					Block block = env.GetBlockAt(new Coordinates(x, y, z));
					if (block != null) {
						capturedBlocks.Add(block);
					}
				}
			}
		}
		messages.QueueMessage("Captured " + capturedBlocks.Count + " blocks");
		PersistedClipboardData data = new PersistedClipboardData();

		//TODO: use persistence plugin to serialize blocks

		data.name = "Untitled " + this.clipboardData.Count;
		foreach (Block block in capturedBlocks) {
			PersistedObject obj = new PersistedObject();
			obj.objectType = block.displayName;
			Component[] persistedComponents = block.GetComponents(typeof(IPersistentBehavior));
			foreach (Component c in persistedComponents) {
				IPersistentBehavior persistedBehavior = (IPersistentBehavior)c;
				persistedBehavior.Save(obj);
			}
			data.blocks.Add(obj);
		}
		data.minCorner = Coordinates.Minimize(captureStart, captureEnd).ToString();
		data.maxCorner = Coordinates.Maximize(captureStart, captureEnd).ToString();
		clipboardData.Add(data);
		mode = ClipboardMode.PreCapture;
		BuildSelectionDisplay();
		//TODO: capture data
	}

	private void BuildSelectionDisplay() {
		DestroySelectionDisplay();
		float diffX = Mathf.Abs(captureEnd.X - captureStart.X);
		float diffY = Mathf.Abs(captureEnd.Y - captureStart.Y);
		float diffZ = Mathf.Abs(captureEnd.Z - captureStart.Z);

		//make sure this selection doesn't accidentally blow up memory
		if (diffX * diffY * diffZ < selectionVolumeLimit) {


			float minX = Mathf.Min(captureStart.X, captureEnd.X) - 0.5f;
			float minY = Mathf.Min(captureStart.Y, captureEnd.Y) - 0.5f;
			float minZ = Mathf.Min(captureStart.Z, captureEnd.Z) - 0.5f;
			
			float maxX = Mathf.Max(captureStart.X, captureEnd.X) + 0.5f;
			float maxY = Mathf.Max(captureStart.Y, captureEnd.Y) + 0.5f;
			float maxZ = Mathf.Max(captureStart.Z, captureEnd.Z) + 0.5f;

			selectionDisplay = frameWizard.BuildGridCube(new Vector3(minX, minY, minZ),
			                                         new Vector3(maxX, maxY, maxZ),
			                                             selectionCubeLineWidth, selectionCubeMaterial,
		                                             selectionGridLineWidth, selectionGridMaterial);
		}
	}

	private void DestroyPreview() {
		if (clipboardPreview != null) {
			Destroy (clipboardPreview);
			clipboardPreview = null;
		}
	}

	private void DestroySelectionDisplay() {
		if (selectionDisplay != null) {
			Destroy (selectionDisplay);
			selectionDisplay = null;
		}
	}

	public void CaptureMode() {
		mode = ClipboardMode.PreCapture;
		DestroyPreview();
	}

	// Use this for initialization
	void Start () {
		pluginRegistry = (PluginRegistry)GameObject.FindObjectOfType(typeof(PluginRegistry));
		persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
		inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
		editorGrid = (EditorGrid)Instantiate(editorGrid);
		frameWizard = (FrameWizard)GameObject.FindObjectOfType(typeof(FrameWizard));
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		messages = (Messages)GameObject.FindObjectOfType(typeof(Messages));

		clipboardUI = (ClipboardUI)Instantiate(clipboardUI);
		Restore (env.PersistedWorldData);
	}

	void OnDisable() {
		Save (env.PersistedWorldData);
		Destroy (clipboardUI);
		Destroy(editorGrid);
		DestroySelectionDisplay();
		DestroyPreview();
	}
	
	// Update is called once per frame
	void Update () {
		Plane plane = editorGrid.getPlane();
		if (!guiRegions.IsMouseOverRegion()) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float distance = 0;
			if (plane.Raycast(ray, out distance)) {
				Coordinates c = new Coordinates(ray.GetPoint(distance));
				switch (mode) {
				case ClipboardMode.PreCapture:
					if (c != this.captureStart) {
						this.captureStart = c;
						this.captureEnd = c;
						BuildSelectionDisplay();
					}
					break;
				case ClipboardMode.Capturing:
					if (c != this.captureEnd) {
						this.captureEnd = c;
						BuildSelectionDisplay();
					}
					break;
				case ClipboardMode.Pasting:
					clipboardPreview.transform.position = c.ToVector();
					//rotate
					if (inputPlugin.GetInputDown("Rotate Selection Y")) {
						RotateSelectedClip(new Vector3(0, -90, 0));
					}
					if (inputPlugin.GetInputDown("Rotate Selection Y (reverse)")) {
						RotateSelectedClip(new Vector3(0, 90, 0));
					}
					if (inputPlugin.GetInputDown("Rotate Selection Z")) {
						RotateSelectedClip(new Vector3(0, 0, 90));
					}
					if (inputPlugin.GetInputDown("Rotate Selection Z (reverse)")) {
						RotateSelectedClip(new Vector3(0, 0, -90));
					}
					//flip

					if (inputPlugin.GetInputDown("Flip Selection X")) {
						FlipSelectedClip(FlipX);
						PasteMode(selectedClipboardData);
					}
					if (inputPlugin.GetInputDown("Flip Selection Y")) {
						FlipSelectedClip(FlipY);
						PasteMode(selectedClipboardData);
					}
					if (inputPlugin.GetInputDown("Flip Selection Z")) {
						FlipSelectedClip(FlipZ);
						PasteMode(selectedClipboardData);
					}

					break;
				}
				if (Input.GetMouseButtonDown(0)) {
					switch(mode) {
					case ClipboardMode.Capturing:
						EndCapture();
						break;
					case ClipboardMode.PreCapture:
						BeginCapture();
						break;
					case ClipboardMode.Pasting:
						Paste(c);
						break;
					}
				}
			}
		}
	}

	public void Save (PersistedObject obj)
	{
		obj.SetData("Clipboard", clipboardData);
	}
	
	public void Restore (PersistedObject obj)
	{
		this.clipboardData.Clear();
		List<PersistedClipboardData> clipboardData = obj.GetData<List<PersistedClipboardData>>("Clipboard");
		if (clipboardData != null) {
			this.clipboardData.AddRange(clipboardData);
		}
	}
}
