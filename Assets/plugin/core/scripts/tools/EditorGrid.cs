﻿using UnityEngine;
using System.Collections;

public class EditorGrid : MonoBehaviour, IPersistentBehavior {
	
	private GUIRegions guiRegions;
	private GameObject grid;
	public Material gridMaterial;
	public float gridLineWidth = 0.02f;
	public int gridSize = 20;
	private CameraGuide cameraGuide;
	private FrameWizard frameWizard;
	private Environment env;

	private Messages messages;
	private InputPlugin inputPlugin;

	//saves previous position between axis changes,
	//and between game save/load
	private Vector3 savedPosition = Vector3.zero;

	// Use this for initialization
	void Start () {
		messages = (Messages)GameObject.FindObjectOfType(typeof(Messages));
		cameraGuide = (CameraGuide)GameObject.FindObjectOfType(typeof(CameraGuide));
		frameWizard = (FrameWizard)GameObject.FindObjectOfType(typeof(FrameWizard));
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
		Restore (env.PersistedWorldData);
		buildGrid();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 camPos = cameraGuide.transform.position;
		if (inputPlugin.GetInputDown("Select Grid X")) {
			//transform.position = wirecube.transform.position;
			transform.forward = Vector3.right;
			transform.position = new Vector3(savedPosition.x, camPos.y, camPos.z);
			messages.QueueMessage("Grid set to Y-Z plane");
		}
		if (inputPlugin.GetInputDown("Select Grid Y")) {
			//transform.position = wirecube.transform.position;
			transform.forward = Vector3.up;
			transform.position = new Vector3(camPos.x, savedPosition.y, camPos.z);
			messages.QueueMessage("Grid set to X-Z plane");
		}
		if (inputPlugin.GetInputDown("Select Grid Z")) {
			//transform.position = wirecube.transform.position;
			transform.forward = Vector3.forward;
			transform.position = new Vector3(camPos.x, camPos.y, savedPosition.z);
			messages.QueueMessage("Grid set to X-Y plane");
		}
		
		//follow camera
		float x = transform.forward == Vector3.right ? savedPosition.x : camPos.x;
		float y = transform.forward == Vector3.up ? savedPosition.y : camPos.y;
		float z = transform.forward == Vector3.forward ? savedPosition.z : camPos.z;
		//round
		transform.position = new Coordinates(new Vector3(x, y, z)).ToVector();
		
		float gridMovement = Input.GetAxisRaw("Mouse ScrollWheel");
		if (Mathf.Abs(gridMovement) > 0.01f) {
			if (gridMovement > 0) {
				transform.position += transform.forward;
				savedPosition += transform.forward;
			} else {
				transform.position += -transform.forward;
				savedPosition += -transform.forward;
			}

			Save(env.PersistedWorldData);
		}
	}
	
	private void buildGrid() {
		if (grid == null) {
			grid = frameWizard.BuildGridCube(
				new Vector3(-gridSize / 2 - 0.5f, -0.5f, -gridSize / 2 - 0.5f),
				new Vector3(gridSize / 2 + 0.5f, 0.5f, gridSize / 2 + 0.5f),
				gridLineWidth, gridMaterial, gridLineWidth, gridMaterial);
			grid.transform.parent = transform;
			grid.transform.forward = grid.transform.up;
		}
		transform.forward = Vector3.up;
	}
	
	void OnDisable() {
		destroyGrid();
	}
	
	private void destroyGrid() {
		if (grid != null) {
			Destroy(grid);
			grid = null;
		}
	}
	
	public Plane getPlane() {
		Vector3 uiPlaneNormal = transform.forward;
		Vector3 uiPlaneDepth = transform.position;
		
		return new Plane(uiPlaneNormal, uiPlaneDepth);
	}

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		PersistedBehaviorData gridData = new PersistedBehaviorData();
		gridData.SetVector3Value("savedPosition", savedPosition);
		obj.SetData("EditorGrid", gridData);
	}

	public void Restore (PersistedObject obj)
	{
		PersistedBehaviorData gridData = obj.GetData<PersistedBehaviorData>("EditorGrid");
		if (gridData != null) {
			if (gridData.IsSet("savedPosition")) {
				savedPosition = gridData.GetVector3Value("savedPosition");
			}
		}
	}

	#endregion
}
