﻿using UnityEngine;
using System.Collections;

public class GameModeTool : MonoBehaviour {

	public PluginElementSelector gameModeSelector;

	// Use this for initialization
	void Start () {
		gameModeSelector = (PluginElementSelector)Instantiate(gameModeSelector);
	}

	void OnDisable() {
		Destroy (gameModeSelector);
	}
}
