﻿using UnityEngine;
using System.Collections;

public class LabelTool : MonoBehaviour {

	public GenericInputMenu inputMenu;

	private FrameWizard frameWizard;
	private GUIRegions guiRegions;
	private Environment env;

	private GameObject cursor;

	void OnDisable() {
		destroyCursor();
	}
	
	void createCursor() {
		if (cursor == null) {
			cursor = frameWizard.BuildCube(new Vector3(-0.5f, -0.5f, -0.5f), new Vector3(0.5f, 0.5f, 0.5f));
		}
	}
	
	void destroyCursor() {
		if (cursor != null) {
			Destroy(cursor);
			cursor = null;
		}
	}

	// Use this for initialization
	void Start () {
		guiRegions = GameObject.FindObjectOfType<GUIRegions>();
		env = GameObject.FindObjectOfType<Environment>();
		frameWizard = GameObject.FindObjectOfType<FrameWizard>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!guiRegions.IsMouseOverRegion()) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)) {
				Coordinates c = new Coordinates(hit.transform.position);
				Block b = env.GetBlockAt(c);
				if (b != null && b.GetComponent<LabelBehavior>() != null) {
					createCursor();
					cursor.transform.position = c.ToVector();
					if (Input.GetMouseButtonDown(0) ) {
						GenericInputMenu menuInstance = (GenericInputMenu)Instantiate(inputMenu);
						menuInstance.SendMessage("PropagateContext", b);
					}
				} else {
					destroyCursor();
				}
			} else {
				destroyCursor();
			}
		}
	}
}
