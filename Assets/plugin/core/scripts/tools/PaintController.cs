using UnityEngine;
using System;
using System.Collections.Generic;

public class PaintController : MonoBehaviour {

	public Material cursorMaterial;
	public Material cursorGridMaterial;
	public float cursorLineWidth = 0.05f;
	public float cursorGridLineWidth = 0.025f; 
	
	private GameObject cursor;
	
	public EditorGrid editorGrid;
	
	private GUIRegions guiRegions;
	private FrameWizard frameWizard;
	private DateTime lastBrushUpdate = DateTime.Now;
	
	//gui items
	public PluginElementSelector blockTypeSelector;
	public PaintOptions paintOptions;
	
	private GameObject grid;
	private Block selectedBlock = null;
	
	private Environment env;
	private InputPlugin inputPlugin;

	private Vector3 cursorOrientation = Vector3.forward;
	
	// Use this for initialization
	void Start () {
		env = GameObject.FindObjectOfType<Environment>();
		guiRegions = GameObject.FindObjectOfType<GUIRegions>();
		frameWizard = GameObject.FindObjectOfType<FrameWizard>();
		blockTypeSelector = (PluginElementSelector)Instantiate(blockTypeSelector);
		blockTypeSelector.preferences = env.PersistedWorldData;
		this.selectedBlock = (Block)blockTypeSelector.SelectedElement;
		editorGrid = (EditorGrid)Instantiate(editorGrid);
		paintOptions = (PaintOptions)Instantiate(paintOptions);
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
		buildCursor();
	}

	/*
	public void PasteMode(PersistedClipboardData data) {
		Plugin blocks = pluginRegistry.GetPlugin("Blocks");
		DestroyPreview();
		selectedClipboardData = data;
		mode = ClipboardMode.Pasting;
		clipboardPreview = (GameObject)Instantiate(new GameObject());
		clipboardPreview.transform.position = Coordinates.Lerp(Coordinates.parse(data.minCorner), Coordinates.parse(data.maxCorner), 0.5f);
		//adjust
		//clipboardPreview.transform.position -= new Vector3(0.5f, 0.5f, 0.5f);
		List<Block> pastedBlocks = persistencePlugin.DeserializeElements<Block>(blocks, data.blocks);
		foreach (Block block in pastedBlocks) {
			block.transform.parent = clipboardPreview.transform;
			MeshRenderer mr = block.GetComponent<MeshRenderer>();
			if (mr != null) {
				mr.enabled = true;
			}
		}
		if (selectedClipboardData.rotation != null) {
			clipboardPreview.transform.Rotate(Coordinates.parse(selectedClipboardData.rotation).ToVector());
		}
	}
	 */

	private void buildCursor() {
		Vector3 cursorPos = Vector3.zero;
		if (cursor != null) {
			cursorPos = cursor.transform.position;
			Destroy (cursor);
			cursor = null;
		}
		/*
selectionDisplay = frameWizard.BuildGridCube(new Vector3(minX, minY, minZ),
			                                         new Vector3(maxX, maxY, maxZ),
			                                             selectionCubeLineWidth, selectionCubeMaterial,
		                                             selectionGridLineWidth, selectionGridMaterial);
		 */

		int brushX = paintOptions.brushSizeX;
		int brushY = paintOptions.brushSizeY;
		int brushZ = paintOptions.brushSizeZ;

		cursor = frameWizard.BuildGridCube(new Vector3(-brushX - 0.5f, -brushY - 0.5f, -brushZ - 0.5f),
		                                   new Vector3(brushX + 0.5f, brushY + 0.5f, brushZ + 0.5f),
		                                   cursorLineWidth, cursorMaterial,
		                                   cursorGridLineWidth, cursorGridMaterial);
		cursor.transform.position = cursorPos;
		cursor.transform.rotation = Quaternion.Euler(Vector3.zero);
		//to support orienting one-block cursor
		if (selectedBlock != null && selectedBlock.orientable && brushX == 0 && brushY == 0 && brushZ == 0) {
			Block b = (Block)Instantiate(selectedBlock);
			b.transform.parent = cursor.transform;
			b.transform.localPosition = Vector3.zero;
			b.UpdateOrientation(cursorOrientation);
		}
	}
	
	void OnDisable() {
		Destroy (blockTypeSelector);
		Destroy (editorGrid);
		Destroy (cursor);
		Destroy (paintOptions);
	}
	
	private void paint(Coordinates center) {
		int brushX = paintOptions.brushSizeX;
		int brushY = paintOptions.brushSizeY;
		int brushZ = paintOptions.brushSizeZ;
		for (int x = -brushX; x <= brushX; x++) {
			for (int y = -brushY; y <= brushY; y++) {
				for (int z = -brushZ; z <= brushZ; z++) {
					Coordinates c;
					c = new Coordinates(x + center.X, y + center.Y, z + center.Z);
					
					Block existing = env.GetBlockAt(c);
					if (existing == null || existing.displayName != blockTypeSelector.SelectedElement.DisplayName) {
						Block b = (Block)Instantiate(selectedBlock);
						b.MoveTo(c);
						env.AddBlock(b);
						b.UpdateOrientation(cursorOrientation);
					}
				}
			}
		}
	}

	private bool HandleCursorRotation() {
		bool rotateY = false;
		bool rotateZ = false;
		if (inputPlugin.GetInputDown("Rotate Selection Y")) {
			rotateY = true;
			cursorOrientation = ZUtil.Rotate(cursorOrientation, new Vector3(0, -90, 0));
		} else if (inputPlugin.GetInputDown("Rotate Selection Y (reverse)")) {
			rotateY = true;
			cursorOrientation = ZUtil.Rotate(cursorOrientation, new Vector3(0, 90, 0));
		} else if (inputPlugin.GetInputDown("Rotate Selection Z")) {
			rotateZ = true;
			cursorOrientation = ZUtil.Rotate(cursorOrientation, new Vector3(0, 0, 90));
		} else if (inputPlugin.GetInputDown("Rotate Selection Z (reverse)")) {
			rotateZ = true;
			cursorOrientation = ZUtil.Rotate(cursorOrientation, new Vector3(0, 0, -90));
		}

		if (rotateY) {
			int brushX = paintOptions.brushSizeX;
			paintOptions.brushSizeX = paintOptions.brushSizeY;
			paintOptions.brushSizeY = brushX;
		}
		if (rotateZ) {
			int brushX = paintOptions.brushSizeX;
			paintOptions.brushSizeX = paintOptions.brushSizeZ;
			paintOptions.brushSizeZ = brushX;
		}
		return rotateY || rotateZ;
	}

	// Update is called once per frame
	void Update () {
		bool newCursor = HandleCursorRotation();

		newCursor = newCursor || lastBrushUpdate != paintOptions.lastUpdate || selectedBlock != (Block)blockTypeSelector.SelectedElement;
		selectedBlock = (Block)blockTypeSelector.SelectedElement;
		if (newCursor) {
			lastBrushUpdate = paintOptions.lastUpdate;
			buildCursor();
		}
		Plane plane = editorGrid.getPlane();
		//cursor.transform.forward = plane.normal;
		if (!guiRegions.IsMouseOverRegion()) {
			
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float distance = 0;
			if (plane.Raycast(ray, out distance)) {
				Coordinates c = new Coordinates(ray.GetPoint(distance));
				
				cursor.transform.position = c.ToVector();
				
				if (Input.GetMouseButton(0)) {
					paint(c);
				}
			}
		}
	}
}
