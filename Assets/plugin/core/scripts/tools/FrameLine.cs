using System;
using UnityEngine;

public struct FrameLine
{
	public Vector3 p1;
	public Vector3 p2;

	public FrameLine(Vector3 p1, Vector3 p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
}


