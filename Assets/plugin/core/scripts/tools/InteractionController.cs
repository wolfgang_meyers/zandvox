﻿using UnityEngine;
using System.Collections;

public class InteractionController : MonoBehaviour {
	
	private Environment env;
	
	public GameObject wirecube;
	
	private GameObject cursor;
	private GUIRegions guiRegions;
	
	// Use this for initialization
	void Start () {
		env = GameObject.FindObjectOfType<Environment>();
		guiRegions = GameObject.FindObjectOfType<GUIRegions>();
	}

	void OnDisable() {
		destroyCursor();
	}
	
	void createCursor() {
		if (cursor == null) {
			cursor = (GameObject)Instantiate(wirecube);
		}
	}
	
	void destroyCursor() {
		if (cursor != null) {
			Destroy(cursor);
			cursor = null;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!guiRegions.IsMouseOverRegion()) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)) {
				createCursor();
				Coordinates c = new Coordinates(hit.transform.position);
				cursor.transform.position = c.ToVector();
				Block b = env.GetBlockAt(c);
				if (b != null) {
					//TODO: show wirecube/cursor
					if (Input.GetMouseButtonDown(0)) {
						b.SendMessage("OnInteract", hit, SendMessageOptions.DontRequireReceiver);
					}
				}
			} else {
				destroyCursor();
			}
		}
	}
}
