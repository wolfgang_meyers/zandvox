﻿using UnityEngine;
using System.Collections.Generic;

public class FrameWizard : MonoBehaviour {

	public GameObject framePrimitive;
	public Material defaultMaterial;
	public float defaultLineWidth;

	//just an empty prototype object to instantiate
	//grid line containers
	private GameObject framePrototype;

	void Start() {
		framePrototype = new GameObject();
	}

	private void ExpandCube(List<Vector3> points, Vector3 direction) {
		List<Vector3> addList = new List<Vector3>();
		foreach (Vector3 p in points) {
			Vector3 newPoint = p + direction;
			addList.Add(newPoint);
		}
		points.AddRange(addList);
	}

	private bool ShouldConnect(Vector3 p1, Vector3 p2) {
		int diff = 0;
		if (p1.x != p2.x) diff++;
		if (p1.y != p2.y) diff++;
		if (p1.z != p2.z) diff++;
		return diff == 1;
	}

	private bool ShouldConnect(FrameLine l1, FrameLine l2) {
		Vector3 diff1 = l1.p2 - l1.p1;
		Vector3 diff2 = l2.p2 - l2.p1;
		return (diff1 == diff2 || diff1 == -diff2)
			&& ShouldConnect(l1.p1, l2.p1)
				&& ShouldConnect(l1.p2, l2.p2);
	}

	private List<FrameLine> ConnectCubePoints(List<Vector3> points) {
		List<FrameLine> result = new List<FrameLine>();
		for (int i = 0; i < points.Count; i++) {
			for (int j = i + 1; j < points.Count; j++) {
				Vector3 p1 = points[i];
				Vector3 p2 = points[j];
				if (ShouldConnect(p1, p2)) {
					result.Add(new FrameLine(p1, p2));
				}
			}
		}
		return result;
	}

	private List<FrameLine> BuildCubeLines(Vector3 p1, Vector3 p2) {
		Vector3 diff = p2 - p1;
		List<Vector3> points = new List<Vector3>();
		points.Add(p1);
		ExpandCube(points, new Vector3(diff.x, 0, 0));
		ExpandCube(points, new Vector3(0, diff.y, 0));
		ExpandCube(points, new Vector3(0, 0, diff.z));
		List<FrameLine> lines = ConnectCubePoints(points);
		return lines;
	}

	private List<FrameLine> BuildGridFace(FrameLine l1, FrameLine l2, float interval) {
		List<FrameLine> face = new List<FrameLine>();
		float len = (l1.p2 - l1.p1).magnitude;
		for (float f = interval; f < len; f += interval) {
			Vector3 p1 = Vector3.Lerp(l1.p1, l1.p2, f / len);
			Vector3 p2 = Vector3.Lerp(l2.p1, l2.p2, f / len);
			face.Add(new FrameLine(p1, p2));
		}
		return face;
	}

	private List<FrameLine> BuildGrid(List<FrameLine> lines, float interval) {
		List<FrameLine> result = new List<FrameLine>();
		for (int i = 0; i < lines.Count; i++) {
			for (int j = i + 1; j < lines.Count; j++) {
				FrameLine l1 = lines[i];
				FrameLine l2 = lines[j];
				if (ShouldConnect(l1, l2)) {
					result.AddRange(BuildGridFace(l1, l2, interval));
				}
			}
		}
		return result;
	}

	public GameObject BuildGridCube(Vector3 p1, Vector3 p2) {
		return BuildGridCube(p1, p2, defaultLineWidth, defaultMaterial, defaultLineWidth, defaultMaterial);
	}

	public GameObject BuildGridCube(Vector3 p1, Vector3 p2,
			                                float lineWidth, Material material,
			                                float gridLineWidth, Material gridMaterial) {
		List<FrameLine> cubeFrame = BuildCubeLines(p1, p2);
		//TODO: configurable interval
		List<FrameLine> gridFrame = BuildGrid(cubeFrame, 1);
		GameObject result = BuildFrame(cubeFrame, lineWidth, material);
		BuildFrame(result, gridFrame, gridLineWidth, gridMaterial);
		return result;
	}

	public GameObject BuildCube(Vector3 p1, Vector3 p2) {
		return BuildCube(p1, p2, defaultLineWidth, defaultMaterial);
	}

	public GameObject BuildCube(Vector3 p1, Vector3 p2, float lineWidth, Material material) {
		List<FrameLine> lines = BuildCubeLines(p1, p2);
		return BuildFrame(lines, lineWidth, material);
	}

	public GameObject BuildFrame(List<FrameLine> lines) {
		return BuildFrame(lines, defaultLineWidth, defaultMaterial);
	}

	public GameObject BuildFrame(List<FrameLine> lines, float lineWidth, Material material) {
		GameObject result = (GameObject)Instantiate(framePrototype);
		BuildFrame(result, lines, lineWidth, material);
		return result;
	}

	public void BuildFrame(GameObject container, List<FrameLine> lines, float lineWidth, Material material) {
		foreach (FrameLine frameLine in lines) {
			Vector3 diff = frameLine.p2 - frameLine.p1;
			GameObject gridline = (GameObject)Instantiate(framePrimitive);
			gridline.transform.position = frameLine.p1 + (diff / 2f);
			gridline.transform.localScale = new Vector3(lineWidth, lineWidth, diff.magnitude);
			gridline.transform.forward = diff.normalized;
			gridline.transform.parent = container.transform;
			gridline.renderer.material = material;
		}
	}

	

}
