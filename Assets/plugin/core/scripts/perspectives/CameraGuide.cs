﻿using UnityEngine;
using System.Collections;
using System;

public class CameraGuide : MonoBehaviour, IPersistentBehavior {
	
	public float velocity = 0.5f;
	private bool faster = false;
	private MouseLook mouseLook;
	private Messages messages;
	private Environment env;

	private DateTime lastSave = DateTime.Now;
	private InputPlugin inputPlugin;

	private const string StrafeLeft = "Camera - Strafe Left";
	private const string StrafeRight = "Camera - Strafe Right";
	private const string Forward = "Camera - Forward";
	private const string Backward = "Camera - Backward";
	private const string MovementSpeed = "Camera - Toggle Movement Speed";
	private const string IncreaseElevation = "Camera - Increase Elevation";
	private const string DecreaseElevation = "Camera - Decrease Elevation";
	private const string Mouselook = "Camera - Toggle Mouselook";
	
	// Use this for initialization
	void Start () {
		mouseLook = (MouseLook)GetComponent(typeof(MouseLook));
		messages = (Messages)GameObject.FindObjectOfType(typeof(Messages));
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		Restore(env.PersistedWorldData);

		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
	}
	
	// Update is called once per frame
	void Update () {
		processInput();
		autosave();
		if (Input.GetMouseButtonDown(1)) {
			mouseLook.enabled = true;
		}
		if (Input.GetMouseButtonUp(1)) {
			mouseLook.enabled = false;
		}
	}

	private void autosave() {
		if (DateTime.Now - lastSave >= TimeSpan.FromSeconds(10)) {
			lastSave = DateTime.Now;
			Save(env.PersistedWorldData);
		}
	}

	private Vector3 flatForward() {
		Vector3 forward = transform.forward;
		Vector3 result = new Vector3(forward.x, 0, forward.z).normalized;
		return result;
	}
	
	private void processInput() {
		float speed = this.velocity;
		if (inputPlugin.GetInputDown(MovementSpeed)) {
			faster = !faster;
			if (faster) {
				messages.QueueMessage("Movement speed increased");
			} else {
				messages.QueueMessage("Movement speed decreased");
			}
		}
		if (faster) {
			speed *= 5;
		}
		if (inputPlugin.GetInput(StrafeLeft)) {
			transform.position += -speed * transform.right;
		}
		if (inputPlugin.GetInput(StrafeRight)) {
			transform.position += speed * transform.right;
		}
		if (inputPlugin.GetInput(Forward)) {
			transform.position += speed * flatForward();
		}
		if (inputPlugin.GetInput(Backward)) {
			transform.position += -speed * flatForward();
		}
		if (inputPlugin.GetInput(IncreaseElevation)) {
			transform.position += speed * Vector3.up;
		}
		if (inputPlugin.GetInput(DecreaseElevation)) {
			transform.position += -speed * Vector3.up;
		}
		
		if (inputPlugin.GetInputDown(Mouselook)) {
			mouseLook.enabled = !mouseLook.enabled;
		}
	}

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		PersistedBehaviorData data = new PersistedBehaviorData();
		data.SetVector3Value("position", transform.position);
		data.SetVector3Value("forward", transform.forward);
		obj.SetData("EditorCamera", data);
	}

	public void Restore (PersistedObject obj)
	{
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("EditorCamera");
		if (data != null) {
			transform.position = data.GetVector3Value("position");
			transform.forward = data.GetVector3Value("forward");
		}
	}

	#endregion
}
