﻿using UnityEngine;
using System.Collections;

public class EditorController : PerspectiveController {

	#region IPerspectiveController implementation

	public override void DisableController ()
	{
		((MonoBehaviour)GetComponent(typeof(CameraGuide))).enabled = false;
		((MonoBehaviour)GetComponent(typeof(MouseLook))).enabled = false;
	}

	public override void EnableController ()
	{
		Debug.Log("EditorController enabled");
		((MonoBehaviour)GetComponent(typeof(CameraGuide))).enabled = true;
		//((MonoBehaviour)GetComponent(typeof(MouseLook))).enabled = true;
	}

	public override Vector3 Position {
		get {
			return transform.position;
		}

		set {
			transform.position = value;
		}
	}

	#endregion
}
