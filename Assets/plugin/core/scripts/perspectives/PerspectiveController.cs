﻿using UnityEngine;
using System.Collections;

public abstract class PerspectiveController : MonoBehaviour, IPerspectiveController {


	#region IPerspectiveController implementation

	public virtual void DisableController ()
	{

	}

	public virtual void EnableController ()
	{

	}

	public abstract Vector3 Position {
		get;
		set;
	}
	#endregion


}
