﻿using UnityEngine;
using System.Collections.Generic;

public class GUIBox : GUIElement {

	public string title;
	private List<GUIElement> children = new List<GUIElement>();

	public void AddChild(GUIElement element) {
		children.Add(element);
		element.parent = this;
	}

	public void RemoveChild(GUIElement element) {
		children.Remove(element);
		element.parent = null;
	}

	public IEnumerable<GUIElement> GetChildren() {
		return children;
	}

	void OnDisable() {
		foreach (GUIElement child in children) {
			try {
				Destroy (child);
			} catch (System.Exception) { }
		}
	}

	public override void Render ()
	{
		Rect position = GetAbsolutePosition();
		GUI.Box(position, title);
		foreach (GUIElement child in children) {
			child.Render();
		}
	}

	public override void Save (PersistedObject obj)
	{
		base.Save (obj);
		List<PersistedObject> savedChildren = new List<PersistedObject>();
		foreach (GUIElement child in children) {
			PersistedObject childObj = new PersistedObject();
			childObj.objectType = child.DisplayName;
			child.Save(childObj);
			savedChildren.Add(childObj);
		}
		obj.SetChildren("GUIBox.children", savedChildren);
		obj.SetData("GUIBox.title", title);
	}

	public override void Restore (PersistedObject obj)
	{
		base.Restore (obj);
		if (obj.children != null && obj.children.ContainsKey("GUIBox.children")) {
			//accessing persistence plugin api here is a little hacky... but not as bad
			//as creating a special case for child elements in the persistence plugin implementation
			PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
			PersistencePlugin persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
			Plugin guiPlugin = pluginRegistry.GetPlugin("GUI");
			List<GUIElement> children = persistencePlugin.DeserializeElements<GUIElement>(guiPlugin, obj.GetChildren("GUIBox.children"));
			foreach (GUIElement child in children) {
				AddChild(child);
			}
		}


	}

}
