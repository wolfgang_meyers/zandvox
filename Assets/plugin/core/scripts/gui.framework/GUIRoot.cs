using UnityEngine;
using System.Collections.Generic;

public class GUIRoot : MonoBehaviour {

	public List<GUIElement> elements = new List<GUIElement>();

	// Use this for initialization
	void Start () {
	
	}
	
	void OnGUI() {
		GUIUtil.screenMatrix();
		foreach (GUIElement element in elements) {
			element.Render();
		}
	}
}
