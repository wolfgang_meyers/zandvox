﻿using UnityEngine;
using System.Collections;

public class GUIButton : GUIElement {

	public string text;
	public GameObject clickListener;

	public override void Render ()
	{
		Rect position = GetAbsolutePosition();
		if (GUI.Button(position, text) && clickListener != null) {
			clickListener.SendMessage("OnClick", this);
		}
	}

	public override void Restore (PersistedObject obj)
	{
		base.Restore (obj);
		string text = obj.GetData<string>("GUIButton.text");
		if (text != null) {
			this.text = text;
		}
	}

	public override void Save (PersistedObject obj)
	{
		base.Save (obj);
		obj.SetData<string>("GUIButton.text", text);
	}
}
