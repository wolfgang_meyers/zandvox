using UnityEngine;
using System.Collections.Generic;

public class GUIEditor : MonoBehaviour {

	private GUIPlugin guiPlugin;

	//TODO: handle states better
	private bool mouseDown = false;
	private bool resizing = false;

	private GUIElement selectedElement;
	private Vector2 dragPoint;
	private GUIRoot guiRoot;

	private List<Rect> controlRects = new List<Rect>();

	public string guiName;

	// Use this for initialization
	void Start () {
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		guiPlugin = (GUIPlugin)pluginRegistry.GetPlugin("GUI");
		guiRoot = GameObject.FindObjectOfType<GUIRoot>();
	}

	void RenderWindow() {

	}

	void OnGUI() {
		controlRects.Clear();
		GUIUtil.screenMatrix();

		Vector2 mousePos = ZUtil.ScaledMousePosition();

		Rect addBoxRect = new Rect(10, 10, 100, 25);
		Rect addButtonRect = new Rect(10, 40, 100, 25);
		controlRects.Add(addBoxRect);
		controlRects.Add(addButtonRect);

		//TODO: add window as well. All top-level containers can be reused.
		if (GUI.Button(addBoxRect, "Add Box")) {
			GUIBox box = guiPlugin.NewBox("Hello, world!", new Rect(50, 50, 200, 200));
			guiRoot.elements.Add(box);
		}
		//TODO: remove this - buttons belong in containers
		if (GUI.Button(addButtonRect, "Add Button")) {
			GUIButton button = guiPlugin.NewButton("A button!", new Rect(50, 50, 100, 25));
			guiRoot.elements.Add(button);
		}
		if (selectedElement != null) {
			Rect absPos = selectedElement.GetAbsolutePosition();
			GUIUtil.DrawBox(selectedElement.GetAbsolutePosition(), 1f, 1f);
			//TODO: move this to plugin
			if (selectedElement is GUIBox) {
				GUIBox box = (GUIBox)selectedElement;
				Rect addChildButtonRect = new Rect(absPos.xMax + 10, absPos.yMin, 25, 25);
				controlRects.Add(addChildButtonRect);
				if (GUI.Button(addChildButtonRect, "+")) {
					Debug.Log("Child button");
					GUIButton button = guiPlugin.NewButton("A button!", new Rect(50, 50, 100, 25));
					box.AddChild(button);
				}
				Rect addChildBoxRect = new Rect(absPos.xMax + 10, addChildButtonRect.yMax + 10, 25, 25);
				controlRects.Add(addChildBoxRect);
				if (GUI.Button(addChildBoxRect, "[]")) {
					GUIBox childBox = guiPlugin.NewBox("Child", new Rect(50, 50, 100, 100));
					box.AddChild(childBox);
				}
				Rect resizeButtonRect = new Rect(absPos.xMax + 10, addChildBoxRect.yMax + 10, 25, 25);
				controlRects.Add(resizeButtonRect);
				if (GUIUtil.ButtonDown(resizeButtonRect, "<>")) {
					resizing = true;
					dragPoint = new Vector2(mousePos.x - absPos.width, mousePos.y - absPos.height);
				}
			}
		}

		//don't process mouse events if the cursor is within any of the control button rects
		foreach (Rect rect in controlRects) {
			if (rect.Contains(mousePos)) {
				return;
			}
		}

		if (resizing) {
			if (Input.GetMouseButtonUp(0)) {
				resizing = false;
			} else {
				Rect currentPosition = selectedElement.GetAbsolutePosition();
				selectedElement.SetAbsolutePosition(new Rect(
					currentPosition.xMin,
					currentPosition.yMin,
					mousePos.x - dragPoint.x,
					mousePos.y - dragPoint.y
				));
			}
		} else {
			if (Input.GetMouseButtonDown(0)) {
				mouseDown = true;
				selectedElement = null;
				for (int i = guiRoot.elements.Count - 1; i >= 0; i--) {
					GUIElement element = guiRoot.elements[i];
					if (element.GetAbsolutePosition().Contains(mousePos)) {
						selectedElement = CalculateSelectedElement(mousePos, element);
						Rect position = selectedElement.GetAbsolutePosition();
						dragPoint = new Vector2(mousePos.x - position.xMin, mousePos.y - position.yMin);
						break;
					}
				}
			} else if (Input.GetMouseButtonUp(0)) {
				mouseDown = false;
			} else if (mouseDown && selectedElement != null) {
				selectedElement.SetAbsolutePosition(new Rect(
					mousePos.x - dragPoint.x,
					mousePos.y - dragPoint.y,
					selectedElement.position.width,
					selectedElement.position.height
				));
			}
		}


	}

	private GUIElement CalculateSelectedElement(Vector2 mousePos, GUIElement el) {
		if (el is GUIBox) {
			GUIBox box = (GUIBox)el;
			foreach (GUIElement child in box.GetChildren()) {
				if (child.GetAbsolutePosition().Contains(mousePos)) {
					return CalculateSelectedElement(mousePos, child);
				}
			}
		}
		return el;
	}
}
