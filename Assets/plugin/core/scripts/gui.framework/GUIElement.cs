using UnityEngine;
using System.Collections;

public abstract class GUIElement : PluginElement, IPersistentBehavior {
	/// <summary>
	/// This is the position of the element, relative to the parent. If parent is unset,
	/// this is the absolute position of the element.
	/// </summary>
	public Rect position;

	/// <summary>
	/// The container of this element.
	/// </summary>
	public GUIElement parent;


	[EditableProperty(EditorTypes.Toggle)]
	public bool snapToGrid = false;

	[EditableProperty(EditorTypes.TextField)]
	public string id = null;

	public void SetAbsolutePosition(Rect position) {
		if (parent != null) {
			Rect parentPosition = parent.GetAbsolutePosition();
			position = new Rect(
				position.xMin - parentPosition.xMin,
				position.yMin - parentPosition.yMin,
				position.width,
				position.height
			);
		}
		this.position = position;
	}

	public Rect GetAbsolutePosition() {
		Rect position = this.position;
		if (parent != null) {
			Rect parentPosition = parent.GetAbsolutePosition();
			position = new Rect(
				position.xMin + parentPosition.xMin,
				position.yMin + parentPosition.yMin,
				position.width, position.height
			);
		}
		return position;
	}

	public abstract void Render();

	//TODO: ***** use PersistedObject.children for something.

	public virtual void Save (PersistedObject obj)
	{
		obj.SetData<string>("GUIElement.position", position.Serialize());
	}

	public virtual void Restore (PersistedObject obj)
	{
		string pos = obj.GetData<string>("GUIElement.position");
		if (pos != null) {
			this.position = ZUtil.ParseRect(pos);
		}
	}

}
