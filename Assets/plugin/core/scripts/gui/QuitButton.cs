﻿using UnityEngine;
using System.Collections;

public class QuitButton : MonoBehaviour {
	
	public GenericMenu gameMenu;

	private InputPlugin inputPlugin;
	private InputBinding gameMenuBinding;

	void Start() {
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
		gameMenuBinding = (InputBinding)inputPlugin.GetElement("Game Menu");
	}

	// Update is called once per frame
	void Update () {
		if (inputPlugin.GetInputDown("Game Menu")) {
			GameMode gameMode = GameObject.FindObjectOfType<GameMode>();
			if (gameMode != null) {
				Destroy (gameMode);
			}

			Destroy (this);
			Instantiate(gameMenu);
		}
	}
	
	void OnGUI() {
		GUIUtil.screenMatrix();
		GUI.Label(new Rect(50, 25, 200, 50), gameMenuBinding.GetInstruction() + " for game menu");
	}
}
