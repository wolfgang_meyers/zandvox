﻿using UnityEngine;
using System.Collections;

public class NewPersistedItemMenu : MonoBehaviour {

	public GameObject cancelTarget;
	public Vector2 itemNameSize;
	public float buttonHeight;
	public float padding;
	public string title = "Create new persisted item";
	
	public string persistenceCategoryName;
	
	protected PersistencePlugin persistencePlugin;
	protected PersistenceCategory persistenceCategory;
	private string itemName = "";
	
	// Use this for initialization
	protected virtual void Start () {
		PluginRegistry pluginRegistry = (PluginRegistry)GameObject.FindObjectOfType(typeof(PluginRegistry));
		persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
		persistenceCategory = (PersistenceCategory)persistencePlugin.GetElement(persistenceCategoryName);
	}
	
	void OnGUI() {
		GUIUtil.screenMatrix();
		float width = itemNameSize.x + padding * 2;
		float height = itemNameSize.y + buttonHeight + (padding * 4);
		float top = (Constants.SCREEN_HEIGHT - height) / 2;
		float left = (Constants.SCREEN_WIDTH - width) / 2;

		float buttonWidth = (width - padding * 3) / 2;

		Rect menuRect = new Rect(left, top, width, height);
		
		Rect characterNameRect = new Rect(
			left + padding, top + padding * 2, itemNameSize.x, itemNameSize.y);
		Rect createButtonRect = new Rect(
			left + padding, characterNameRect.yMax + padding, buttonWidth, buttonHeight);
		Rect cancelButtonRect = new Rect(
			createButtonRect.xMax + padding, createButtonRect.yMin, buttonWidth, buttonHeight);
		
		GUI.Box(menuRect, title);
		itemName = GUI.TextField(characterNameRect, itemName);
		if (GUI.Button(createButtonRect, "Create")) {
			if (persistencePlugin.PersistedItemExists(persistenceCategory, itemName)) {
				Debug.Log(persistenceCategoryName + ": Item already exists");
			} else {
				if (!Application.isPlaying) {
					Debug.Log("Create");
				} else {
					Destroy (gameObject);
					OnItemCreate(itemName);
				}
			}
		}
		if (GUI.Button(cancelButtonRect, "Cancel")) {
			if (!Application.isPlaying) {
				Debug.Log("Cancel");
			} else {
				if (cancelTarget == null) {
					Debug.Log("Cancel target is not set");
				} else {
					Destroy(gameObject);
					Instantiate(cancelTarget);
				}
			}
		}
	}

	protected virtual void OnItemCreate(string itemName) {

	}

}
