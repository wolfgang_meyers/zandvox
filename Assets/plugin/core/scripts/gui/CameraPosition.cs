﻿using UnityEngine;
using System.Collections;

public class CameraPosition : MonoBehaviour {

	public float x = 10;
	public float y = 50;

	void OnGUI() {
		GUIUtil.screenMatrix();
		GUI.Label(new Rect(x, y, 300, 25), "Camera Position: " + Camera.main.transform.position);
	}
}
