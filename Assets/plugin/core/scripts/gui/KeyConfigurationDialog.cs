﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class KeyConfigurationDialog : MonoBehaviour {

	public float padding = 10f;
	public GUISkin skin;
	public GameObject parentMenu;

	private InputBinding binding;
	private KeyCode defaultKey;
	private KeyCode defaultModifierKey;
	private KeyCode key;
	private KeyCode modifierKey;

	private bool setting = false;
	private bool settingModifier = false;

	void PropagateContext(InputBinding context) {
		this.binding = context;
		this.key = binding.key;
		this.modifierKey = binding.modifierKey;
		this.defaultKey = binding.defaultKey;
		this.defaultModifierKey = binding.defaultModifierKey;
	}

	// Use this for initialization
	void Start () {
	
	}

	void Update() {
		if (setting) {
			foreach(KeyCode key in System.Enum.GetValues(typeof(KeyCode)).ToList<KeyCode>()) {
				if (Input.GetKey(key)) {
					this.key = key;
					setting = false;
				}
			}
		}
		if (settingModifier) {
			foreach (KeyCode key in System.Enum.GetValues(typeof(KeyCode)).ToList<KeyCode>()) {
				if (Input.GetKey(key)) {
					this.modifierKey = key;
					settingModifier = false;
				}
			}
		}
	}
	
	void OnGUI() {
		GUIUtil.screenMatrix();
		if (skin == null) {
			skin = GUI.skin;
		}
		//one row for title, one row for key, one row for modifier key, one row for ok/cancel
		Vector2 labelSize = skin.label.CalcSize(new GUIContent("Modifier:"));
		float maxKeynameWidth = 0;
		foreach (KeyCode item in System.Enum.GetValues(typeof(KeyCode)).ToList<KeyCode>()) {
			maxKeynameWidth = Mathf.Max(maxKeynameWidth, skin.label.CalcSize(new GUIContent(item.ToString())).x);
		}
		float setWidth = skin.label.CalcSize(new GUIContent("Set")).x + 20;
		float resetWidth = skin.label.CalcSize(new GUIContent("Reset")).x + 20;

		float dialogWidth = labelSize.x + maxKeynameWidth + setWidth + resetWidth + (padding * 6);
		float dialogHeight = labelSize.y * 3 + (padding * 5);

		float dialogLeft = (Constants.SCREEN_WIDTH - dialogWidth) / 2;
		float dialogTop = (Constants.SCREEN_HEIGHT - dialogHeight) / 2;

		Rect menuRect = new Rect(dialogLeft, dialogTop, dialogWidth, dialogHeight);
		GUI.Box(menuRect, binding.DisplayName);

		//first row - key
		Rect keyLabelRect = new Rect(menuRect.xMin + padding, menuRect.yMin + padding * 2, labelSize.x, labelSize.y);
		GUI.Label(keyLabelRect, "Key:");
		Rect keyRect = new Rect(keyLabelRect.xMax + padding, menuRect.yMin + padding * 2, maxKeynameWidth, labelSize.y);
		GUI.Label(keyRect, setting ? "(set)" : this.key.ToString());
		Rect keySetRect = new Rect(keyRect.xMax + padding, menuRect.yMin + padding * 2, setWidth, labelSize.y);
		if (GUI.Button(keySetRect, "Set")) {
			settingModifier = false;
			setting = true;
		}
		Rect keyResetRect = new Rect(keySetRect.xMax + padding, menuRect.yMin + padding * 2, resetWidth, labelSize.y);
		if (GUI.Button(keyResetRect, "Reset")) {
			settingModifier = false;
			setting = false;
			this.key = this.defaultKey;
		}

		//second row - modifier key
		keyLabelRect = new Rect(keyLabelRect.xMin, keyLabelRect.yMax + padding, keyLabelRect.width, keyLabelRect.height);
		GUI.Label(keyLabelRect, "Modifier:");
		keyRect = new Rect(keyRect.xMin, keyRect.yMax + padding, keyRect.width, keyRect.height);
		GUI.Label(keyRect, settingModifier ? "(set)" : this.modifierKey.ToString());
		keySetRect = new Rect(keySetRect.xMin, keySetRect.yMax + padding, keySetRect.width, keySetRect.height);
		if (GUI.Button(keySetRect, "Set")) {
			setting = false;
			settingModifier = true;
		}
		keyResetRect = new Rect(keyResetRect.xMin, keyResetRect.yMax + padding, keyResetRect.width, keyResetRect.height);
		if (GUI.Button(keyResetRect, "Reset")) {
			setting = false;
			settingModifier = false;
			this.modifierKey = this.defaultModifierKey;
		}

		//third row - ok/cancel
		Rect cancelRect = new Rect(menuRect.xMin + padding, menuRect.yMax - labelSize.y - padding,
		                           (menuRect.width - padding * 3) / 2, labelSize.y);
		if (GUI.Button(cancelRect, "Cancel")) {
			Destroy (gameObject);
			Instantiate (parentMenu);
		}
		Rect okRect = new Rect(cancelRect.xMax + padding, cancelRect.yMin, cancelRect.width, cancelRect.height);
		if (GUI.Button(okRect, "OK")) {
			binding.key = this.key;
			binding.modifierKey = this.modifierKey;
			Destroy (gameObject);
			Instantiate (parentMenu);
			saveInputPreferences();
		}

	}

	private void saveInputPreferences() {
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		pluginRegistry.SavePluginPrefences();
	}
}
