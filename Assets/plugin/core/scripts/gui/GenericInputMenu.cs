﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode()]
public class GenericInputMenu : MonoBehaviour {

	public GameObject cancelTarget;
	public GameObject okTarget;

	public Vector2 itemNameSize;
	public float buttonHeight;
	public float padding;
	public string title = "Input a value";
	public string itemName = "";

	private bool valid = true;

	public bool propagateContext = true;
	
	private object context;

	void PropagateContext(object context) {
		this.context = context;
	}

	private GUIRegions guiRegions;

	void Start() {
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
		guiRegions.AddRegion(CalculateMenuRect());
	}
	
	void OnDisable() {
		if (guiRegions != null) {
			guiRegions.RemoveRegion(CalculateMenuRect());
		}
	}

	void ValidationResult(ValidationResult result) {
		this.valid = result.Valid;
	}

	private Rect CalculateMenuRect() {
		float width = itemNameSize.x + padding * 2;
		float height = itemNameSize.y + buttonHeight + (padding * 4);
		float top = (Constants.SCREEN_HEIGHT - height) / 2;
		float left = (Constants.SCREEN_WIDTH - width) / 2;
		Rect menuRect = new Rect(left, top, width, height);
		return menuRect;
	}

	void OnGUI() {
		GUIUtil.screenMatrix();

		Rect menuRect = CalculateMenuRect();
		float buttonWidth = (menuRect.width - padding * 3) / 2;

		
		Rect inputRect = new Rect(
			menuRect.xMin + padding, menuRect.yMin + padding * 2, itemNameSize.x, itemNameSize.y);
		Rect createButtonRect = new Rect(
			menuRect.xMin + padding, inputRect.yMax + padding, buttonWidth, buttonHeight);
		Rect cancelButtonRect = new Rect(
			createButtonRect.xMax + padding, createButtonRect.yMin, buttonWidth, buttonHeight);
		
		GUI.Box(menuRect, title);
		itemName = GUI.TextField(inputRect, itemName);
		if (GUI.Button(createButtonRect, "OK") && Application.isPlaying) {
			//"ValidationResult" callback exists for another component
			//to answer with a true/false result and message
			SendMessage("Validate", itemName, SendMessageOptions.DontRequireReceiver);
			//TODO: pop up validation error
			if (valid) {
				Destroy (gameObject);
				GameObject okAction = (GameObject)Instantiate(okTarget);

				Dictionary<string, object> arg = new Dictionary<string, object>();
				arg["value"] = itemName;

				if (propagateContext && context != null) {
					arg["context"] = context;
				}
				okAction.SendMessage("PropagateContext", arg);
			}
		}
		if (GUI.Button(cancelButtonRect, "Cancel") && Application.isPlaying) {

			Destroy(gameObject);
			if (cancelTarget != null) {
				Instantiate(cancelTarget);
			}
			
		}
	}
}
