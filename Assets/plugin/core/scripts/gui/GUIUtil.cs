using System;
using System.Collections.Generic;
using UnityEngine;

public class GUIUtil
{
	public static void screenMatrix(){
		GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(
			Screen.width / Constants.SCREEN_WIDTH, Screen.height / Constants.SCREEN_HEIGHT, 1));
	}
	
	public static void renderGrid() {
		
		for (int x = 50; x < Constants.SCREEN_WIDTH; x += 50) {
			//drawLine(new Vector2(x, 0), new Vector2(x, Constants.SCREEN_HEIGHT),
			//	clr, 2);
			DrawVgridline(x);
			GUI.Label(new Rect(x, 5, 50, 50), x.ToString());
		}
		for (int y = 50; y < Constants.SCREEN_HEIGHT; y += 50) {
			//drawLine(new Vector2(0, y), new Vector2(Constants.SCREEN_WIDTH, y), clr, 2);
			DrawHgridline(y);
			GUI.Label(new Rect(5, y - 25, 50, 50), y.ToString());
		}
		
	}
	
	private static Texture2D hgridline;
	private static Texture2D vgridline;

	public static void DrawBox(Rect box, float lineWidth=1.5f, float opacity=0.1f) {
		DrawVgridline(box.xMin, box.yMin, box.yMax, lineWidth, opacity);
		DrawVgridline(box.xMax, box.yMin, box.yMax, lineWidth, opacity);
		DrawHgridline(box.yMin, box.xMin, box.xMax, lineWidth, opacity);
		DrawHgridline(box.yMax, box.xMin, box.xMax, lineWidth, opacity);
	}

	public static void DrawVgridline(float x, float y1=0, float y2=Constants.SCREEN_HEIGHT, float lineWidth=1.5f, float opacity=0.1f) {
		Color clr = GUI.color;
		GUI.color = new Color(1, 1, 1, opacity);
		if (vgridline == null) {
			vgridline = new Texture2D(1, (int)Constants.SCREEN_HEIGHT);
			for (int y = 0; y < Constants.SCREEN_HEIGHT; y++) {
				vgridline.SetPixel(0, y, Color.white);
			}
			vgridline.Apply();
		}
		GUI.DrawTexture(new Rect(x, y1, lineWidth, (y2 - y1)), vgridline);
		GUI.color = clr;
	}
	
	public static void DrawHgridline(float y, float x1=0, float x2=Constants.SCREEN_WIDTH, float lineWidth=1.5f, float opacity=0.1f) {
		Color clr = GUI.color;
		GUI.color = new Color(1, 1, 1, opacity);
		if (hgridline == null) {
			hgridline = new Texture2D((int)Constants.SCREEN_WIDTH, 1);
			for (int x = 0; x < Constants.SCREEN_WIDTH; x++) {
				hgridline.SetPixel(x, 0, Color.white);
			}
			hgridline.Apply();
		}
		//GUI.DrawTexture(new Rect(pointA.x, pointA.y, 1, 1), lineTex);
		GUI.DrawTexture(new Rect(x1, y, (x2 - x1), lineWidth), hgridline);
		GUI.color = clr;
	}
	
	private static Texture2D lineTex;
	
	public static void drawLine(Vector2 pointA, Vector2 pointB, Color color, float width) {
	    // Save the current GUI matrix, since we're going to make changes to it.
	    Matrix4x4 matrix = GUI.matrix;
	 
	    // Generate a single pixel texture if it doesn't exist
	    if (!lineTex) {
	    	lineTex = new Texture2D(1, 1);
	    	lineTex.SetPixel(0, 0, Color.white);
	    	lineTex.Apply();
	    }
		
	 
	    // Store current GUI color, so we can switch it back later,
	    // and set the GUI color to the color parameter
	    Color savedColor = GUI.color;
	    GUI.color = color;
	 
	    // Determine the angle of the line.
	    float angle = Vector3.Angle(pointB-pointA, Vector2.right);
	 
	    // Vector3.Angle always returns a positive number.
	    // If pointB is above pointA, then angle needs to be negative.
	    if (pointA.y > pointB.y) { angle = -angle; }
	 
	    // Use ScaleAroundPivot to adjust the size of the line.
	    // We could do this when we draw the texture, but by scaling it here we can use
	    //  non-integer values for the width and length (such as sub 1 pixel widths).
	    // Note that the pivot point is at +.5 from pointA.y, this is so that the width of the line
	    //  is centered on the origin at pointA.
	    GUIUtility.ScaleAroundPivot(new Vector2((pointB-pointA).magnitude, width), new Vector2(pointA.x, pointA.y + 0.5f));
	 
	    // Set the rotation for the line.
	    //  The angle was calculated with pointA as the origin.
	    GUIUtility.RotateAroundPivot(angle, pointA);
	 
	    // Finally, draw the actual line.
	    // We're really only drawing a 1x1 texture from pointA.
	    // The matrix operations done with ScaleAroundPivot and RotateAroundPivot will make this
	    //  render with the proper width, length, and angle.
	    GUI.DrawTexture(new Rect(pointA.x, pointA.y, 1, 1), lineTex);
	 
	    // We're done.  Restore the GUI matrix and GUI color to whatever they were before.
	    GUI.matrix = matrix;
	    GUI.color = savedColor;
	}

	public static int DisplayPagingControls<T>(Rect menuRect, List<T> elements, float padding, int pageSize, int page) {
		int numPages = (int)Mathf.Ceil(elements.Count / (float)pageSize);

		Rect prevPageRect = new Rect(menuRect.xMin + padding, menuRect.yMax - padding - 25, 25, 25);
		Rect nextPageRect = new Rect(menuRect.xMax - padding - 25, prevPageRect.yMin, 25, 25);
		Rect pageLabelRect = new Rect(menuRect.xMin + (menuRect.width - 100) / 2, prevPageRect.yMin, 100, 25);
		//paging
		if (GUI.Button(prevPageRect, "<")) {
			page -= 1;
		}
		
		if (GUI.Button(nextPageRect, ">")) {
			page += 1;
		}
		if (page >= numPages) {
			page = numPages - 1;
		}
		if (page < 0) {
			page = 0;
		}
		
		GUI.Label(pageLabelRect, "Page " + (page + 1) + " of " + numPages);
		return page;
	}

	public static bool ButtonDown(Rect position, string text) {
		return ButtonDown(position, new GUIContent(text));
	}

	public static bool ButtonDown(Rect position, Texture2D texture) {
		return ButtonDown(position, new GUIContent(texture));
	}

	public static bool ButtonDown(Rect position, GUIContent content) {
		GUI.Button(position, content);
		bool mouseDown = Input.GetMouseButtonDown(0);
		if (mouseDown) {
			Vector2 mousePos = ZUtil.ScaledMousePosition();
			return position.Contains(mousePos);
		}
		return false;
	}


}
