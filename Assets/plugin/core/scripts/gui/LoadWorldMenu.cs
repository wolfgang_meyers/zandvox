﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode()]
public class LoadWorldMenu : LoadPersistedItemMenu {

	public GenericMenu worldActionsMenu;

	protected override void Start () {
		persistenceCategoryName = "Worlds";
		base.Start();
	}

	protected override void OnResultSelected (string result)
	{
		worldActionsMenu = (GenericMenu)Instantiate (worldActionsMenu);
		worldActionsMenu.SendMessage("PropagateContext", result, SendMessageOptions.DontRequireReceiver);
		Destroy (gameObject);
	}

}
