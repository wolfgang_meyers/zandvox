﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode()]
public class MockGUILayout : MonoBehaviour {

	public List<MockGUIElement> windows = new List<MockGUIElement>();

	void OnGUI() {
		GUIUtil.screenMatrix();
		foreach (MockGUIElement window in windows) {
			GUI.Box(window.menuRect, window.title);
		}
	}
}
