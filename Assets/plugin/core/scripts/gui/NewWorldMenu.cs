﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class NewWorldMenu : NewPersistedItemMenu {

	public GameMode editMode;
	private Environment env;
	
	// Use this for initialization
	override protected void Start () {
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		persistenceCategoryName = "Worlds";
		base.Start();
	}

	protected override void OnItemCreate (string itemName)
	{
		env.Save(itemName);
		//triggers controller-switching
		env.Load(itemName);
		Instantiate (editMode);
	}
	
}
