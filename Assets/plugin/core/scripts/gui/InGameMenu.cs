﻿using UnityEngine;
using System.Collections;

public class InGameMenu : MonoBehaviour {
	
	public GenericMenu mainMenu;
	public QuitButton quitButton;
	
	public Rect menuRect;
	public Rect saveWorldRect;
	public Rect optionsRect;
	public Rect quitRect;
	public Rect cancelRect;
	
	private GUIRegions guiRegions;
	
	private Environment env;
	private MouseLook mouseLook;
	private PluginElementSelector toolSelector;
	//save - saves game
	//options - configuration?
	//quit game - back to main menu
	
	
	// Use this for initialization
	void Start () {
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
		mouseLook = (MouseLook)GameObject.FindObjectOfType(typeof(MouseLook));
		guiRegions.AddRegion(menuRect);
		mouseLook.enabled = false;
	}
	
	void OnDisable() {
		guiRegions.RemoveRegion(menuRect);
	}
	
	void OnGUI() {
		GUIUtil.screenMatrix();
		GUI.Box(menuRect, "World Menu");
		if (GUI.Button(saveWorldRect, "Save World")) {
			env.Save(env.WorldName);
		}
		if (GUI.Button(optionsRect, "Options")) {
			Debug.Log("Options clicked");
		}
		if (GUI.Button(quitRect, "Quit")) {
			foreach (object obj in GameObject.FindObjectsOfType(typeof(ElementSwitcher))) {
				Destroy (((ElementSwitcher)obj).gameObject);
			}
			Destroy(gameObject);
			env.Quit();
			Instantiate(mainMenu);
		}
		if (GUI.Button(cancelRect, "Cancel")) {
			Destroy (this);
			Instantiate(quitButton);
		}
	}
	
}
