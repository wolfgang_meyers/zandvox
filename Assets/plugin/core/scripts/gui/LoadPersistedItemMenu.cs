﻿using UnityEngine;
using System.Collections.Generic;

public class LoadPersistedItemMenu : GenericSelectionMenu<string> {

	public string persistenceCategoryName;
	public GenericMenu actionsMenu;

	private PersistencePlugin persistencePlugin;
	private PersistenceCategory persistenceCategory;
	
	// Use this for initialization
	protected virtual void Start () {
		PluginRegistry pluginRegistry = (PluginRegistry)GameObject.FindObjectOfType(typeof(PluginRegistry));
		persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
		persistenceCategory = (PersistenceCategory)persistencePlugin.GetElement(persistenceCategoryName);
		if (title == "") {
			title = "Load " + persistenceCategory.DisplayName;
		}
	}
	
	protected override List<string> GetFilteredItemList (string filter)
	{
		return persistencePlugin.ListPersistedItems(persistenceCategory, filter);
	}

	protected override void OnResultSelected (string result)
	{
		actionsMenu = (GenericMenu)Instantiate (actionsMenu);
		actionsMenu.SendMessage("PropagateContext", result, SendMessageOptions.DontRequireReceiver);
		Destroy (gameObject);
	}
}
