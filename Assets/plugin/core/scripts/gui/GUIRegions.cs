﻿using UnityEngine;
using System.Collections.Generic;

public class GUIRegions : MonoBehaviour {
	
	private List<Rect> regions = new List<Rect>();
	
	public void AddRegion(Rect region) {
		regions.Add(region);
	}
	
	public void RemoveRegion(Rect region) {
		if (regions.Contains(region)) {
			regions.Remove(region);
		} else {
			Debug.Log("RemoveRegion: Region not found: " + region);
		}
	}

	public void PushModal() {
		regions.Add(new Rect(0, 0, Screen.width, Screen.height));
	}

	public void PopModal() {
		regions.Remove(new Rect(0, 0, Screen.width, Screen.height));
	}
	
	public bool IsMouseOverRegion() {
		bool result = false;
		float x = Input.mousePosition.x / Screen.width * Constants.SCREEN_WIDTH;
		float y = Constants.SCREEN_HEIGHT - (Input.mousePosition.y / Screen.height * Constants.SCREEN_HEIGHT);
		Vector2 mousePosition = new Vector2(x, y);
		foreach (Rect rect in regions) {
			result = result || rect.Contains(mousePosition);
		}
		return result;
	}
}
