﻿using UnityEngine;
using System.Collections.Generic;

public class EditUserInterface : MonoBehaviour {

	public GUIEditor editor;

	private void PropagateContext(object context) {
		string userInterfaceName = (string)context;
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		PersistencePlugin persistence = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
		Plugin guiPlugin = pluginRegistry.GetPlugin("GUI");
		PersistenceCategory userInterfaces = (PersistenceCategory)persistence.GetElement("User Interfaces");

		GUIRoot guiRoot = GameObject.FindObjectOfType<GUIRoot>();
		guiRoot.elements.Clear();

		if (persistence.PersistedItemComponentExists(userInterfaces, userInterfaceName, "interface")) {
			List<PersistedObject> persistedElements = persistence.LoadPersistedComponent<List<PersistedObject>>(userInterfaces, userInterfaceName, "interface");
			List<GUIElement> elements = persistence.DeserializeElements<GUIElement>(guiPlugin, persistedElements);

			//TODO: more elegant cleanup
			foreach (GUIElement element in guiRoot.elements) {
				Destroy (element);
			}
			
			guiRoot.elements.AddRange(elements);
		}
		editor = (GUIEditor)Instantiate(editor);
		editor.guiName = userInterfaceName;

	}
}
