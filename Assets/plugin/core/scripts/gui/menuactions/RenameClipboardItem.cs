﻿using UnityEngine;
using System.Collections.Generic;

public class RenameClipboardItem : MonoBehaviour {

	void PropagateContext(Dictionary<string, object> context) {
		PersistedClipboardData data = (PersistedClipboardData)context["context"];
		string name = (string)context["value"];
		data.name = name;
		Destroy (gameObject);
	}
}
