﻿using UnityEngine;
using System.Collections;

public class SaveWorld : MonoBehaviour {

	public QuitButton quitButton;

	// Use this for initialization
	void Start () {
		Environment env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		env.Save(env.WorldName);
		Destroy (gameObject);
		Instantiate(quitButton);
	}
}
