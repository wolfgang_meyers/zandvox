﻿using UnityEngine;
using System.Collections;

public class ApplicationQuit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Debug.Log("Application Quit");
		Application.Quit();
	}
}
