﻿using UnityEngine;
using System.Collections;

public class DeletePersistedObject : MonoBehaviour {

	public GameObject returnMenu;
	public string objectType;
	
	void PropagateContext(object context) {
		string name = (string)context;
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		PersistencePlugin persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
		PersistenceCategory userInterfaces = (PersistenceCategory)persistencePlugin.GetElement(objectType);
		persistencePlugin.DeletePersistedItem(userInterfaces, name);
		Destroy (this);
		Instantiate (returnMenu);
	}
}
