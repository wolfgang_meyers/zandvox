﻿using UnityEngine;
using System.Collections;

public class DeleteClipboardItem : MonoBehaviour {

	void PropagateContext(PersistedClipboardData data) {
		ClipboardController clipboard = GameObject.FindObjectOfType<ClipboardController>();
		clipboard.RemoveClip(data);
	}
}
