﻿using UnityEngine;
using System.Collections;

public class PasteClipboardItem : MonoBehaviour {

	void PropagateContext(PersistedClipboardData data) {
		ClipboardController clipboard = GameObject.FindObjectOfType<ClipboardController>();
		clipboard.PasteMode(data);
	}
}
