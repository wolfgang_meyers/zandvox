﻿using UnityEngine;
using System.Collections;

public class PlayWorld : MonoBehaviour {

	// Use this for initialization
	void PropagateContext(object context) {
		string worldName = (string)context;
		Environment env = GameObject.FindObjectOfType<Environment>();
		env.Load(worldName);
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		Plugin gameModesPlugin = pluginRegistry.GetPlugin("GameModes");
		Debug.Log("PlayWorld starting up - mode=" + env.GameMode);
		GameMode mode = (GameMode)gameModesPlugin.GetElement(env.GameMode);
		Instantiate(mode);
		Destroy (gameObject);
	}
}
