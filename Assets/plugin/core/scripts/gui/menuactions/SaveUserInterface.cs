﻿using UnityEngine;
using System.Collections.Generic;

public class SaveUserInterface : MonoBehaviour {

	void Start() {
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		PersistencePlugin persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
		PersistenceCategory userInterfaces = (PersistenceCategory)persistencePlugin.GetElement("User Interfaces");

		GUIRoot guiRoot = GameObject.FindObjectOfType<GUIRoot>();
		GUIEditor editor = GameObject.FindObjectOfType<GUIEditor>();
		List<PersistedObject> persistedElements = persistencePlugin.SerializeElements(guiRoot.elements);
		persistencePlugin.SavePersistedComponent(userInterfaces, editor.guiName, persistedElements, "interface");

	}

}
