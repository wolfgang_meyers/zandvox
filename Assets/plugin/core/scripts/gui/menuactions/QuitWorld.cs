﻿using UnityEngine;
using System.Collections;

public class QuitWorld : MonoBehaviour {

	public GenericMenu mainMenu;

	// Use this for initialization
	void Start () {
		Environment env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		//destroy any tools
		foreach (object obj in GameObject.FindObjectsOfType(typeof(ElementSwitcher))) {
			Destroy (((ElementSwitcher)obj).gameObject);
		}
		env.Quit();
		Destroy(gameObject);
		Instantiate(mainMenu);
	}
}
