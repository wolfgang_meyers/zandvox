﻿using UnityEngine;
using System.Collections.Generic;

public class UpdateLabel : MonoBehaviour {

	void PropagateContext(Dictionary<string, object> context) {
		Destroy(gameObject);
		Block block = (Block)context["context"];
		string label = (string)context["value"];
		block.SendMessage("UpdateLabel", label);
	}
}
