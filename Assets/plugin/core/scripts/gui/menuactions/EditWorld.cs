﻿using UnityEngine;
using System.Collections;

public class EditWorld : MonoBehaviour {

	public GameMode worldEditMode;

	void PropagateContext(object context) {
		Instantiate (worldEditMode);
		Environment env = GameObject.FindObjectOfType<Environment>();
		env.Load((string)context);
		Destroy (gameObject);
	}
}
