﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode()]
public class ZTooltip : MonoBehaviour {

	public float width = 200f;
	public float padding = 10f;
	public GUISkin skin;
	public float triggerDelay = 0.5f;

	private Rect trigger;
	private string title;
	private string message;
	private bool displaying;
	
	private bool triggering = false;

	public void Tooltip(Rect trigger, string title, string content) {
		if (!triggering && trigger.Contains(ZUtil.ScaledMousePosition())) {
			triggering = true;
			this.trigger = trigger;
			this.title = title;
			this.message = content;
			StartCoroutine(Trigger ());
		}
	}

	// Use this for initialization
	void Start () {
	}

	void OnGUI () {
		if (displaying) {
			//detect if mouse is still over the trigger
			if (!trigger.Contains(ZUtil.ScaledMousePosition())) {
				displaying = false;
				return;
			}
			GUIUtil.screenMatrix();
			if (skin == null) {
				skin = GUI.skin;
			}
			float titleHeight = skin.label.CalcHeight(new GUIContent(title), this.width);
			float messageHeight = skin.label.CalcHeight(new GUIContent(message), this.width);
			float height = titleHeight + messageHeight + padding * 2;
			float width = this.width + padding * 2;
			float left = Mathf.Min(Constants.SCREEN_WIDTH - (width + 10), trigger.xMax + 10);
			float top = Mathf.Min(Constants.SCREEN_HEIGHT - (height + 10), trigger.yMax + 10);
			Rect displayRect = new Rect(left, top, width, height);
			Rect messageRect = new Rect(left + padding, top + padding + titleHeight, this.width, messageHeight);
			//twice so that that box isn't so transparent
			GUI.Box(displayRect, title);
			GUI.Box(displayRect, title);
			GUI.Label(messageRect, message);
		}
	}

	System.Collections.IEnumerator Trigger() {
		yield return new WaitForSeconds(triggerDelay);
		if (trigger.Contains(ZUtil.ScaledMousePosition())) {
			displaying = true;
		}
		triggering = false;
	}


	public void Deactivate() {
		displaying = false;
	}
}
