﻿using UnityEngine;
using System.Collections;

public class DescriptionPreviewUpdater : MonoBehaviour {

	public DescriptionPreview preview;

	void Start() {
		preview = (DescriptionPreview)Instantiate(preview);
	}

	void OnSelectElement(ISelectableElement element) {
		preview.target = element;
	}
}
