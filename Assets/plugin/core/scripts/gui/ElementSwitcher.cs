﻿using UnityEngine;
using System.Collections;

public class ElementSwitcher : MonoBehaviour {

	private PluginElement selectedElement;

	void OnSelectElement(PluginElement element) {
		if (selectedElement != null) {
			Destroy (selectedElement.gameObject);
		}
		selectedElement = (PluginElement)Instantiate(element);
	}

	void OnDisable() {
		if (selectedElement != null) {
			Destroy (selectedElement.gameObject);
		}
	}
}
