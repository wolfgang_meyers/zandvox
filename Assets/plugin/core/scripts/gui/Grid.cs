﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class Grid : MonoBehaviour {
	
	public bool showGrid;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI() {
		if (showGrid) {
			GUIUtil.screenMatrix();
			GUIUtil.renderGrid();
		}
	}
}
