﻿using UnityEngine;
using System.Collections;

public class GameModeUpdater : MonoBehaviour {

	private Environment env;

	private bool initialized = false;

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			env = GameObject.FindObjectOfType<Environment>();
		}
	}

	void OnSelectElement(GameMode gameMode) {
		Initialize();
		env.GameMode = gameMode.DisplayName;
	}

	// Use this for initialization
	void Start () {
		Initialize();
	}
}
