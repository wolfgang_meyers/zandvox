﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode()]
public class GenericMenu : MonoBehaviour {

	public string title;
	public List<MenuOption> menuItems = new List<MenuOption>();
	public Vector2 menuItemSize;
	public float padding;

	private List<Rect> menuItemPositions = new List<Rect>();

	/// <summary>
	/// If set to true, any objects that are created by clicking
	/// a menu option will be passed the context, if any, that
	/// was passed to the menu.
	/// </summary>
	public bool propagateContext = true;

	private object context;
	private ZTooltip tooltip;

	void PropagateContext(object context) {
		this.context = context;
	}

	private GUIRegions guiRegions;
	//if this menu was passed a menu action, then
	//hold on to it in case it needs to be propagated
	//to any child menus
	private string menuAction;

	protected virtual void Start() {
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
		guiRegions.AddRegion(CalculateMenuRect());

		//register tooltips
		//TODO: help button to hover over..
		BuildMenuItemPositions();
		tooltip = GameObject.FindObjectOfType<ZTooltip>();
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		InputPlugin inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
		inputPlugin.CaptureInput("modal");
	}

	private void BuildMenuItemPositions() {
		menuItemPositions.Clear();
		Rect menuRect = CalculateMenuRect();
		float i = menuRect.yMin + padding + menuItemSize.y;
		foreach (MenuOption option in menuItems) {
			Rect position = new Rect(menuRect.xMin + padding, i, menuItemSize.x, menuItemSize.y);
			menuItemPositions.Add(position);
			i += (menuItemSize.y + padding);
		}
	}

	void OnDisable() {
		if (guiRegions != null) {
			guiRegions.RemoveRegion(CalculateMenuRect());
		}
		//unregister tooltips
		//TODO: help button to hover over..
		tooltip.Deactivate();
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		InputPlugin inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
		inputPlugin.ReleaseInput();
	}

	private Rect CalculateMenuRect() {
		float height = (menuItemSize.y + padding) * (menuItems.Count + 1);
		float width = menuItemSize.x + padding * 2;
		float top = (Constants.SCREEN_HEIGHT - height) / 2;
		float left = (Constants.SCREEN_WIDTH - width) / 2;
		return new Rect(left, top, width, height);
	}

	void OnGUI() {
		GUIUtil.screenMatrix();
		Rect menuRect = CalculateMenuRect();
		string title = this.title;
		if (context != null) {
			title = title.Replace("%s", context.ToString());
		}
		GUI.Box(menuRect, title);

		for (int i = 0; i < menuItems.Count; i++) {
			MenuOption option = menuItems[i];
			Rect position = menuItemPositions[i];
			DisplayMenuOption(position, option);
		}
	}

	private void DisplayMenuOption(Rect position, MenuOption option) {
		tooltip.Tooltip(position, option.DisplayName, option.Description);
		GUIContent content = null;
		if (option.Thumbnail == null) {
			content = new GUIContent(option.DisplayName);
		} else {
			content = new GUIContent(option.DisplayName, option.Thumbnail);
		}
		if (GUI.Button(position, content)) {
			if (Application.isPlaying) {
				Destroy(gameObject);
				if (option.target == null) {
					Debug.Log("Menu option " + option.displayName + " has no target assigned");
				} else {
					GameObject target = (GameObject)Instantiate(option.target);
					if (propagateContext && context != null) {
						target.SendMessage("PropagateContext", context, SendMessageOptions.DontRequireReceiver);
					}
				}
			}
		}
	}
}
