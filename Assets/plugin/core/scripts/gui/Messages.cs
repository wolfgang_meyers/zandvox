﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Messages : MonoBehaviour {
	
	public Rect displayLocation;
	public float displayLength = 5;
	
	private Queue<string> messages = new Queue<string>();
	private bool clearing = false;
	private string currentMessage;
	
	public void QueueMessage(string message) {
		messages.Enqueue(message);
		if (!clearing) {
			clearing = true;
			StartCoroutine("ClearMessages");
		}
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	void OnGUI() {
		GUIUtil.screenMatrix();
		if (currentMessage == null) {
			if (messages.Count > 0) {
				currentMessage = messages.Dequeue();
			}
		}
		if (currentMessage != null) {
			GUI.Label(displayLocation, currentMessage);
		}
	}
	
	IEnumerator ClearMessages() {
		yield return new WaitForSeconds(displayLength);
		currentMessage = null;
		if (this.enabled && messages.Count > 0) {
			clearing = true;
			StartCoroutine("ClearMessages");
		} else {
			clearing = false;
		}
	}
}
