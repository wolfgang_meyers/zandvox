﻿using UnityEngine;
using System.Collections;

public class DescriptionPreview : MonoBehaviour {

	public Rect menuRect;
	public float padding;
	public string emptyTitle = "No item selected";
	public ISelectableElement target;

	public GUIStyle labelStyle;

	void OnGUI() {
		string title = target == null ? emptyTitle : target.DisplayName;
		GUI.Box(menuRect, title);
		Rect labelRect = new Rect(
			menuRect.xMin + padding,
			menuRect.yMin + padding + 25,
			menuRect.width - padding * 2,
			menuRect.height - padding * 2 - 25
		);
		GUI.Label(labelRect, target.Description == null ? "No Description" : target.Description, labelStyle);
	}
}
