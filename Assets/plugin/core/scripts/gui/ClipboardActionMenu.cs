﻿using UnityEngine;
using System.Collections;

public class ClipboardActionMenu : GenericMenu {

	protected override void Start ()
	{
		menuItems.Clear();
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		Plugin clipboardActionsPlugin = pluginRegistry.GetPlugin("Clipboard Actions");
		foreach (ISelectableElement el in clipboardActionsPlugin) {
			menuItems.Add((MenuOption)el);
		}
		MenuOption cancel = new MenuOption();
		cancel.displayName = "Cancel";
		menuItems.Add(cancel);
		base.Start ();
	}
}
