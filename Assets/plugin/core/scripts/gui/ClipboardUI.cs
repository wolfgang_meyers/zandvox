﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode()]
public class ClipboardUI : MonoBehaviour {

	public Rect menuRect;

	public float padding;
	public float captureButtonHeight;
	public float filterLabelWidth;
	public float filterSectionHeight;
	public int pageSize;
	public float itemHeight;

	public GenericMenu clipboardItemMenu;

	private ClipboardController clipboard;
	private GUIRegions guiRegions;
	private string filterString = string.Empty;
	//private int editingName = -1;
	private int page = 0;

	//used to refresh results when items show up
	private int lastClipboardItemCount = 0;

	private List<PersistedClipboardData> filteredItems = new List<PersistedClipboardData>();

	// Use this for initialization
	void Start () {
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
		clipboard = (ClipboardController)GameObject.FindObjectOfType(typeof(ClipboardController));
		guiRegions.AddRegion(menuRect);
		refreshFilteredItems();
	}

	void OnDisable() {
		guiRegions.RemoveRegion(menuRect);
	}

	private void refreshFilteredItems() {
		filteredItems.Clear();
		if (clipboard == null) {
			//mock data
			for (int i = 0; i < 20; i++) {
				PersistedClipboardData data = new PersistedClipboardData();
				data.name = "Untitled " + i;
				filteredItems.Add(data);
			}
		} else {
			for (int i = 0; i < clipboard.ClipCount; i++) {
				PersistedClipboardData data = clipboard.GetClip(i);
				if (data.name.Contains(filterString)) {
					filteredItems.Add(data);
				}
			}
		}
	}

	void OnGUI() {
		if (clipboard != null && lastClipboardItemCount != clipboard.ClipCount) {
			lastClipboardItemCount = clipboard.ClipCount;
			refreshFilteredItems();
		}
		GUIUtil.screenMatrix();
		GUI.Box(menuRect, "Clipboard");
		Rect captureButtonRect = new Rect(menuRect.xMin + padding, menuRect.yMin + padding + 25,
		                                  menuRect.width - padding * 2, captureButtonHeight);
		if (GUI.Button(captureButtonRect, "Capture")) {
			clipboard.CaptureMode();
		}
		Rect filterLabelRect = new Rect(menuRect.xMin + padding, captureButtonRect.yMax + padding,
		                                filterLabelWidth, filterSectionHeight);
		GUI.Label(filterLabelRect, "Filter:");
		Rect filterRect = new Rect(filterLabelRect.xMax + padding, captureButtonRect.yMax + padding,
		                           menuRect.width - (filterLabelWidth + padding * 3), filterSectionHeight);
		string newFilterString = GUI.TextField(filterRect, filterString);
		if (newFilterString != filterString) {
			refreshFilteredItems();
			filterString = newFilterString;
		}

		List<PersistedClipboardData> data = filteredItems;
		/*
for (int i = 0; i + (page * pageSize) < elements.Count && i < elementLocations.Count; i++) {
				toggleElement(elementLocations[i], elements[i + (int)(page * pageSize)]);
			}
		 */
//		for (int i = 0; i < clipboardItemRects.Count
//		     && i < data.Count; i++) {
//			renderClipItem(data[i], i);
//		}
		Rect clipItemRect = new Rect(menuRect.xMin + padding, filterRect.yMax + padding,
		                             menuRect.width - padding * 2, itemHeight);
		for (int i = 0; i < pageSize && i + (page * pageSize) < data.Count; i++) {
			try {
				renderClipItem(data[i + (page * pageSize)], i, clipItemRect);
			} catch (System.ArgumentOutOfRangeException) {
				Debug.LogError("Error rendering clip item. i=" + i + ", data len=" + data.Count + ", page=" + page + ", pageSize=" + pageSize);
			}
			clipItemRect = new Rect(clipItemRect.xMin, clipItemRect.yMax + padding,
			                        clipItemRect.width, clipItemRect.height);
		}

		page = GUIUtil.DisplayPagingControls(menuRect, data, padding, pageSize, page);
		/*
		public float padding;
	public float captureButtonHeight;
	public float filterLabelWidth;
	public float filterSectionHeight;
	public int pageSize;
	public float itemHeight;
	public float editButtonWidth;
	public float deleteButtonWidth;
		 */
	}

	private void renderClipItem(PersistedClipboardData data, int index, Rect clipItemRect) {
//		Rect clipItemRect = clipboardItemRects[index];


//		Rect deleteRect = new Rect(clipItemRect.xMax - deleteButtonWidth,
//		                           clipItemRect.yMin, deleteButtonWidth, clipItemRect.height);
//		Rect editRect = new Rect(deleteRect.xMin - editButtonWidth - padding, clipItemRect.yMin,
//		                         editButtonWidth, clipItemRect.height);
		Rect itemRect = new Rect(clipItemRect.xMin, clipItemRect.yMin,
		                         clipItemRect.width,
		                         clipItemRect.height);

		if (GUI.Button(itemRect, data == null ? "Untitled " + index : data.name)) {
			if (Application.isPlaying) {
				GenericMenu menuInstance = (GenericMenu)Instantiate(clipboardItemMenu);
				menuInstance.SendMessage("PropagateContext", data);
			}
			//clipboard.PasteMode(data);
		}


	}
}
