using UnityEngine;
using System.Collections.Generic;

public class ElementSelector : MonoBehaviour {

	public Rect menuRect;
	
	//paging controls
	
	public bool enablePaging = false;
	private int page = 0;
	
	public GUIStyle elementLabelStyle;

	public Vector2 elementSize;
	public Vector2 displayGridSize;
	public float padding;

	public bool allowNullSelection = false;
	
	//used to detect changes and rebuild the display grid
	private Vector2 _elementSize;
	private Vector2 _displayGridSize;
	private float _padding;
	
	public ISelectableProvider provider;
	private ZTooltip tooltip;

	private GUISkin skin;
	
//	private bool NeedsDisplayGridRebuild() {
//		return elementSize != _elementSize
//				|| displayGridSize != _displayGridSize
//				|| padding != _padding;
//	}
//	
//	private void BuildElementLocations() {
//		elementLocations.Clear();
//		Vector2 elementStartCorner = new Vector2(menuRect.xMin + padding, menuRect.yMin + padding * 3 + 50);
//		for (float y = elementStartCorner.y;
//		     y < elementStartCorner.y + ((elementSize.y + padding)* displayGridSize.y);
//		     y += (elementSize.y + padding)) {
//			for (float x = elementStartCorner.x;
//			     x < elementStartCorner.x + ((elementSize.x + padding) * displayGridSize.x);
//			     x += (elementSize.x + padding)) {
//				elementLocations.Add(new Rect(x, y, elementSize.x, elementSize.y));
//			}
//		}
//		_elementSize = elementSize;
//		_displayGridSize = displayGridSize;
//		_padding = padding;
//	}
	
//	private List<Rect> elementLocations = new List<Rect>();
	//used in case the editor can't generate a preview
	public Texture2D defaultThumbnail;
	public string selectorName;
	public PersistedObject preferences;

	private GUIRegions guiRegions;

	private ISelectableElement element;
	private List<ISelectableElement> favorites = new List<ISelectableElement>();
	
	private string filter = "";
	
	public ISelectableElement SelectedElement {
		get { return element; }
	}
	
	protected virtual void Start () {
		if (provider == null) {
			throw new System.InvalidProgramException("Provider must be specified");
		}
		SkinProvider skinProvider = GameObject.FindObjectOfType<SkinProvider>();
		if (skinProvider != null && skinProvider.skin != null) {
			this.skin = skinProvider.skin;
		}
		if (this.skin == null) {
			this.skin = GUI.skin;
		}
		tooltip = GameObject.FindObjectOfType<ZTooltip>();
//		BuildElementLocations();
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
		element = provider.GetDefaultElement();
		SendMessage("OnSelectElement", this.element, SendMessageOptions.DontRequireReceiver);
		guiRegions.AddRegion(menuRect);
		Restore (preferences);
		refreshFilteredElementList();
	}
	
	void OnDisable() {
		guiRegions.RemoveRegion(menuRect);
		tooltip.Deactivate();
	}
	
	//optimization - don't create a new list every time
	private List<ISelectableElement> filteredElementList = new List<ISelectableElement>();

	private void refreshFilteredElementList() {
		filteredElementList.Clear();
		if (this.element != null &&
		    	provider.GetElement(this.element.DisplayName) != null && 
		    	this.element.DisplayName.ToUpper().Contains(filter.ToUpper())) {
			filteredElementList.Add(provider.GetElement(this.element.DisplayName));
		}
		foreach (ISelectableElement n in favorites) {
			if (provider.GetElement(n.DisplayName) != null && n.DisplayName.ToUpper().Contains(filter.ToUpper())) {
				filteredElementList.Add(provider.GetElement(n.DisplayName));
			}
		}
		if (allowNullSelection && !filteredElementList.Exists(elem => elem.DisplayName == "None")) {
			SelectableElement nullElement = new SelectableElement();
			nullElement.displayName = "None";
			filteredElementList.Add(nullElement);
		}
		foreach (ISelectableElement b in provider) {
			if (showElement(b) && !filteredElementList.Exists(elem => elem.DisplayName == b.DisplayName)) {
				filteredElementList.Add(b);
			}
		}
	}

	protected virtual bool showElement(ISelectableElement element) {
		return element.Selectable && element.DisplayName.ToUpper().Contains(filter.ToUpper());
	}
	
	void OnGUI() {
//		if (NeedsDisplayGridRebuild()) {
//			BuildElementLocations();
//		}
		GUIUtil.screenMatrix();
		GUI.Box(menuRect, selectorName);
		Rect filterLabelRect = new Rect(menuRect.xMin + padding, menuRect.yMin + padding + 25, 50, 25);
		Rect filterRect = new Rect(menuRect.xMin + padding * 2 + 50, menuRect.yMin + padding + 25, menuRect.width - padding * 3 - 50, 25);
		GUI.Label(filterLabelRect, "Filter:");
		string newFilter = GUI.TextField(filterRect, filter);
		if (newFilter != filter) {
			filter = newFilter;
			refreshFilteredElementList();
		}
		List<ISelectableElement> elements = filteredElementList;
		int pageSize = (int)(displayGridSize.x * displayGridSize.y);

		//tooltips should be refreshed if the page changes
		int newpage = GUIUtil.DisplayPagingControls(menuRect, elements, padding, pageSize, page);
		if (newpage != page) {
			page = newpage;
		}

		if (elements.Count > 0) {
//			for (int i = 0; i + (page * pageSize) < elements.Count && i < elementLocations.Count; i++) {
//				toggleElement(elementLocations[i], elements[i + (int)(page * pageSize)]);
//			}
			//Vector2 elementStartCorner = new Vector2(menuRect.xMin + padding, menuRect.yMin + padding * 3 + 50);
			//		Texture2D preview = getElementPreview(element);
			//		tooltip.Tooltip(position, element.DisplayName, element.Description);
			//
			//		Rect labelPosition = new Rect(position.xMin, position.yMax - 10, position.width, 50);
			//		GUI.Label(labelPosition, element.ToString(), elementLabelStyle);
			//
			//		if (element != null && element.DisplayName == "None") {
			//			element = null;
			//		}
			//		//static function Toggle(position: Rect, value: bool, text: string): bool;
			//		if (GUI.Toggle(position, element == this.element, preview)) {
			//			bool changed = this.element != element;
			//			if (changed) {
			//				UpdateSelectedElement(element);
			//			}
			//		}
			int selectedIndex = elements.IndexOf(SelectedElement) - page * pageSize;
			float numHigh = Mathf.Ceil(Mathf.Min(displayGridSize.y, elements.Count / displayGridSize.x));
			float numWide = displayGridSize.x;
			Rect selectorRect = new Rect(
				menuRect.xMin + padding,
				menuRect.yMin + padding * 4 + 5,
				numWide * elementSize.x,
				numHigh * elementSize.y
			);
			List<GUIContent> toDisplay = new List<GUIContent>();
			for (int i = 0; i + (page * pageSize) < elements.Count && i < displayGridSize.x * displayGridSize.y; i++) {
				ISelectableElement element = elements[i + (int)(page * pageSize)];
				toDisplay.Add(new GUIContent(element.DisplayName, element.Thumbnail == null ? defaultThumbnail : element.Thumbnail, element.Description));
				/*
//		Texture2D preview = getElementPreview(element);
//		tooltip.Tooltip(position, element.DisplayName, element.Description);
				 */
				//calculate tooltip position
				float tooltipX = (i % displayGridSize.x) * elementSize.x + selectorRect.xMin;
				float tooltipY = Mathf.Floor(i / displayGridSize.x) * elementSize.y + selectorRect.yMin;
				tooltip.Tooltip(new Rect(tooltipX, tooltipY, elementSize.x, elementSize.y), element.DisplayName, element.Description);
			}

			int newSelectedIndex = GUI.SelectionGrid(selectorRect, selectedIndex, toDisplay.ToArray(), (int)displayGridSize.x, skin.button);
			bool changed = selectedIndex != newSelectedIndex;
			if (changed) {
				Debug.Log("selected=" + selectedIndex + ", new=" + newSelectedIndex);
				ISelectableElement el = null;
				if (newSelectedIndex != -1) {
					el = elements[newSelectedIndex + page * pageSize];
					if (el.DisplayName == "None") {
						el = null;
					}
				}
				UpdateSelectedElement(el);
			}
			//(elementSize.x + padding) * displayGridSize.x
			//y += (elementSize.y + padding)

		}
	}

	private Texture2D getElementPreview(ISelectableElement b) {
		Texture2D result = null;
		
		result = b.Thumbnail;
		if (result == null) {
			result = defaultThumbnail;
		}
		
		return result;
	}

	public void UpdateSelectedElement(string name) {
		ISelectableElement element = name == null || name == "None" ? null : provider.GetElement(name);
		UpdateSelectedElement(element);
		refreshFilteredElementList();
	}

	public void Refresh() {
		refreshFilteredElementList();	
	}

	private void UpdateSelectedElement(ISelectableElement element) {
		if (element != this.element) {
			if (this.element != null) {
				favorites.Insert(0, this.element);
			}
			this.element = element;
			SendMessage("OnSelectElement", this.element, SendMessageOptions.DontRequireReceiver);
			if (element != null) {
				favorites.RemoveAll(elem => elem.DisplayName == element.DisplayName);
			}
			Save (preferences);
			filter = "";
			page = 0;
			//blur input filter
			GUI.SetNextControlName("");
			GUI.FocusControl("");
			refreshFilteredElementList();
		}

	}
	
//	private void toggleElement(Rect position, ISelectableElement element) {
//		Texture2D preview = getElementPreview(element);
//		tooltip.Tooltip(position, element.DisplayName, element.Description);
//
//		Rect labelPosition = new Rect(position.xMin, position.yMax - 10, position.width, 50);
//		GUI.Label(labelPosition, element.ToString(), elementLabelStyle);
//
//		if (element != null && element.DisplayName == "None") {
//			element = null;
//		}
//		//static function Toggle(position: Rect, value: bool, text: string): bool;
//		if (GUI.Toggle(position, element == this.element, preview)) {
//			bool changed = this.element != element;
//			if (changed) {
//				UpdateSelectedElement(element);
//			}
//		}
//	}
	
	#region IPersistentBehavior implementation
	
	public void Save (PersistedObject obj)
	{
		if (obj != null) {
			PersistedBehaviorData data = new PersistedBehaviorData();
			data.SetStringValue("selectedElement", SelectedElement.DisplayName);
			List<string> favoritenames = new List<string>();
			foreach (ISelectableElement element in favorites) {
				favoritenames.Add(element.DisplayName);
			}
			data.SetStringListValue("favorites", favoritenames);
			obj.SetData(selectorName, data);
		}
	}
	
	public void Restore (PersistedObject obj)
	{
		if (obj != null) {
			PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>(selectorName);
			if (data != null) {
				if (data.IsSet("selectedElement")) {
					element = provider.GetElement(data.GetStringValue("selectedElement"));
					SendMessage("OnSelectElement", this.element, SendMessageOptions.DontRequireReceiver);
				}
				if (data.IsSet("favorites")) {
					favorites.Clear();
					List<string> favoritenames = data.GetStringListValue("favorites");
					foreach (string name in favoritenames) {
						ISelectableElement favorite = provider.GetElement(name);
						if (favorite != null) {
							favorites.Add(favorite);
						}
					}
				}
			}
		}
		
	}
	
	#endregion
}
