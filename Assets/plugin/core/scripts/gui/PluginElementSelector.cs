﻿using UnityEngine;
using System.Collections.Generic;

public class PluginElementSelector : ElementSelector {

	public string pluginName;

	private PluginRegistry pluginRegistry;

	protected override void Start () {
		pluginRegistry = (PluginRegistry)GameObject.FindObjectOfType(typeof(PluginRegistry));
		this.provider = GetProvider();
		Debug.Log("PluginElementSelector: provider=" + provider);
		base.Start();
	}

	//slightly hacky... allowing a nested plugin reference,
	//where a plugin might provide several child plugins
	//this was done to allow multiple avatar definitions,
	//each being a child plugin with their own set of bones
	//currently only one level of nesting is supported
	private ISelectableProvider GetProvider() {
		if (pluginName.IndexOf("/") == -1) {
			return pluginRegistry.GetPlugin(pluginName);
		} else {
			string[] path = pluginName.Split('/');
			Plugin parent = pluginRegistry.GetPlugin(path[0]);
			Plugin child = (Plugin)parent.GetElement(path[1]);
			return child;
		}
	}
}
