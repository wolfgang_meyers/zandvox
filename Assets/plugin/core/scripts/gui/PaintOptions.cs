﻿using UnityEngine;
using System;
using System.Collections;

[ExecuteInEditMode()]
public class PaintOptions : MonoBehaviour, IPersistentBehavior {

	public Rect menuRect;
	
	public Rect brushSizeXLabelRect;
	public Rect brushSizeXRect;
	public Rect brushSizeXValueRect;

	public float brushOptionsYTop;
	public float brushOptionsZTop;
	
	public int brushSizeX = 0;
	public int brushSizeY = 0;
	public int brushSizeZ = 0;

	public DateTime lastUpdate;
	
	private GUIRegions guiRegions;
	private Environment env;
	
	// Use this for initialization
	void Start () {
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		guiRegions.AddRegion(menuRect);
		lastUpdate = DateTime.Now;
	}
	
	void OnDisable() {
		guiRegions.RemoveRegion(menuRect);
	}
	
	void OnGUI() {
		GUIUtil.screenMatrix();
		GUI.Box(menuRect, "Paint Options");

		//draw x size
		GUI.Label(brushSizeXLabelRect, "Brush Size(X):");
		brushSizeX = brushSize (brushSizeXRect, brushSizeX);
		GUI.Label(brushSizeXValueRect, brushSizeX.ToString());

		//draw y size
		Rect brushSizeYLabelRect = copyRect(brushSizeXLabelRect, brushOptionsYTop);
		GUI.Label(brushSizeYLabelRect, "Brush Size(Y):");
		Rect brushSizeYRect = copyRect(brushSizeXRect, brushOptionsYTop + 5);
		brushSizeY = brushSize(brushSizeYRect, brushSizeY);
		Rect brushSizeYValueRect = copyRect(brushSizeXValueRect, brushOptionsYTop);
		GUI.Label(brushSizeYValueRect, brushSizeY.ToString());

		//draw z size
		Rect brushSizeZLabelRect = copyRect(brushSizeXLabelRect, brushOptionsZTop);
		GUI.Label(brushSizeZLabelRect, "Brush Size(Z):");
		Rect brushSizeZRect = copyRect(brushSizeXRect, brushOptionsZTop + 5);
		brushSizeZ = brushSize(brushSizeZRect, brushSizeZ);
		Rect brushSizeZValueRect = copyRect(brushSizeXValueRect, brushOptionsZTop);
		GUI.Label(brushSizeZValueRect, brushSizeZ.ToString());
	}

	private Rect copyRect(Rect src, float top) {
		return new Rect(
			src.xMin,
			top,
			src.width,
			src.height
		);
	}

	private int brushSize(Rect position, int currentValue) {
		int result = (int)Mathf.Round(GUI.HorizontalSlider(position, currentValue, 0, 5));
		if (result != currentValue) {
			lastUpdate = DateTime.Now;
			Save(env.PersistedWorldData);
		}
		return result;
	}

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		PersistedBehaviorData data = new PersistedBehaviorData();
		data.SetIntValue("brushSizeX", brushSizeX);
		data.SetIntValue("brushSizeY", brushSizeY);
		data.SetIntValue("brushSizeZ", brushSizeZ);
		obj.SetData("PaintOptions", data);
	}

	public void Restore (PersistedObject obj)
	{
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("PaintOptions");
		if (data != null) {
			brushSizeX = data.GetIntValue("brushSizeX");
			brushSizeY = data.GetIntValue("brushSizeY");
			brushSizeZ = data.GetIntValue("brushSizeZ");
			lastUpdate = DateTime.Now;
		}
	}

	#endregion
}
