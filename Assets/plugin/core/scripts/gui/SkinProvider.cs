﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class SkinProvider : MonoBehaviour {

	public GUISkin skin;

	// Use this for initialization
	void Start () {
	
	}

	void OnGUI() {
		if (skin == null) {
			skin = (GUISkin)Instantiate(GUI.skin);
		}
	}
}
