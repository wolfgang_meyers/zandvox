﻿using UnityEngine;
using System.Collections;

public class WorldEditMode : MonoBehaviour {

	public QuitButton quitButton;
	public PluginElementSelector toolSelector;			
	
	// Use this for initialization
	void Start () {
		quitButton = (QuitButton)Instantiate(quitButton);
		toolSelector = (PluginElementSelector)Instantiate (toolSelector);
		Environment env = GameMode.FindObjectOfType<Environment>();
		toolSelector.preferences = env.PersistedWorldData;
		//if a controller exists in the scene, use it.
		env.PerspectiveController = (IPerspectiveController)GameObject.FindObjectOfType(typeof(EditorController));
	}

	void OnDisable() {
		Destroy (quitButton);
		Destroy (toolSelector);
	}
}
