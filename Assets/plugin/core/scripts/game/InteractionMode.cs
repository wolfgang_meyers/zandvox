﻿using UnityEngine;
using System.Collections;

public class InteractionMode : MonoBehaviour {

	public InteractionController interactionController;
	public QuitButton quitButton;

	// Use this for initialization
	void Start () {
		interactionController = (InteractionController)Instantiate(interactionController);
		Environment env = GameMode.FindObjectOfType<Environment>();
		//if a controller exists in the scene, use it.
		env.PerspectiveController = (IPerspectiveController)GameObject.FindObjectOfType(typeof(EditorController));
		Instantiate(quitButton);
	}

	void OnDisable() {
		Destroy (interactionController.gameObject);
	}
}
