// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;

[AttributeUsage(AttributeTargets.Field, AllowMultiple=true)]
public class EditorOption : Attribute
{
	private string name;
	private object value;

	public string Name {
		get { return name; }
	}

	public object Value {
		get { return value; }
	}

	public EditorOption(string name, object value) {
		this.name = name;
		this.value = value;
	}
}


