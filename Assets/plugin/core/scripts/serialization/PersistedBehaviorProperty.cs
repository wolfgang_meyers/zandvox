using System;

public class PersistedBehaviorProperty
{
	public string name;
	public string value;
	
	public PersistedBehaviorProperty() {
		
	}
	
	public PersistedBehaviorProperty(string name, string value) {
		this.name = name;
		this.value = value;
	}
}
