using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class PersistedObject
{
	public string objectType;
	public Dictionary<string, string> behaviorData = new Dictionary<string, string>();
	public Dictionary<string, List<PersistedObject>> children = new Dictionary<string, List<PersistedObject>>();

	public List<PersistedObject> GetChildren(string name) {
		if (children.ContainsKey(name)) {
			return children[name];
		}
		return null;
	}

	public void SetChildren(string name, List<PersistedObject> children) {
		this.children[name] = children;
	}

	public T GetData<T>(string name) where T : class {
		if (behaviorData.ContainsKey(name)) {
			try {
				return JsonConvert.DeserializeObject<T>(this.behaviorData[name]);
			} catch (Exception e) {
				Debug.LogError("Error deserializing value: " + e.Message);
				Debug.LogError(e.StackTrace);
			}
		}
		return null;
	}

	public void SetData<T>(string name, T data) where T : class {
		try {
			behaviorData[name] = JsonConvert.SerializeObject(data);
		} catch (Exception e) {
			Debug.LogError("Error serializing value: " + e.Message);
			Debug.LogError(e.StackTrace);
		}
	}


}

