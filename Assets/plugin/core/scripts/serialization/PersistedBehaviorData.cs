using System;
using System.Collections.Generic;
using UnityEngine;

public class PersistedBehaviorData
{
	public List<PersistedBehaviorProperty> properties = new List<PersistedBehaviorProperty>();
	
	private PersistedBehaviorProperty getProperty(string property) {
		List<PersistedBehaviorProperty> props = getListProperty(property);
		return props.Count > 0 ? props[0] : null;
	}
	
	private List<PersistedBehaviorProperty> getListProperty(string property) {
		List<PersistedBehaviorProperty> result = new List<PersistedBehaviorProperty>();
		foreach(PersistedBehaviorProperty prop in properties) {
			if (prop.name == property) {
				result.Add(prop);
			}
		}
		return result;
	}
	
	public bool IsSet(string property) {
		return getProperty(property) != null;
	}

	public void ClearProperty(string property) {
		List<PersistedBehaviorProperty> toDelete = new List<PersistedBehaviorProperty>();
		foreach (PersistedBehaviorProperty prop in this.properties) {
			if (prop.name == property) {
				toDelete.Add(prop);
			}
		}
		foreach (PersistedBehaviorProperty prop in toDelete) {
			properties.Remove(prop);
		}
	}
	
	public void SetStringListValue(string name, List<string> value) {
		foreach(string item in value) {
			SetValue (name, item);
		}
	}
	
	public void SetIntListValue(string name, List<int> value) {
		foreach (int i in value) {
			SetIntValue (name, i);
		}
	}
	
	public void SetFloatListValue(string name, List<float> value) {
		foreach (float i in value) {
			SetFloatValue(name, i);
		}
	}
	
	public void SetVector3ListValue(string name, List<Vector3> value) {
		foreach(Vector3 v in value) {
			SetStringValue(name, serializeVector3(v));
		}
	}
	
	public void SetCoordinatesListValue(string name, List<Coordinates> value) {
		foreach (Coordinates c in value) {
			SetStringValue(name, serializeVector3(c.ToVector()));
		}
	}
	
	public void SetStringValue(string name, string value) {
		SetValue (name, value);
	}
	
	public void SetBoolValue(string name, bool value) {
		SetValue (name, value);
	}
	
	public void SetIntValue(string name, int value) {
		SetValue(name, value);
	}
	
	public void SetFloatValue(string name, float value) {
		SetValue(name, value);
	}
	
	private void SetValue(string name, object value) {
		if (value != null) {
			string s = value.ToString();
			properties.Add(new PersistedBehaviorProperty(name, s));
		}
	}
	
	private string serializeVector3(Vector3 value) {
		return value.x + "," + value.y + "," + value.z;
	}
	
	public void SetVector3Value(string name, Vector3 value) {
		string s = serializeVector3(value);
		properties.Add(new PersistedBehaviorProperty(name, s));
	}
	
	private Vector3 parseVector3(string value) {
		string[] parts = value.Split(',');
		return new Vector3(float.Parse(parts[0]), float.Parse(parts[1]), float.Parse(parts[2]));
	}
	
	private Coordinates parseCoordinates(string value) {
		return new Coordinates(parseVector3(value));
	}
	
	public Vector3 GetVector3Value(string property) {
		PersistedBehaviorProperty prop = getProperty(property);
		return parseVector3(prop.value);
	}
	
	public void SetCoordinatesValue(string name, Coordinates value) {
		SetVector3Value(name, value.ToVector());
	}
	
	public Coordinates GetCoordinatesValue(string property) {
		return new Coordinates(GetVector3Value(property));
	}
	
	public string GetStringValue(string property) {
		PersistedBehaviorProperty prop = getProperty(property);
		return prop == null ? null : prop.value;
	}
	
	public int GetIntValue(string property) {
		PersistedBehaviorProperty prop = getProperty(property);
		return prop == null ? 0 : int.Parse(prop.value);
	}
	
	public float GetFloatValue(string property) {
		PersistedBehaviorProperty prop = getProperty(property);
		return prop == null ? 0 : float.Parse(prop.value);
	}
	
	public bool GetBoolValue(string property) {
		PersistedBehaviorProperty prop = getProperty(property);
		return prop == null ? false : bool.Parse(prop.value);
	}
	
	public List<string> GetStringListValue(string property) {
		List<PersistedBehaviorProperty> props = getListProperty(property);
		List<string> result = new List<string>();
		foreach (PersistedBehaviorProperty prop in props) {
			result.Add(prop.value);
		}
		return result;
	}
	
	public List<int> GetIntListValue(string property) {
		List<string> strings = GetStringListValue(property);
		List<int> result = new List<int>();
		foreach(string s in strings) {
			result.Add(int.Parse(s));
		}
		return result;
	}
	
	public List<float> GetFloatListValue(string property) {
		List<string> strings = GetStringListValue(property);
		List<float> result = new List<float>();
		foreach(string s in strings) {
			result.Add(float.Parse(s));
		}
		return result;
	}
	
	public List<Vector3> GetVector3ListValue(string property) {
		List<string> strings = GetStringListValue(property);
		List<Vector3> result = new List<Vector3>();
		foreach(string s in strings) {
			result.Add(parseVector3(s));
		}
		return result;
	}
	
	public List<Coordinates> GetCoordinatesListValue(string property) {
		List<string> strings = GetStringListValue(property);
		List<Coordinates> result = new List<Coordinates>();
		foreach(string s in strings) {
			result.Add(parseCoordinates(s));
		}
		return result;
	}
}


