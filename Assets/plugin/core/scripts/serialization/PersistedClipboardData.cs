//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;

public class PersistedClipboardData
{
	public string name;
	public string minCorner;
	public string maxCorner;
	public string rotation;
	public List<PersistedObject> blocks = new List<PersistedObject>();
}


