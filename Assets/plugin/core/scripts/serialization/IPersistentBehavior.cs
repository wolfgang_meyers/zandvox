using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

public interface IPersistentBehavior
{
	void Save(PersistedObject obj);
	void Restore(PersistedObject obj);
}


