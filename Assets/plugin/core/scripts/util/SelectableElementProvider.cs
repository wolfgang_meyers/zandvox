﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public class SelectableElementProvider : SelectableElement, ISelectableStore {

	public SelectableElementProvider() : this(typeof(ISelectableElement)) { }

	public SelectableElementProvider(System.Type elementType) {
		this.elementType = elementType;
	}

	private List<ISelectableElement> elements = new List<ISelectableElement>();
	private System.Type elementType;
	private IDictionary<string, ISelectableElement> elementIndex = new Dictionary<string, ISelectableElement>();

	protected virtual bool ShouldAddElement<T>(T element) where T : ISelectableElement {
		return elementType.IsInstanceOfType(element) && !elements.Contains(element);
	}

	public bool AddElement<T>(T element) where T : ISelectableElement{
		//This might be slower than it could be, but prevents
		//invalid elements from being added and should only
		//be run once at startup time.
		if (ShouldAddElement(element)) {
			elements.Add(element);
			elementIndex[element.DisplayName] = element;
			return true;
		}
		return false;
	}

	public int AddElements<T>(params T[] elements) where T : ISelectableElement {
		return AddElements<T>(new List<T>(elements));
	}

	public int AddElements<T>(IEnumerable<T> elements) where T : ISelectableElement {
		int count = 0;
		foreach (T element in elements) {
			if (AddElement<T>(element)) {
				count += 1;
			}
		}
		return count;
	}

	public bool RemoveElement(string name) {
		ISelectableElement element = GetElement(name);
		if (element == null) {
			return false;
		} else {
			elements.Remove(element);
			elementIndex.Remove(name);
			return true;
		}
	}

	#region ISelectableProvider implementation

	public virtual ISelectableElement GetDefaultElement ()
	{
		return elements.Count > 0 ? elements[0] : null;
	}

	public ISelectableElement GetElement (string name)
	{
		if (elementIndex.ContainsKey(name)) {
			return elementIndex[name];
		}
		return null;
	}

	#endregion

	#region IEnumerable implementation

	public IEnumerator<ISelectableElement> GetEnumerator ()
	{
		return elements.GetEnumerator();
	}

	#endregion

	#region IEnumerable implementation

	System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator ()
	{
		return elements.GetEnumerator();
	}

	#endregion
}
