﻿using UnityEngine;
using System.Collections;

[System.Serializable()]
public class InputBinding : SelectableElement {

	/// <summary>
	/// This allows a component to gain exclusive access to the controls
	/// by limiting input bindings to a certain group.
	/// </summary>
	public string inputGroup;

	/// <summary>
	/// The modifier must be held down in order for this input binding to fire
	/// </summary>
	public KeyCode defaultModifierKey;
	/// <summary>
	/// This key, if pressed, will cause the input binding to fire
	/// </summary>
	public KeyCode defaultKey;

	public KeyCode modifierKey;

	public KeyCode key;

	public bool ignoreGroups = false;

	private KeyCode ModifierKey {
		get {
			return modifierKey == KeyCode.None ? defaultModifierKey : modifierKey;
		}
	}

	private KeyCode Key {
		get {
			return key == KeyCode.None ? defaultKey : key;
		}
	}

	public string GetInstruction() {
		if (modifierKey == KeyCode.None) {
			return "Press " + key.ToString();
		} else {
			return "Hold " + modifierKey.ToString() + " and press " + key.ToString();
		}
	}

	public bool GetKeyDown() {
		bool modifier = ModifierKey == KeyCode.None || Input.GetKey(ModifierKey);
		return Key == KeyCode.None ? false : modifier && Input.GetKeyDown(Key);
	}

	public bool GetKey() {
		bool modifier = ModifierKey == KeyCode.None || Input.GetKey(ModifierKey);
		return Key == KeyCode.None ? false : modifier && Input.GetKey(Key);
	}

	public bool GetKeyUp() {
		bool modifier = ModifierKey == KeyCode.None || Input.GetKey(ModifierKey);
		return Key == KeyCode.None ? false : modifier && Input.GetKeyUp(Key);
	}
}
