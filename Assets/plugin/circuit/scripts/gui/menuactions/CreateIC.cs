﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class CreateIC : MonoBehaviour {

	void PropagateContext(PersistedClipboardData ctx) {
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		Plugin blocksPlugin = pluginRegistry.GetPlugin("Blocks");

		//for building the IC outer leads
		Block icLeadPrototype = (Block)blocksPlugin.GetElement("ICOuterLead");
		Block icBlueprintPrototype = (Block)blocksPlugin.GetElement("ICBlueprint");

		PersistencePlugin persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
		List<Block> blocks = persistencePlugin.DeserializeElements<Block>(blocksPlugin, ctx.blocks);
		List<ICInnerLead> leads = blocks.ConvertAll(b => b.GetComponent<ICInnerLead>()).FindAll(lead => lead != null);

		IsolationSystem circuit = null;
		//attempt to discover groups of leads
		//a group of leads is aligned on one axis
		//there must be 2 groups, one to act as inputs
		//and one to act as outputs. Which acts as input
		//and which output is up to the designer, but
		//there must be 2.
		List<List<ICInnerLead>> alignedLeads = AlignLeads(leads);
		if (alignedLeads != null) {
			//first element of first row is the blueprint
			//all other blocks are ICLead
			//create 2 rows aligned to x axis, z axis
			//first row is the longer of the two
			alignedLeads = alignedLeads.OrderBy(lst => -lst.Count).ToList();
			List<ICInnerLead> innerRow1 = alignedLeads[0];
			List<ICInnerLead> innerRow2 = alignedLeads[1];
			//keep track of outer rows that are about to be built
			//set both outer rows to size of largest inner row
			List<Block> outerRow1 = new List<Block>(innerRow1.Count);
			List<Block> outerRow2 = new List<Block>(innerRow1.Count);
			Block icBlueprint = (Block)Instantiate(icBlueprintPrototype);
			//build row 1
			int left = 0 - innerRow1.Count / 2;
			icBlueprint.MoveTo(new Coordinates(left, 0, 0));
			outerRow1.Add(icBlueprint);
			//add other leads
			for (int x = 1; x < innerRow1.Count; x++) {
				Block outerLead = (Block)Instantiate(icLeadPrototype);
				outerLead.MoveTo(new Coordinates(left + x, 0, 0));
				outerRow1.Add(outerLead);
			}
			//build row 2
			for (int x = 0; x < innerRow1.Count; x++) {
				Block outerLead = (Block)Instantiate(icLeadPrototype);
				outerLead.MoveTo(new Coordinates(left + x, 0, 1));
				outerRow2.Add(outerLead);
			}
			//associate leads in outerRow1 with leads in innerRow1
			//as long as the labels match, the ICBlueprint will connect
			//the circuit properly shortly after instantiation
			for (int i = 0; i < outerRow1.Count; i++) {
				string label = innerRow1[i].GetComponent<LabelBehavior>().label;
				outerRow1[i].SendMessage("UpdateLabel", label);
			}
			//TODO: center leads in second row
			for (int i = 0; i < outerRow2.Count; i++) {
				string label = null;
				if (i < innerRow2.Count) {
					label = innerRow2[i].GetComponent<LabelBehavior>().label;
				} else {
					label = "-";
				}
				outerRow2[i].SendMessage("UpdateLabel", label);
			}
			//store circuit inside of blueprint
			circuit = icBlueprint.GetComponent<IsolationSystem>();
			foreach (Block block in blocks) {
				circuit.AddBlock(block);
			}
			//capture the two rows into a new clipboard object
			PersistedClipboardData clipboardData = new PersistedClipboardData();
			clipboardData.name = "IC:" + ctx.name;
			clipboardData.blocks = new List<PersistedObject>();
			clipboardData.blocks.AddRange(persistencePlugin.SerializeElements(outerRow1));
			clipboardData.blocks.AddRange(persistencePlugin.SerializeElements(outerRow2));
			clipboardData.minCorner = (new Coordinates()).ToString();
			clipboardData.maxCorner = (new Coordinates()).ToString();
			//add to clipboard
			ClipboardController clipboard = GameObject.FindObjectOfType<ClipboardController>();
			clipboard.AddClip(clipboardData);
			//cleanup
			foreach (Block block in outerRow1) {
				Destroy (block.gameObject);
			}
			foreach (Block block in outerRow2) {
				Destroy (block.gameObject);
			}
		}

		//cleanup
		foreach (Block block in blocks) {
			Destroy (block.gameObject);
		}
	}

	//align leads to an axis such that 2 groups of aligned leads
	//are returned. If 2 distinct groups cannot be found,
	//null is returned.
	private List<List<ICInnerLead>> AlignLeads(List<ICInnerLead> leads) {
		List<List<ICInnerLead>> scanX = AlignLeads(leads, c => c.X);
		if (scanX.Count == 2) {
			return scanX;	
		} else {
			List<List<ICInnerLead>> scanY = AlignLeads(leads, c => c.Y);
			if (scanY.Count == 2) {
				return scanY;
			} else {
				List<List<ICInnerLead>> scanZ = AlignLeads(leads, c => c.Z);
				if (scanZ.Count == 2) {
					return scanZ;
				} else {
					//TODO: show meaningful error?
					Debug.LogError("Unable to align leads into 2 groups");
					return null;
				}
			}
		}
	}

	//align leads into groups based on an individual axis
	private List<List<ICInnerLead>> AlignLeads(List<ICInnerLead> leads, System.Func<Coordinates, float> f) {
		Dictionary<float, List<ICInnerLead>> lookup = new Dictionary<float, List<ICInnerLead>>();
		foreach (ICInnerLead lead in leads) {
			float axis = f(lead.GetComponent<Block>().Position);
			if (!lookup.ContainsKey(axis)) {
				lookup[axis] = new List<ICInnerLead>();
			}
			lookup[axis].Add(lead);
		}
		return lookup.Values.ToList();
	}
}
