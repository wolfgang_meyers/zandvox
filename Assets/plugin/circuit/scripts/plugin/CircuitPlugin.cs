﻿using UnityEngine;
using System.Collections.Generic;

public class CircuitPlugin : Plugin {

	public List<Block> blocks = new List<Block>();
	public GameObject createICAction;

	public override void InitializePlugin ()
	{
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		Plugin blocksPlugin = pluginRegistry.GetPlugin("Blocks");
		foreach (Block block in blocks) {
			blocksPlugin.AddElement(block);
		}
		//register "Create Integrated Circuit" action with clipboard menu
		Plugin clipboardActionsPlugin = pluginRegistry.GetPlugin("Clipboard Actions");
		MenuOption icOption = new MenuOption("Create Integrated Circuit", "Uses the ICLead blocks in the captured area to assemble an integrated circuit. ICLead blocks must form exactly 2 rows that are aligned on the same axis, and all labels (other than '-') must be unique in order for creation of the integrated circuit to succeed.", createICAction);
		clipboardActionsPlugin.AddElement(icOption);
	}
}
