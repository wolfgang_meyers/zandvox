﻿using UnityEngine;
using System.Collections;

public class BinaryPushButton : BinaryLogicConductor {

	public float delay = 0.5f;

	IEnumerator SwitchOff() {
		yield return new WaitForSeconds(delay);
		BinaryPulse(false);
	}
	
	void OnInteract() {
		BinaryPulse(true);
		StartCoroutine("SwitchOff");
	}

	public override void BinaryPulse (bool on)
	{
		base.BinaryPulse (on);
		if (on) {
			StartCoroutine(SwitchOff());
		}
		SendMessage("ButtonState", on);
	}
}
