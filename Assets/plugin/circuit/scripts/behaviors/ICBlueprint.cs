﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Block), typeof(IsolationSystem), typeof(BlockGroupBehavior))]
[RequireComponent(typeof(ICLead))]
public class ICBlueprint : MonoBehaviour {

	private bool adjustingBlueprint = false;

	// Use this for initialization
	void Start () {
		AdjustBlueprint();
	}

	private Dictionary<string, T> IndexLeads<T>(IEnumerable<Block> blocks) where T : ICLead {
		return blocks.Where(b => b.GetComponent<T>() != null && b.GetComponent<LabelBehavior>() != null
		                    && b.GetComponent<LabelBehavior>().label != "-")
			.ToList().ConvertAll(b => b.GetComponent<T>())
				.ToDictionary(lead => lead.GetComponent<LabelBehavior>().label);
	}

	void AdjustBlueprint() {
		if (!adjustingBlueprint) {
			adjustingBlueprint = true;
			StartCoroutine("SetupIC");
		}
	}

	IEnumerator SetupIC() {
		//10 seconds or so?
		int tries = 100;
		//allow time for the block group to connect
		//and for isolation system to restore itself
		//this happens on region load and clipboard paste
		yield return new WaitForSeconds(0.1f);
		if (GetComponent<Block>().Environment != null) {
			IsolationSystem circuit = GetComponent<IsolationSystem>();
			while (circuit.IsLoading && tries > 0) {
				yield return new WaitForSeconds(0.1f);
				tries -= 1;
			}
			BlockGroupBehavior groupBehavior = GetComponent<BlockGroupBehavior>();

			Dictionary<string, ICOuterLead> outerLeads = IndexLeads<ICOuterLead>(groupBehavior.Group);
			Dictionary<string, ICInnerLead> innerLeads = IndexLeads<ICInnerLead>(circuit);
			foreach (string label in outerLeads.Keys) {
				if (innerLeads.ContainsKey(label)) {
					outerLeads[label].RemoteLead = innerLeads[label].gameObject;
					innerLeads[label].RemoteLead = outerLeads[label].gameObject;
				}
			}
			//TODO: listen for destruction of any outer leads, destroy entire
			//circuit if that happens
		}
		adjustingBlueprint = false;
	}
}
