﻿using UnityEngine;
using System.Collections;

public class ICLead : BinaryLogicConductor {

	private GameObject remoteLead;

	public GameObject RemoteLead {
		get {
			return remoteLead;
		}
		set {
			remoteLead = value;
		}
	}

	protected override bool ShouldConnect (Block neighbor)
	{
		//this block shouldn't conduct signals to neighboring ICLead blocks
		return base.ShouldConnect(neighbor) && neighbor.GetComponent<ICLead>() == null;
	}

	override public void BinaryPulse (bool on)
	{
		bool fire = on != this.On;
		base.BinaryPulse (on);
		if (RemoteLead != null && fire) {
			RemoteLead.SendMessage("BinaryPulse", on);
		}
	}
}
