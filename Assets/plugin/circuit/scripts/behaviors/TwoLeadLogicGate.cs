﻿using UnityEngine;
using System.Collections;

public abstract class TwoLeadLogicGate : BinaryComponent {

	/// <summary>
	/// Set to true to treat this as the NXXX version of the gate
	/// (NOR, NAND, XNOR, etc.)
	/// </summary>
	public bool inverted;
	private IBlockSystem env;
	private Block block;
	
	private BinaryLogicConductor input1;
	private BinaryLogicConductor input2;
	//other kinds of blocks might be receivers for BinaryPulse event,
	//but might not be conductors
	private BinaryComponent output;

	private bool lastInputState1 = false;
	private bool lastInputState2 = false;

	//flag to prevent doubling of coroutines
	private bool computing = false;
	private float lastCompute = 0;

	public float latency = 0.05f;
	
	void Start() {
		block = GetComponent<Block>();
		env = block.Environment;
		discoverLeads();
		StartCoroutine(ComputeOutput());
	}
	
	private void discoverLeads() {
		//seems this might be called before Start()
		if (env != null && block != null) {
			Vector3 forward = gameObject.transform.forward;
			Vector3 right = gameObject.transform.right;
			Vector3 left = -right;
			
			Block outputBlock = env.GetBlockAt(new Coordinates(forward) + block.Position);
			output = outputBlock == null ? null : outputBlock.GetComponent<BinaryComponent>();
			Block blockInput1 = env.GetBlockAt(new Coordinates(left) + block.Position);
			Block blockInput2 = env.GetBlockAt(new Coordinates(right) + block.Position);
			if (output != null && blockInput1 != null && blockInput2 != null) {
				input1 = (BinaryLogicConductor)blockInput1.GetComponent(typeof(BinaryLogicConductor));
				input2 = (BinaryLogicConductor)blockInput2.GetComponent(typeof(BinaryLogicConductor));
			} else {
				input1 = null;
				input2 = null;
			}
		}
	}
	
	void WakeUp() {
		discoverLeads();
		SafeComputeOutput();
	}
	
	override public void BinaryPulse(bool on) {
		SafeComputeOutput();
	}

	void SafeComputeOutput() {
		if (!computing) {
			if (Time.time - latency > lastCompute) {
				ComputeOutputImmediately();
			} else {
				computing = true;
				StartCoroutine(ComputeOutput());
			}
		}
	}
	
	protected abstract bool computeOutput(bool input1, bool input2);

	private void ComputeOutputImmediately() {
		if (enabled) {
			if (input1 != null && input2 != null && output != null) {
				if (lastInputState1 != input1.On || lastInputState2 != input2.On) {
					lastInputState1 = input1.On;
					lastInputState2 = input2.On;
					bool result = computeOutput(input1.On, input2.On);
					output.BinaryPulse(inverted ? !result : result);
				}
			}
		}
		lastCompute = Time.time;
	}

	IEnumerator ComputeOutput() {
		yield return new WaitForSeconds(latency);
		ComputeOutputImmediately();
		computing = false;
	}
}
