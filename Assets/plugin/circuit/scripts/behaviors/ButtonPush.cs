﻿using UnityEngine;
using System.Collections;

public class ButtonPush : MonoBehaviour, IPersistentBehavior {

	public Vector3 onPosition = new Vector3(0, 0.15f, 0);
	public Vector3 offPosition = new Vector3(0, 0.25f, 0);

	public Material offMaterial;
	public Material onMaterial;

	private bool on = false;
	private GameObject button;
	private bool initialized = false;

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			try {
				button = transform.FindChild("Button").gameObject;
			} catch (System.NullReferenceException) {
				Debug.LogError("No button found!");
			}
		}
	}

	// Use this for initialization
	void Start () {
		Initialize();
	}

	void ButtonState(bool on) {
		Initialize();
		this.on = on;
		button.transform.localPosition = on ? onPosition : offPosition;
		button.renderer.material = on ? onMaterial : offMaterial;
	}

	#region IPersistentBehavior implementation
	public void Save (PersistedObject obj)
	{
		Initialize();
		PersistedBehaviorData data = new PersistedBehaviorData();
		data.SetBoolValue("on", this.on);
		obj.SetData<PersistedBehaviorData>("ButtonPush", data);
	}
	public void Restore (PersistedObject obj)
	{
		Initialize();
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("ButtonPush");
		if (data != null) {
			ButtonState(data.GetBoolValue("on"));
		}
	}
	#endregion
}
