﻿using UnityEngine;
using System.Collections;

public class ClockIndicator : MonoBehaviour{

	public Material indicatorOnMaterial;
	public Material indicatorOffMaterial;
	public string indicatorName = "Indicator";

	private GameObject indicator;
	private bool initialized = false;
	
	private void Initialize() {
		if (!initialized) {
			initialized = true;
			try {
				indicator = transform.FindChild(indicatorName).gameObject;
			} catch (System.NullReferenceException) {
				Debug.LogError("Indicator not found");
			}
		}
	}

	public void ClockState(bool on) {
		Initialize();
		indicator.renderer.material = on ? indicatorOnMaterial : indicatorOffMaterial;
	}

	// Use this for initialization
	void Start () {
		Initialize();
	}
}
