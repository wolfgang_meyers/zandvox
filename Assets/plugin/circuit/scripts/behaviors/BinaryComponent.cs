﻿using UnityEngine;
using System.Collections;

public abstract class BinaryComponent : MonoBehaviour {

	public abstract void BinaryPulse(bool on);
}
