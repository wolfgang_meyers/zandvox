﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AndGate : TwoLeadLogicGate {
	
	protected override bool computeOutput (bool input1, bool input2)
	{
		return input1 && input2;
	}
	
}
