﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// The entire purpose of this class is to alert the ICBlueprint when blocks
/// are either added or removed from an integrated circuit - this is to support
/// lazy load scenarios where an integrated circuit is spread across multiple chunks
/// or regions, or in lazy-loading isolation systems.
/// </summary>
[RequireComponent(typeof(BlockGroup))]
public class ICBlockGroupWatcher : MonoBehaviour {

	public float watchDelay = 0.1f;
	private bool adjusting = false;

	private void BlockAdded(Block block) {
		ScheduleAdjustment();
	}

	private void BlockRemoved(Block block) {
		ScheduleAdjustment();
	}

	private void ScheduleAdjustment() {
		if (!adjusting) {
			adjusting = true;
			StartCoroutine(AdjustBlueprint());
		}
	}

	System.Collections.IEnumerator AdjustBlueprint() {
		yield return new WaitForSeconds(watchDelay);
		BlockGroup bg = GetComponent<BlockGroup>();
		if (bg.Any(b => b.GetComponent<ICBlueprint>() != null)) {
			Block bp = bg.First(b => b.GetComponent<ICBlueprint>() != null);
			bp.SendMessage("AdjustBlueprint");
		}
		adjusting = false;
	}
}
