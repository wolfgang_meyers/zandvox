﻿using UnityEngine;
using System.Collections;

public class BinaryLED : BinaryLogicConductor, IPersistentBehavior {
	
	public Material onMaterial;
	public Material offMaterial;
	
	override public void BinaryPulse(bool on) {
		base.BinaryPulse(on);
		gameObject.renderer.material = on ? onMaterial : offMaterial;
	}

	#region IPersistentBehavior implementation

	override public void Restore (PersistedObject obj)
	{
		base.Restore(obj);
		BinaryPulse(On);
	}
	#endregion
}
