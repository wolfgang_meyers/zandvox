﻿using UnityEngine;
using System.Collections;

public class OrGate : TwoLeadLogicGate {
	
	protected override bool computeOutput (bool input1, bool input2)
	{
		return input1 || input2;
	}
	
}
