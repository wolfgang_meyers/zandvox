﻿using UnityEngine;
using System.Collections;

public class BinaryClock : BinaryLogicConductor {

	public float delay = 0.5f;

	private bool running = false;
	private bool cycling = false;

	//TODO: start ticking on signal, or by default?
	//TODO: use interaction to change delay between ticks
	void OnInteract() {
		running = !running;
		if (running && !cycling) {
			StartCoroutine("ClockCycle");
		}
	}

	IEnumerator ClockCycle() {
		cycling = true;
		yield return new WaitForSeconds(delay);
		if (enabled && running) {
			SendMessage("ClockState", !On, SendMessageOptions.DontRequireReceiver);
			BinaryPulse(!On);
			StartCoroutine("ClockCycle");
			cycling = true;
		} else {
			cycling = false;
		}
	}
}
