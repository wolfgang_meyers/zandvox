﻿using UnityEngine;
using System.Collections;

public class BinaryToggleButton : BinaryLogicConductor, IPersistentBehavior {
	
	override public void BinaryPulse(bool on) {
		base.BinaryPulse(on);
		SendMessage("ButtonState", on);
	}
	
	void OnInteract() {
		BinaryPulse(!On);
	}

	override public void Restore (PersistedObject obj)
	{
		base.Restore (obj);
		SendMessage("ButtonState", On);
	}
}
