﻿using UnityEngine;
using System.Collections.Generic;

public class BinaryLogicConductor : BinaryComponent, IPersistentBehavior {
	
	private bool on = false;
	public List<string> insulateFrom = new List<string>();

	private List<BinaryComponent> connected = new List<BinaryComponent>();

	void WakeUp() {
		Block block = GetComponent<Block>();
		if (block.Environment != null) {
			connected.Clear();
			foreach (Coordinates c in block.Position.GetNeighbors()) {
				Block neighbor = block.Environment.GetBlockAt(c);
				if (ShouldConnect(neighbor)) {
					connected.Add(neighbor.GetComponent<BinaryComponent>());
				}
			}
		}
	}
	
	public bool On {
		get { return on; }
		protected set { on = value; }
	}

	// Use this for initialization
	protected virtual void Start () {
		WakeUp();
	}

	protected virtual bool ShouldConnect(Block neighbor) {
		return neighbor != null && neighbor.GetComponent<BinaryComponent>() != null
			&& !insulateFrom.Contains(neighbor.displayName);
	}

	override public void BinaryPulse(bool on) {
		if (on != this.on) {
			this.on = on;
			//Debug.Log("connected:" + connected.Count);
			foreach (BinaryComponent neighbor in connected) {
				neighbor.BinaryPulse(on);
			}
		}
	}

	public virtual void Save (PersistedObject obj)
	{
		PersistedBehaviorData data = new PersistedBehaviorData();
//		data.name = "BinaryLogicConductor";
		data.SetBoolValue("on", on);
		obj.SetData("BinaryLogicConductor", data);
	}

	public virtual void Restore (PersistedObject obj)
	{
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("BinaryLogicConductor");
		if (data != null) {
			On = data.GetBoolValue("on");
		}
	}
}
