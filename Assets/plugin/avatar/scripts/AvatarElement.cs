﻿using UnityEngine;
using System.Collections.Generic;

public class AvatarElement : Plugin {

	public AvatarCharacter avatar;
	public List<AvatarBoneElement> bones = new List<AvatarBoneElement>();

	public override void InitializePlugin ()
	{
		AddElements<AvatarBoneElement>(bones);
	}
}
