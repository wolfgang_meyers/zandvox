﻿using UnityEngine;
using System.Collections;

public class AvatarSurvivalMode : MonoBehaviour {

	public QuitButton quitButton;
	public AvatarCharacter avatar;
	public ElementSelector inventorySelector;

	private Environment env;

	// Use this for initialization
	void Start () {
		env = GameObject.FindObjectOfType<Environment>();
		Instantiate (quitButton);

		avatar = (AvatarCharacter)Instantiate(avatar);
		AvatarController controller = avatar.GetComponent<AvatarController>();
		env.PerspectiveController = controller;
		AvatarInventory inv = avatar.GetComponent<AvatarInventory>();
		inv.enabled = true;
		inventorySelector = (ElementSelector)Instantiate(inventorySelector);
		inventorySelector.provider = inv;
		AvatarInteraction interaction = avatar.GetComponent<AvatarInteraction>();
		interaction.enabled = true;

		//TODO: inventory, health, hunger, thirst
		//TODO: armor, weapons, tools
		//TODO: seed planting, etc.
		//TODO: block placement

		//TODO: (editor) 3d cursor to replace crappy numeric adjuster ui
	}

	void OnDisable() {
		Destroy (avatar.gameObject);
		Destroy (inventorySelector.gameObject);
		env.PerspectiveController = null;
		env.Quit();
		//TODO: destroy any other elements that were added on startup
	}
}
