﻿using UnityEngine;
using System.Collections;

public class AvatarInteraction : MonoBehaviour {

	public float maxInteractDistance = 5f;
	private Environment env;
	private GUIRegions guiRegions;
	private Plugin blocksPlugin;
	private FrameWizard frameWizard;

	public Material interactionCursorMaterial;

	private Vector3 buildOrientation = Vector3.forward;
	private GameObject cursor;

	private Vector3 lastTargetPosition;
	private Vector3 lastTargetHitNormal;

	private AvatarInventory inventory;
	private ElementSelector inventoryUI;
	private Plugin avatarItemPlugin;

	private void DestroyCursor() {
		if (cursor != null) {
			Destroy (cursor.gameObject);
		}
		cursor = null;
	}

	private void BuildCursor(Block block, RaycastHit hit) {
		DestroyCursor();
		cursor = frameWizard.BuildCube(
			new Vector3(-0.5f, -0.5f, -0.5f), new Vector3(0.5f, 0.5f, 0.5f), 0.01f, interactionCursorMaterial
		);
		//TODO: if selected block is a tool, use it instead of trying to place it
		if (block.supportsInteraction) {
			cursor.transform.position = block.transform.position;
		} else {
			if (inventoryUI.SelectedElement != null) {
				AvatarItem selectedItem = (AvatarItem)avatarItemPlugin.GetElement(inventoryUI.SelectedElement.DisplayName);
				if (selectedItem.itemType == AvatarItemType.Block) {
					cursor.transform.position = block.transform.position + hit.normal;
					Block preview = (Block)Instantiate((Block)blocksPlugin.GetElement(inventoryUI.SelectedElement.DisplayName));
					preview.GetComponent<Collider>().enabled = false;
					preview.transform.position = cursor.transform.position;
					preview.transform.parent = cursor.transform;
				} else {
					cursor.transform.position = block.transform.position;
				}
			}
		}

	}

	// Use this for initialization
	void Start () {
		env = GameObject.FindObjectOfType<Environment>();
		guiRegions = GameObject.FindObjectOfType<GUIRegions>();
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		blocksPlugin = pluginRegistry.GetPlugin("Blocks");
		avatarItemPlugin = pluginRegistry.GetPlugin("AvatarItems");
		frameWizard = GameObject.FindObjectOfType<FrameWizard>();
		inventory = GameObject.FindObjectOfType<AvatarInventory>();
		inventoryUI = ZUtil.FindObjectByTypeAndName<ElementSelector>("InventorySelector");
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (!guiRegions.IsMouseOverRegion() && Physics.Raycast(ray, out hit)) {
			//test for item first, then for block
			AvatarItem item = hit.transform.GetComponent<AvatarItem>();
			if (item == null) {
				Coordinates c = new Coordinates(hit.transform.position);
				float distance = (hit.transform.position - transform.position).magnitude;
				Block b = env.GetBlockAt(c);
				if (b != null && distance <= maxInteractDistance) {
					//cursor
					if (cursor == null
					    || hit.transform.position != lastTargetPosition
					    || hit.normal != lastTargetHitNormal) {
						BuildCursor(b, hit);
					}
					lastTargetPosition = hit.transform.position;
					lastTargetHitNormal = hit.normal;
					//TODO: place block or interact?
					if (Input.GetMouseButton(0)) {
						//TODO: variable damage based on tool and block
						//b.SendMessage("OnDamage", 0.75f, SendMessageOptions.DontRequireReceiver);
						//TODO: damage target instead
						SendMessage("AvatarDamage", b.gameObject, SendMessageOptions.DontRequireReceiver);
					}
					if (Input.GetMouseButtonDown(1)) {
						if (inventoryUI.SelectedElement != null) {
							//shitty way of placing blocks
							AvatarItem avatarItem = (AvatarItem)avatarItemPlugin.GetElement(inventoryUI.SelectedElement.DisplayName);
							if (avatarItem.itemType == AvatarItemType.Block) {
								Block newBlock = (Block)Instantiate((Block)blocksPlugin.GetElement(avatarItem.DisplayName));
								newBlock.MoveTo(new Coordinates(hit.transform.position + hit.normal));
								env.AddBlock(newBlock);
								inventory.TakeItem(avatarItem.DisplayName, 1);
								return;
							}
						}
						//b.SendMessage("OnInteract", hit, SendMessageOptions.DontRequireReceiver);
						InteractionInfo info = new InteractionInfo();
						info.hit = hit;
						info.target = b.gameObject;
						SendMessageUpwards("AvatarInteract", info, SendMessageOptions.DontRequireReceiver);
					}
				}
			} else {
				//TODO: save/restore? what about quality?
				//maybe just add the object?
				if (Input.GetMouseButtonDown(0)) {
					Destroy (item.gameObject);
					SendMessage("OnResourceHarvested", item.DisplayName);
				}
			}
		} else {
			DestroyCursor();
		}
	}
}
