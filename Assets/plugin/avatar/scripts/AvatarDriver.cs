﻿using UnityEngine;
using System.Collections;

public class AvatarDriver : MonoBehaviour {

	private Animator animator;
	private CharacterController cc;

	public float walkSpeed = 0.05f;
	public float runSpeed = 0.1f;
	public float turnSpeed = 500f;
	public float jumpHeight = 0.5f;
	public float gravity = 20f;
	public float damageDelay = 1f;
	public float interactionDelay = 1f;
	public float defaultDamageAmount = 1f;

	private float verticalVelocity;
	private bool jumping = false;
	private bool interacting = false;
	private bool damaging = false;
	private CollisionFlags collisionFlags;

	private bool immediateSurroundingsLoaded = false;

	private InputPlugin inputPlugin;

	// Use this for initialization
	void Start () {
		animator = (Animator)GetComponent(typeof(Animator));
		cc = (CharacterController)GetComponent(typeof(CharacterController));
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
	}

	void ResetSurroundings() {
		immediateSurroundingsLoaded = false;
	}

	private bool checkImmediateSurroundingsLoaded() {
		if (immediateSurroundingsLoaded) {
			return true;
		}
		Environment env = GameObject.FindObjectOfType<Environment>();
		Coordinates mine = ZUtil.RoundCoordinates(new Coordinates(transform.position), env.chunkSize);
		bool surroundingsLoaded = true;
		for (int x = -env.chunkSize; x <= env.chunkSize; x++) {
			for (int y = -env.chunkSize; y <= env.chunkSize; y++) {
				for (int z = -env.chunkSize; z <= env.chunkSize; z++) {
					Coordinates chunkToTest = ZUtil.RoundCoordinates(new Coordinates(
						mine.X + x, mine.Y + y, mine.Z + z
					), env.chunkSize);
					surroundingsLoaded = surroundingsLoaded && !env.IsChunkLoading(chunkToTest);
				}
			}
		}
		immediateSurroundingsLoaded = surroundingsLoaded;
		return immediateSurroundingsLoaded;
	}

	/*
	 * function CalculateJumpVerticalSpeed (targetJumpHeight : float)
{
	// From the jump height and gravity we deduce the upwards speed 
	// for the character to reach at the apex.
	return Mathf.Sqrt(2 * targetJumpHeight * gravity);
}
	*/
	private float CalculateJumpVerticalSpeed() {
		return Mathf.Sqrt(2 * jumpHeight * gravity);
	}

	private void Jump() {
		if (IsGrounded()) {
			verticalVelocity = CalculateJumpVerticalSpeed();
			jumping = true;
		}
	}

	private void ApplyGravity() {
		if (IsGrounded()) {
			verticalVelocity = 0;
			jumping = false;
		} else {
			verticalVelocity -= gravity * Time.deltaTime;
		}
	}

	private bool IsGrounded() {
		return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
	}

	//interaction is immediate, coroutine resets animation/flag
	void AvatarInteract(InteractionInfo info) {
		if (!interacting) {
			interacting = true;
			animator.SetBool("Interacting", true);
			info.target.SendMessage("OnInteract", info.hit, SendMessageOptions.DontRequireReceiver);
			StartCoroutine("ApplyInteraction", info);
		}
	}

	//damage happens on completion of the coroutine
	void AvatarDamage(GameObject target) {
		if (!damaging) {
			damaging = true;
			animator.SetBool("Mining", true);
			StartCoroutine("ApplyDamage", target);
		}
	}

	// Update is called once per frame
	void Update () {
		if (!checkImmediateSurroundingsLoaded()) {
			return;
		}
//		float h = Input.GetAxis("Horizontal");
//		float v = Input.GetAxis("Vertical");
		float h = 0;
		if (inputPlugin.GetInput("Avatar Turn Left")) {
			h = -1;
		} else if (inputPlugin.GetInput("Avatar Turn Right")) {
			h = 1;
		}
		float v = 0;
		if (inputPlugin.GetInput("Avatar Forward")) {
			v = 1;
		} else if (inputPlugin.GetInput("Avatar Backward")) {
			v = -1;
		}

		transform.Rotate(new Vector3(0, h * turnSpeed * Time.deltaTime, 0));
		bool runkey = inputPlugin.GetInput("Avatar Run");
		Vector3 velocity = ((-transform.forward * v * (runkey ? runSpeed : walkSpeed)) + new Vector3(0, verticalVelocity, 0)) * Time.deltaTime;
		collisionFlags = cc.Move(velocity);

		bool moving = Mathf.Abs(h) + Mathf.Abs(v) > 0.5;
		animator.SetBool("Moving", moving);
		animator.SetBool("Runkey", runkey);
		//Debug.Log((collisionFlags & CollisionFlags.CollidedBelow) == 0);
		animator.SetBool("Jumping", jumping);
		ApplyGravity();
		if (inputPlugin.GetInput("Avatar Jump")) {
			Jump();
		}

	}

	private float calculateDamage(GameObject target) {
		//TODO: calculate based on tool, if one is equipped
		//may also vary based on the target
		//could even repair objects
		return defaultDamageAmount;
	}

	IEnumerator ApplyDamage(GameObject obj) {
		yield return new WaitForSeconds(damageDelay);
		damaging = false;
		animator.SetBool("Mining", false);
		obj.SendMessageUpwards("OnDamage", new GameMessage(gameObject, calculateDamage(obj)), SendMessageOptions.DontRequireReceiver);
	}

	IEnumerator ApplyInteraction(InteractionInfo info) {
		//TODO: longer delay for some items...?
		yield return new WaitForSeconds(interactionDelay);
		interacting = false;
		animator.SetBool("Interacting", false);
	}

	/*
	 *function IsMoving ()  : boolean
{
	return Mathf.Abs(Input.GetAxisRaw("Vertical")) + Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0.5;
}
	 */
}
