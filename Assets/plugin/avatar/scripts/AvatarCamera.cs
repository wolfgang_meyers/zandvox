﻿using UnityEngine;
using System.Collections;

public class AvatarCamera : MonoBehaviour {

	public float maxFollowHeight = 5f;
	public float maxFollowDistance = 10f;

	public float minFollowHeight = 0f;
	public float minFollowDistance = 1f;

	public float followAdjustmentStep = 0.1f;

	public float followHeight = 4f;
	public float followDistance = 4f;
	public float cameraSpringiness = 2f;

	//used to calculate height and distance
	private float zoom = 0.5f;

	public Vector3 targetOffset;

	private bool rear = false;
	// Use this for initialization
	void Start () {

	}

	void HandleZooming ()
	{
		float zoomDelta = Input.GetAxisRaw ("Mouse ScrollWheel");
		if (Mathf.Abs (zoomDelta) > 0.01f) {
			zoom -= zoomDelta * Time.deltaTime;
			if (zoom < 0) {
				zoom = 0;
			} else if (zoom > 1) {
				zoom = 1;
			}
			float heightDiff = (maxFollowHeight - minFollowHeight);
			float distDiff = (maxFollowDistance - minFollowDistance);
			followHeight = minFollowHeight + heightDiff * zoom;
			followDistance = minFollowDistance + distDiff * zoom;

		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.RightBracket)) {
			rear = !rear;
		}
		Camera camera = Camera.main;
		HandleZooming ();
		Vector3 targetposition = transform.position + ((rear ? -transform.forward : transform.forward) * followDistance + new Vector3(0, followHeight, 0)) + targetOffset;
		Vector3 diff = targetposition - camera.transform.position;
		camera.transform.position += diff * Time.deltaTime * cameraSpringiness;
		camera.transform.LookAt(transform.position + targetOffset);

	}
}
