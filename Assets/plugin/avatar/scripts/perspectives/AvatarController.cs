﻿using UnityEngine;
using System.Collections;

public class AvatarController : PerspectiveController {

	#region IPerspectiveController implementation

	public override void DisableController ()
	{
		((MonoBehaviour)GetComponent(typeof(AvatarDriver))).enabled = false;
		((MonoBehaviour)GetComponent(typeof(AvatarCamera))).enabled = false;

	}

	public override void EnableController ()
	{
		SpawnSystem spawnSystem = (SpawnSystem)GameObject.FindObjectOfType(typeof(SpawnSystem));
		Environment env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		spawnSystem.Restore(env.PersistedWorldData);
		IdentityInfo identityInfo = (IdentityInfo)GameObject.FindObjectOfType(typeof(IdentityInfo));
		transform.position = spawnSystem.GetSpawnPoint(identityInfo.UserName);
		((MonoBehaviour)GetComponent(typeof(AvatarDriver))).enabled = true;
		((MonoBehaviour)GetComponent(typeof(AvatarCamera))).enabled = true;
	}

	public override Vector3 Position {
		get {
			return transform.position;
		}

		set {
			transform.position = value;
		}
	}

	#endregion
}
