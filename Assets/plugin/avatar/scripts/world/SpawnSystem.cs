﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnSystem : MonoBehaviour, IPersistentBehavior {

	private Environment env;
	private PersistedBehaviorData spawnPointLookup = new PersistedBehaviorData();

	// Use this for initialization
	void Start () {
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		Restore (env.PersistedWorldData);
	}

	public void SetSpawnPoint(string user, Vector3 spawnPoint) {
		spawnPointLookup.ClearProperty(user);
		spawnPointLookup.SetVector3Value(user, spawnPoint);
		Save (env.PersistedWorldData);
	}

	public Vector3 GetSpawnPoint(string user) {
		if (spawnPointLookup.IsSet(user)) {
			return spawnPointLookup.GetVector3Value(user);
		} else {
			return new Vector3(0, 3, 0);
		}
	}

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		obj.SetData("SpawnSystem", spawnPointLookup);
	}

	public void Restore (PersistedObject obj)
	{
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("SpawnSystem");
		if (data != null) {
			this.spawnPointLookup = data;
		}
	}

	#endregion
}
