﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class AvatarComponentAdjuster : MonoBehaviour {

	public Rect menuRect;
	public Rect modeRect;
	public Rect adjustmentRect;
	public float padding;
	public float adjustmentStep = 1;
	//mode - rotate, scale, translate

	private GUIRegions guiRegions;
	public string mode = "move";
	private bool lockScale = true;

	public AvatarComponent target;
	
	// Use this for initialization
	void Start () {
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
		guiRegions.AddRegion(menuRect);
	}

	void OnDisable() {
		guiRegions.RemoveRegion(menuRect);
	}
	
	void OnGUI() {
		GUIUtil.screenMatrix();
		GUI.Box(menuRect, mode + " component");
		//change mode
		ApplyModeSelection();
		if (target == null) {
			GUI.Label(adjustmentRect, "No component selected");
		} else {
			//adjust object
			if (mode == "move") {
				ApplyTranslation();
			} else if (mode == "rotate") {
				ApplyRotation();
			} else if (mode == "scale") {
				ApplyScale();
			}
		}
	}

	private void ApplyTranslation() {
		Vector3 position = target.transform.position;
		target.transform.position = ApplyAdjustmentAxes(position, adjustmentStep, false);
	}

	private void ApplyRotation() {
		Vector3 rotation = target.transform.eulerAngles;
		Vector3 newRotation = ApplyAdjustmentAxes(rotation, adjustmentStep * 100, false);
		target.transform.Rotate(newRotation - rotation); 
	}

	private void ApplyScale() {
		Rect lockScaleRect = new Rect(adjustmentRect.xMin, adjustmentRect.yMax + (adjustmentRect.height + padding) * 2 + 5, 100, 25);
		if (GUI.Button(lockScaleRect, lockScale ? "Unlock Axes" : "Lock Axes")) {
			lockScale = !lockScale;
		}
		Vector3 scale = target.transform.localScale;
		target.transform.localScale = ApplyAdjustmentAxes(scale, adjustmentStep, lockScale);
	}

	private Vector3 ApplyAdjustmentAxes(Vector3 value, float adjustmentStep, bool lockValues) {
		Rect xRect = adjustmentRect;
		Rect yRect = new Rect(adjustmentRect.xMin, adjustmentRect.yMax + padding, adjustmentRect.width, adjustmentRect.height);
		Rect zRect = new Rect(adjustmentRect.xMin, yRect.yMax + padding, adjustmentRect.width, adjustmentRect.height);
		float x = ApplyAdjustment("X", xRect, value.x, adjustmentStep);
		float y = ApplyAdjustment("Y", yRect, lockValues ? value.x : value.y, adjustmentStep);
		float z = ApplyAdjustment("Z", zRect, lockValues ? value.x : value.z, adjustmentStep);
		return new Vector3(x, y, z);
	}

	private float ApplyAdjustment(string axis, Rect rect, float currentValue, float adjustmentStep) {
		float result = currentValue;
		Rect labelRect = new Rect(rect.xMin, rect.yMin, 25, 25);
		Rect decreaseRect = new Rect(rect.xMin + 25, rect.yMin, 25, 25);
		Rect increaseRect = new Rect(rect.xMax - 25, rect.yMin, 25, 25);
		Rect valueRect = new Rect(((decreaseRect.xMin + increaseRect.xMax) / 2f) - 25, rect.yMin, 50, 25);

		GUI.Label(labelRect, axis + ":");
		if (GUI.RepeatButton(decreaseRect, "<")) {
			result -= adjustmentStep;
		}
		if (GUI.RepeatButton(increaseRect, ">")) {
			result += adjustmentStep;
		}
		try {
			result = float.Parse(GUI.TextField(valueRect, result.ToString("0.00")));
		} catch { }
		return result;
	}

	private void ApplyModeSelection() {
		Rect moveRect = new Rect(modeRect.xMin, modeRect.yMin, modeRect.width / 3, modeRect.height);
		Rect rotateRect = new Rect(modeRect.xMin + modeRect.width / 3, modeRect.yMin, modeRect.width / 3, modeRect.height);
		Rect scaleRect = new Rect(modeRect.xMin + modeRect.width / 1.5f, modeRect.yMin, modeRect.width / 3, modeRect.height);
		ApplyModeToggle(moveRect, "move");
		ApplyModeToggle(rotateRect, "rotate");
		ApplyModeToggle(scaleRect, "scale");
	}

	private void ApplyModeToggle(Rect rect, string mode) {
		if (GUI.Toggle(rect, this.mode == mode, mode)) {
			this.mode = mode;
		}
	}


}
