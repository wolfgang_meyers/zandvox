﻿using UnityEngine;
using System.Collections;

public class LoadCharacterMenu : LoadPersistedItemMenu {
	
	public GameObject resultHandler;
	
	// Use this for initialization
	protected override void Start () {
		persistenceCategoryName = "Characters";
		base.Start();
	}

	protected override void OnResultSelected (string result)
	{
		Destroy(gameObject);
		resultHandler = (GameObject)Instantiate(resultHandler);
		resultHandler.SendMessage("PropagateContext", result, SendMessageOptions.DontRequireReceiver);
	}

}
