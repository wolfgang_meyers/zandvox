﻿using UnityEngine;
using System.Collections;

public class SaveCharacter : MonoBehaviour {

	public QuitButton quitButton;

	// Use this for initialization
	void Start () {
		AvatarCharacter avatar = (AvatarCharacter)GameObject.FindObjectOfType(typeof(AvatarCharacter));
		avatar.Save(avatar.characterName);
		Instantiate(quitButton);
		Destroy (gameObject);
	}
}
