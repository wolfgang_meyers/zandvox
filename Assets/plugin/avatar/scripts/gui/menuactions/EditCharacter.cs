﻿using UnityEngine;
using System.Collections;

public class EditCharacter : MonoBehaviour {

	public AvatarCharacter avatarCharacter;
	//public FlatBiomeGenerator biomeGenerator;
	public GameMode characterEditMode;

	// Use this for initialization
	void PropagateContext(object context) {
		string result = (string)context;
		//Instantiate(biomeGenerator);
		avatarCharacter = (AvatarCharacter)Instantiate(avatarCharacter);
		
		Instantiate (characterEditMode);
		
		avatarCharacter.Load(result);
		avatarCharacter.SendMessage("EditorMode");
		Camera.main.transform.position = avatarCharacter.transform.position + new Vector3(0, 0, 5);
		Camera.main.transform.LookAt(avatarCharacter.transform.position);

		Destroy(gameObject);
	}
}
