﻿using UnityEngine;
using System.Collections;

public class QuitCharacter : MonoBehaviour {

	public GenericMenu mainMenu;
	
	// Use this for initialization
	void Start () {
		Environment env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		env.Quit();
		//destroy any tools
		foreach (object obj in GameObject.FindObjectsOfType(typeof(ElementSwitcher))) {
			Destroy (((ElementSwitcher)obj).gameObject);
		}
		AvatarCharacter avatar = (AvatarCharacter)GameObject.FindObjectOfType(typeof(AvatarCharacter));
		Destroy (avatar.gameObject);
		Destroy(gameObject);
		Instantiate(mainMenu);
	}
}
