﻿using UnityEngine;
using System.Collections;

public class LoadCharacter : MonoBehaviour {

	//this can be different for avatar preview vs survival mode...
	public GameObject escButton;

	void PropagateContext(object context) {
		Destroy (gameObject);
		string result = (string)context;
		AvatarCharacter avatar = GameObject.FindObjectOfType<AvatarCharacter>();
		avatar.Load(result);
		if (escButton != null) {
			Instantiate (escButton);
		}
	}

}
