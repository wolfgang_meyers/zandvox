﻿using UnityEngine;
using System.Collections;

public class QuitSurvival : MonoBehaviour {

	public MainMenu mainMenu;

	// Use this for initialization
	void Start () {
		AvatarSurvivalMode mode = GameObject.FindObjectOfType<AvatarSurvivalMode>();
		Destroy (mode.gameObject);
		Instantiate (mainMenu);
		Destroy (gameObject);
	}
}
