﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class NewCharacterMenu : NewPersistedItemMenu {

	public QuitButton quitButton;

	public AvatarCharacter avatarCharacter;

	public PluginElementSelector toolSelector;
	public FlatBiomeGenerator biomeGenerator;
	public GameMode characterEditMode;

	// Use this for initialization
	protected override void Start () {
		persistenceCategoryName = "Characters";
		base.Start();
	}

	protected override void OnItemCreate (string itemName)
	{
		Instantiate(biomeGenerator);
		persistencePlugin.CreatePersistedItem(persistenceCategory, itemName);
		avatarCharacter = (AvatarCharacter)Instantiate(avatarCharacter);
		avatarCharacter.SendMessage("EditorMode");

		Instantiate (characterEditMode);

		avatarCharacter.Save(itemName);
		avatarCharacter.Load(itemName);

		Camera.main.transform.position = avatarCharacter.transform.position + new Vector3(0, 0, 5);
		Camera.main.transform.LookAt(avatarCharacter.transform.position);
	}

}
