﻿using UnityEngine;
using System.Collections;

public class AvatarComponentCreator : MonoBehaviour {

	public Rect menuRect;
	public float padding = 15;

	public string boneSelectorName = "AvatarBoneSelector";
	public string componentSelectorName = "AvatarComponentSelector";

	private PluginElementSelector boneSelector;
	private PluginElementSelector componentSelector;
	private AvatarComponentAdjuster adjuster;
	private AvatarCharacter avatarCharacter;

	private GUIRegions guiRegions;

	// Use this for initialization
	void Start () {
		boneSelector = ZUtil.FindObjectByTypeAndName<PluginElementSelector>(boneSelectorName);
		componentSelector = ZUtil.FindObjectByTypeAndName<PluginElementSelector>(componentSelectorName);
		avatarCharacter = (AvatarCharacter)GameObject.FindObjectOfType(typeof(AvatarCharacter));
		adjuster = GameObject.FindObjectOfType<AvatarComponentAdjuster>();
		guiRegions = GameObject.FindObjectOfType<GUIRegions>();
		guiRegions.AddRegion(menuRect);
	}

	void OnDisable() {
		guiRegions.RemoveRegion(menuRect);
	}

	void OnGUI() {
		GUIUtil.screenMatrix();
		ISelectableElement component = componentSelector.SelectedElement;
		ISelectableElement bone = boneSelector.SelectedElement;
		GUI.Box(menuRect, "New Component");
		Rect buttonRect = new Rect(menuRect.xMin + padding, menuRect.yMin + padding + 25, menuRect.width - padding * 2, 25);
		if (GUI.Button(buttonRect, "Add " + component.DisplayName + " to " + bone.DisplayName)) {
			avatarCharacter.AddComponent(bone.DisplayName, (AvatarComponent)component);
		}
		buttonRect = new Rect(buttonRect.xMin, buttonRect.yMax + padding, buttonRect.width, buttonRect.height);
		if (adjuster.target != null && GUI.Button(buttonRect, "Copy Selected to " + bone.DisplayName)) {
			PersistedObject tmp = new PersistedObject();
			Component[] components = adjuster.target.GetComponents(typeof(IPersistentBehavior));
			foreach (Component c in components) {
				((IPersistentBehavior)c).Save(tmp);
			}
			AvatarComponent newComponent = avatarCharacter.AddComponent(bone.DisplayName, adjuster.target);
			components = newComponent.GetComponents(typeof(IPersistentBehavior));
			foreach (Component c in components) {
				((IPersistentBehavior)c).Restore(tmp);
			}
		}
	}
}
