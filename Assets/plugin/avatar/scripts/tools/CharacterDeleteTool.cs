﻿using UnityEngine;
using System.Collections;

public class CharacterDeleteTool : MonoBehaviour {

	//update objects under the mouse to help
	//prevent accidental deletion
	//TODO: port this to other tools
	public Material hoverMaterial;
	private Material currentObjectMaterial;
	private GUIRegions guiRegions;
	private GameObject hoverObject;

	private GameObject HoverObject {
		get {
			return hoverObject;
		}
		set {
			if (hoverObject != null) {
				hoverObject.renderer.material = currentObjectMaterial;
			}
			hoverObject = value;
			if (hoverObject != null) {
				currentObjectMaterial = hoverObject.renderer.material;
				hoverObject.renderer.material = hoverMaterial;
			}
		}
	}

	// Use this for initialization
	void Start () {
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
	}

	void OnDisable() {
		HoverObject = null;
	}

	// Update is called once per frame
	void Update () {
		if (!guiRegions.IsMouseOverRegion()) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)) {
				HoverObject = hit.transform.gameObject;
				if (HoverObject != null && Input.GetMouseButtonDown(0)) {
					HoverObject = null;
					Destroy(hit.transform.gameObject);
				}
			} else {
				HoverObject = null;
			}
		}
	}
}
