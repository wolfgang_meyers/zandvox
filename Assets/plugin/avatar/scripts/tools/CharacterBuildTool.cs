﻿using UnityEngine;
using System.Collections;

public class CharacterBuildTool : MonoBehaviour {

	public Material previewMaterial;

	public AvatarComponentAdjuster componentAdjuster;
	public PluginElementSelector componentSelector;
	public PluginElementSelector boneSelector;
	public AvatarComponentCreator componentCreator;

	//store the material of the selected object
	//so that it can be restored when the object
	//is no longer selected
	private Material currentObjectMaterial;
	private GUIRegions guiRegions;

	private AvatarComponent CurrentObject {
		get {
			return componentAdjuster.target;
		}
		set {
			if (componentAdjuster.target != null) {
				componentAdjuster.target.renderer.material = currentObjectMaterial;
			}
			componentAdjuster.target = value;
			if (value != null) {
				currentObjectMaterial = value.renderer.material;
				value.renderer.material = previewMaterial;
			}
		}
	}

	// Use this for initialization
	void Start () {
		componentAdjuster = (AvatarComponentAdjuster)Instantiate(componentAdjuster);
		componentSelector = (PluginElementSelector)Instantiate(componentSelector);
		boneSelector = (PluginElementSelector)Instantiate(boneSelector);
		componentCreator = (AvatarComponentCreator)Instantiate(componentCreator);
		guiRegions = (GUIRegions)GameObject.FindObjectOfType(typeof(GUIRegions));
	}

	void OnDisable() {
		CurrentObject = null;
		Destroy (componentSelector);
		Destroy (componentAdjuster);
		Destroy (boneSelector);
		Destroy (componentCreator);
	}
	
	// Update is called once per frame
	void Update () {
		if (!guiRegions.IsMouseOverRegion()) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Input.GetMouseButtonDown(0)) {
				AvatarComponent target = null;
				if (Physics.Raycast(ray, out hit)) {
					GameObject obj = hit.transform.gameObject;
					target = (AvatarComponent)obj.GetComponent(typeof(AvatarComponent));
				}
				CurrentObject = target;
			}
		}
	}
}
