﻿using UnityEngine;
using System.Collections;

public class AvatarPreview : MonoBehaviour {

	public GameObject avatar;
	public ElementSelector inventoryUI;

	private IPerspectiveController previousPerspective;
	private Environment env;
	private InputPlugin inputPlugin;

	//TODO: remove this temporary hack and add a ui
	//for loading characters
	public LoadCharacterMenu characterSelectionMenu;

	// Use this for initialization
	void Start () {
		env = (Environment)GameObject.FindObjectOfType(typeof(Environment));
		previousPerspective = env.PerspectiveController;
		avatar = (GameObject)Instantiate(avatar);
		AvatarInventory inv = avatar.GetComponent<AvatarInventory>();
		inv.enabled = true;
		inventoryUI = (ElementSelector)Instantiate(inventoryUI);
		inventoryUI.provider = inv;
		avatar.GetComponent<AvatarInteraction>().enabled = true;
		//avatar.transform.position = cameraPos;
		//CharacterController cc = (CharacterController)avatar.GetComponent(typeof(CharacterController));
		env.PerspectiveController = (IPerspectiveController)avatar.GetComponent(typeof(IPerspectiveController));
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
	}

	void OnDisable() {
		Destroy (avatar);
		Destroy (inventoryUI);
		env.PerspectiveController = previousPerspective;
	}

	void Update() {
		//if (Input.GetKeyDown(KeyCode.Semicolon)) {
		if (inputPlugin.GetInputDown("Load Character")) {
			Instantiate (characterSelectionMenu);
		}
	}
}
