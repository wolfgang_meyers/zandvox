﻿using UnityEngine;
using System.Collections;

public class CharacterPaintTool : MonoBehaviour {

	public PluginElementSelector materialSelector;

	private GUIRegions guiRegions;

	// Use this for initialization
	void Start () {
		guiRegions = GameObject.FindObjectOfType<GUIRegions>();
		materialSelector = (PluginElementSelector)Instantiate(materialSelector);
	}
	
	// Update is called once per frame
	void Update () {
		if (!guiRegions.IsMouseOverRegion()) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit)) {
				GameObject obj = hit.transform.gameObject;
				ColorSpec colorSpec = (ColorSpec)materialSelector.SelectedElement;
				obj.SendMessage("PaintObject", colorSpec.DisplayName);
			}
		}
	}

	void OnDisable() {
		Destroy (materialSelector);
	}
}
