﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AvatarInventory : MonoBehaviour, ISelectableProvider, IPersistentBehavior {

	private SelectableElementProvider store = new SelectableElementProvider();
	private Plugin itemPlugin;

	void OnResourceHarvested(string itemName) {
		AddItem(itemName, 1);
	}

	void Start() {
		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		itemPlugin = pluginRegistry.GetPlugin("AvatarItems");
	}

	public void AddItem(string name, int quantity) {
		Debug.Log("AddItem: name=" + name + ", quantity=" + quantity);
		//TODO: do some kind of capacity checking...
		AvatarInventoryItem invItem = (AvatarInventoryItem)store.GetElement(name);
		if (invItem == null) {
			invItem = new AvatarInventoryItem();
			AvatarItem item = (AvatarItem)itemPlugin.GetElement(name);
			if (item == null) {
				Debug.LogError("Attempt to add unknown item to inventory: " + name);
			} else {
				ZUtil.CopyProperties(item, invItem);
				invItem.quantity = quantity;
				store.AddElement(invItem);
			}
		} else {
			Debug.Log("Existing item " + name);
			invItem.quantity += quantity;
		}
		ZUtil.FindObjectByTypeAndName<ElementSelector>("InventorySelector").Refresh();

	}

	//intended to be useful for discarding items or trading with other players
	public AvatarInventoryItem TakeItem(string name, int quantity) {
		AvatarInventoryItem invItem = (AvatarInventoryItem)store.GetElement(name);
		if (invItem != null) {
			ElementSelector invUI = ZUtil.FindObjectByTypeAndName<ElementSelector>("InventorySelector");
			int actualQuantity = Mathf.Min(quantity, invItem.quantity);
			AvatarInventoryItem result = new AvatarInventoryItem();
			ZUtil.CopyProperties(invItem, result);
			result.quantity = actualQuantity;
			invItem.quantity -= actualQuantity;
			if (invItem.quantity <= 0) {
				store.RemoveElement(invItem.DisplayName);
				invUI.UpdateSelectedElement(GetDefaultElement() == null ? null : GetDefaultElement().DisplayName);
			} else {
				invUI.Refresh();
			}
			return result;
		}
		return null;
	}

	#region ISelectableProvider implementation
	public ISelectableElement GetDefaultElement ()
	{
		return store.GetDefaultElement();
	}
	public ISelectableElement GetElement (string name)
	{
		return store.GetElement(name);
	}
	#endregion
	#region IEnumerable implementation
	public IEnumerator<ISelectableElement> GetEnumerator ()
	{
		return store.GetEnumerator();
	}
	#endregion
	#region IEnumerable implementation
	IEnumerator IEnumerable.GetEnumerator ()
	{
		return store.GetEnumerator();
	}
	#endregion

	#region IPersistentBehavior implementation

	public void Save (PersistedObject obj)
	{
		List<AvatarInventoryItem> items = new List<AvatarInventoryItem>();
		foreach (ISelectableElement element in store) {
			items.Add((AvatarInventoryItem)element);
		}
		obj.SetData<List<AvatarInventoryItem>>("AvatarInventory", items);
	}

	public void Restore (PersistedObject obj)
	{
		List<AvatarInventoryItem> items = obj.GetData<List<AvatarInventoryItem>>("AvatarInventory");
		if (items != null) {
			store.AddElements<AvatarInventoryItem>(items);
		}
	}

	#endregion
}
