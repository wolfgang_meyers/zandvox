﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {

	private SpawnSystem spawnSystem;

	void Start() {
		spawnSystem = (SpawnSystem)GameObject.FindObjectOfType(typeof(SpawnSystem));
	}

	void OnInteract(RaycastHit hit) {
		IdentityInfo identityInfo = (IdentityInfo)GameObject.FindObjectOfType(typeof(IdentityInfo));
		Vector3 spawnPoint = transform.position + new Vector3(0, 3, 0);
		spawnSystem.SetSpawnPoint(identityInfo.UserName, spawnPoint);
		Messages messages = (Messages)GameObject.FindObjectOfType(typeof(Messages));
		messages.QueueMessage("Spawn point set to " + spawnPoint);
	}
}
