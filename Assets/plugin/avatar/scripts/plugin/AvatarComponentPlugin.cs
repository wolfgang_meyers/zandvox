﻿using UnityEngine;
using System.Collections.Generic;

public class AvatarComponentPlugin : Plugin {

	public List<AvatarComponent> components = new List<AvatarComponent>();

	protected override System.Type ElementType ()
	{
		return typeof(AvatarComponent);
	}

	public override void InitializePlugin ()
	{
		base.InitializePlugin();
		AddElements<AvatarComponent>(components);
	}
}
