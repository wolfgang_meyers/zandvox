﻿using UnityEngine;
using System.Collections.Generic;

public class AvatarItemPlugin : Plugin {

	public List<AvatarItem> items = new List<AvatarItem>();

	protected override System.Type ElementType ()
	{
		return typeof(AvatarItem);
	}

	public override void InitializePlugin ()
	{
		base.InitializePlugin ();
		AddElements<AvatarItem>(items);

		PluginRegistry pluginRegistry = GameObject.FindObjectOfType<PluginRegistry>();
		//register one item for every block
		Plugin blocksPlugin = pluginRegistry.GetPlugin("Blocks");
		foreach (ISelectableElement element in blocksPlugin) {
			Block block = (Block)Instantiate((Block)element);
			block.transform.position = new Vector3(float.MaxValue, 0, 0);
			block.gameObject.AddComponent<Rigidbody>();
			AvatarItem item = block.gameObject.AddComponent<AvatarItem>();
			ZUtil.CopyProperties(block, item);
			item.itemType = AvatarItemType.Block;
			AddElement(item);
		}
	}

}
