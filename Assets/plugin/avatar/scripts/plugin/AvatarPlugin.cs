﻿using UnityEngine;
using System.Collections.Generic;

public class AvatarPlugin : Plugin {

	public List<AvatarElement> avatars = new List<AvatarElement>();
	public List<InputBinding> inputBindings = new List<InputBinding>();
	public List<Block> blocks = new List<Block>();

	public Tool avatarPreview;
	public GameObject charactersMenu;
	public GameMode characterEditMode;
	public GameMode avatarSurvivalMode;

	protected override System.Type ElementType ()
	{
		return typeof(AvatarElement);
	}

	public override void InitializePlugin ()
	{
		base.InitializePlugin();
		AddElements<AvatarElement>(avatars);
		//make sure each avatar is initialized
		foreach (AvatarElement element in avatars) {
			element.InitializePlugin();
		}

		PluginRegistry pluginRegistry = (PluginRegistry)GameObject.FindObjectOfType<PluginRegistry>();
		Plugin blocksPlugin = pluginRegistry.GetPlugin("Blocks");
		foreach (Block block in blocks) {
			blocksPlugin.AddElement<Block>(block);
		}

		//add avatar preview to the world editor
		Plugin toolsPlugin = pluginRegistry.GetPlugin("Tools");
		toolsPlugin.AddElement(avatarPreview);

		//extend main menu
		Plugin mainMenuPlugin = pluginRegistry.GetPlugin("MainMenu");
		mainMenuPlugin.AddElement(new MenuOption("Characters", "Create and edit characters that can be used as avatars", charactersMenu));

		//extend persistence
		Plugin persistencePlugin = pluginRegistry.GetPlugin("Persistence");
		persistencePlugin.AddElement(new PersistenceCategory("Characters", "characters"));

		//extend game modes
		Plugin gameModesPlugin = pluginRegistry.GetPlugin("GameModes");
		gameModesPlugin.AddElement(characterEditMode);
		gameModesPlugin.AddElement(avatarSurvivalMode);

		//extend input bindings
		InputPlugin inputPlugin = (InputPlugin)pluginRegistry.GetPlugin("Input");
		foreach (InputBinding binding in inputBindings) {
			inputPlugin.AddElement(binding);
		}

	}



}
