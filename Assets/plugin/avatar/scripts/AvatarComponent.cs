﻿using UnityEngine;
using System.Collections;

public class AvatarComponent : PluginElement, IPersistentBehavior {
	#region IPersistentBehavior implementation
	public void Save (PersistedObject obj)
	{
		PersistedBehaviorData data = new PersistedBehaviorData();
		data.SetVector3Value("position", transform.localPosition);
		data.SetVector3Value("rotation", transform.localEulerAngles);
		data.SetVector3Value("scale", transform.localScale);
		obj.SetData("AvatarComponent", data);
	}

	public void Restore (PersistedObject obj)
	{
		PersistedBehaviorData data = obj.GetData<PersistedBehaviorData>("AvatarComponent");
		if (data != null) {
			transform.localPosition = data.GetVector3Value("position");
			transform.localEulerAngles = data.GetVector3Value("rotation");
			transform.localScale = data.GetVector3Value("scale");
		}
	}

	void EditorMode() {
		GetComponent<Collider>().enabled = true;
	}

	void PreviewMode() {
		GetComponent<Collider>().enabled = false;
	}
	#endregion
	//TODO: persist parent bone name
}
