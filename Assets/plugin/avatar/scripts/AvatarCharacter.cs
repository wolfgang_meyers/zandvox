﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class AvatarCharacter : MonoBehaviour {

	//this class can be called on to load avatar definitions
	//before starting - record initialized flag to prevent dual initialization
	//might find a better pattern for this later...
	private bool initialized = false;

	//corresponds to the display name of an AvatarElement
	//in the AvatarPlugin
	public string characterType;
	private PersistencePlugin persistencePlugin;
	private PersistenceCategory characters;
	private AvatarElement avatarElement;
	private AvatarComponentPlugin avatarComponentPlugin;

	public string characterName;
	private Dictionary<string, Transform> boneLookup = new Dictionary<string, Transform>();
	private bool editorMode = false;

	void EditorMode() {
		editorMode = true;
		CharacterController cc = (CharacterController)GetComponent(typeof(CharacterController));
		cc.enabled = false;
		foreach (AvatarComponent cmp in GetComponentsInChildren<AvatarComponent>()) {
			cmp.SendMessage("EditorMode", SendMessageOptions.DontRequireReceiver);
		}
		//BroadcastMessage("EditorMode", SendMessageOptions.DontRequireReceiver);
	}

	void PreviewMode() {
		editorMode = false;
		CharacterController cc = (CharacterController)GetComponent(typeof(CharacterController));
		cc.enabled = true;
		foreach (AvatarComponent cmp in GetComponentsInChildren<AvatarComponent>()) {
			cmp.SendMessage("PreviewMode", SendMessageOptions.DontRequireReceiver);
		}
		//BroadcastMessage("PreviewMode", SendMessageOptions.DontRequireReceiver);
	}

	private void Initialize() {
		if (!initialized) {
			initialized = true;
			PluginRegistry pluginRegistry = (PluginRegistry)GameObject.FindObjectOfType(typeof(PluginRegistry));
			persistencePlugin = (PersistencePlugin)pluginRegistry.GetPlugin("Persistence");
			characters = (PersistenceCategory)persistencePlugin.GetElement("Characters");
			AvatarPlugin avatarPlugin = (AvatarPlugin)pluginRegistry.GetPlugin("Avatars");
			avatarElement = (AvatarElement)avatarPlugin.GetElement(characterType);
			avatarComponentPlugin = (AvatarComponentPlugin)pluginRegistry.GetPlugin("Avatar Components");
		}
	}

	// Use this for initialization
	void Start () {
		Initialize();
	}

	public void Save(string name) {
		//quadruple nested loop
		//it just needs to be there in order for this to work.
		//each level out in the loop represents one level
		//of flexibility in the design
		Initialize();
		this.characterName = name;
		PersistedCharacter persistedCharacter = new PersistedCharacter();
		foreach (ISelectableElement boneElement in avatarElement) {
			Transform bone = FindBone(boneElement.DisplayName);
			PersistedBone persistedBone = new PersistedBone();
			persistedBone.name = boneElement.DisplayName;
			for (int i = 0; i < bone.childCount; i++) {
				Transform child = bone.GetChild(i);
				AvatarComponent[] boneComponents = child.GetComponents<AvatarComponent>();
				foreach (AvatarComponent component in boneComponents) {
					PersistedObject persistedComponent = new PersistedObject();
					persistedComponent.objectType = component.DisplayName;
					Component[] persistentBehaviors = component.GetComponents(typeof(IPersistentBehavior));
					foreach (Component cc in persistentBehaviors) {
						((IPersistentBehavior)cc).Save(persistedComponent);
					}
					persistedBone.components.Add(persistedComponent);
				}
			}
			persistedCharacter.bones.Add(persistedBone);
		}
		persistencePlugin.SavePersistedComponent(characters, name, persistedCharacter, "character");
	}
//	public void Save(string name) {
//		PersistenceCategory worlds = (PersistenceCategory)persistencePlugin.GetElement("Worlds");
//		persistencePlugin.SavePersistedComponent(worlds, name, persistedWorldData, "world");
//		
//		//TODO: save/load by region
//		//save region
//		string regionPath = "region.0.0.0";
//		PersistedRegion region = new PersistedRegion();
//		//region.blocks = new List<PersistedObject>();
//		region.blocks = persistencePlugin.SerializeElements(grid.Values);
//		persistencePlugin.SavePersistedComponent(worlds, name, region, regionPath);
//		worldName = name;
//		messages.QueueMessage("World saved");
//	}

	public bool Exists(string name) {
		return persistencePlugin.PersistedItemExists(characters, name);
	}

//	
//	public List<string> ListGames(string filter) {
//		PersistenceCategory worlds = (PersistenceCategory)persistencePlugin.GetElement("Worlds");
//		return persistencePlugin.ListPersistedItems(worlds, filter);
//	}

	private Transform FindBone(string name) {
		if (boneLookup.Count == 0) {
			IndexBone(transform);
		}
		if (boneLookup.ContainsKey(name)) {
			return boneLookup[name];
		}
		return null;
	}

	private void IndexBone(Transform bone) {
		boneLookup[bone.name] = bone;
		for (int i = 0; i < bone.childCount; i++) {
			Transform child = bone.GetChild(i);
			IndexBone(child);
		}
	}

	public AvatarComponent AddComponent(string bonename, AvatarComponent component) {
		Transform bone = FindBone(bonename);
		if (bone == null) {
			throw new Exception("Bone not found: " + bonename);
		}
		AvatarComponent newComponent = (AvatarComponent)Instantiate(component);
		newComponent.transform.parent = bone;
		newComponent.transform.localPosition = Vector3.zero;
		if (editorMode) {
			newComponent.SendMessage("EditorMode", SendMessageOptions.DontRequireReceiver);
		} else {
			newComponent.SendMessage("PreviewMode", SendMessageOptions.DontRequireReceiver);
		}
		return newComponent;
	}

	private void ClearComponents() {
		foreach (AvatarComponent component in GetComponentsInChildren<AvatarComponent>()) {
			Destroy (component.gameObject);
		}
	}

	public void Load(string name) {
		Initialize();
		ClearComponents();
		characterName = name;
		PersistedCharacter persistedCharacter = persistencePlugin.LoadPersistedComponent<PersistedCharacter>(characters, name, "character");
		foreach (PersistedBone persistedBone in persistedCharacter.bones) {
			foreach (PersistedObject persistedComponent in persistedBone.components) {
				AvatarComponent component = (AvatarComponent)avatarComponentPlugin.GetElement(persistedComponent.objectType);
				if (component != null) {
					component = AddComponent(persistedBone.name, component);
					Component[] persistentBehaviors = component.GetComponents(typeof(IPersistentBehavior));
					foreach (Component persistentBehavior in persistentBehaviors) {
						((IPersistentBehavior)persistentBehavior).Restore(persistedComponent);
					}
				}
			}
		}
	}
//	
//	public void Load(string name) {
//		
//		Plugin blocks = pluginRegistry.GetPlugin("Blocks");
//		PersistenceCategory worlds = (PersistenceCategory)persistencePlugin.GetElement("Worlds");
//		persistedWorldData = persistencePlugin.LoadPersistedComponent<PersistedObject>(worlds, name, "world");
//		//TODO: hack?
//		CameraGuide cameraGuide = (CameraGuide)GameObject.FindObjectOfType(typeof(CameraGuide));
//		cameraGuide.Restore(persistedWorldData);
//		
//		//load blocks
//		ClearBlocks();
//		//TODO: load individual region
//		PersistedRegion region = persistencePlugin.LoadPersistedComponent<PersistedRegion>(worlds, name, "region.0.0.0");
//		foreach (PersistedObject obj in region.blocks) {
//			//TODO: display an error for blocks not found
//			//this could be due to a missing modpack...
//			Block b = (Block)blocks.GetElement(obj.objectType);
//			Block newBlock = (Block)Instantiate(b);
//			Component[] components = newBlock.GetComponents(typeof(IPersistentBehavior));
//			foreach (Component component in components) {
//				IPersistentBehavior persistentBehavior = (IPersistentBehavior)component;
//				persistentBehavior.Restore(obj);
//			}
//			AddBlock(newBlock);
//		}
//		worldName = name;
//	}

}
