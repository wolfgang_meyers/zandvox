#!/usr/bin/python
import os
from os import path
for (parent, dirs, files) in os.walk("."):
	for fname in files:
		if fname.endswith(".meta") or "conflicted" in fname:
			os.remove(path.join(parent, fname))